<!DOCTYPE html>
<!--[if lte IE 9 ]><html class="ie lt-ie9" lang="zh-TW"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="zh-TW"> <!--<![endif]-->
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="http://global.joyoung.com/xmlrpc.php" />

					<script type="text/javascript">document.documentElement.className = document.documentElement.className + ' yes-js js_active js'</script>
			<title>沒有符合條件的頁面。 &#8211; Joyoung 九陽健康食尚家</title>
			<style>
				.wishlist_table .add_to_cart, a.add_to_wishlist.button.alt { border-radius: 16px; -moz-border-radius: 16px; -webkit-border-radius: 16px; }			</style>
			<script type="text/javascript">
				var yith_wcwl_plugin_ajax_web_url = '/wp-admin/admin-ajax.php';
			</script>
		<link rel="alternate" type="application/rss+xml" title="訂閱 Joyoung 九陽健康食尚家 &raquo;" href="http://global.joyoung.com/feed/" />
<link rel="alternate" type="application/rss+xml" title="訂閱 Joyoung 九陽健康食尚家 &raquo; 迴響" href="http://global.joyoung.com/comments/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/global.joyoung.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.4.2"}};
			!function(a,b,c){function d(a){var c,d=b.createElement("canvas"),e=d.getContext&&d.getContext("2d"),f=String.fromCharCode;return e&&e.fillText?(e.textBaseline="top",e.font="600 32px Arial","flag"===a?(e.fillText(f(55356,56806,55356,56826),0,0),d.toDataURL().length>3e3):"diversity"===a?(e.fillText(f(55356,57221),0,0),c=e.getImageData(16,16,1,1).data.toString(),e.fillText(f(55356,57221,55356,57343),0,0),c!==e.getImageData(16,16,1,1).data.toString()):("simple"===a?e.fillText(f(55357,56835),0,0):e.fillText(f(55356,57135),0,0),0!==e.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag"),unicode8:d("unicode8"),diversity:d("diversity")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag&&c.supports.unicode8&&c.supports.diversity||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='jquery-custom-style-css'  href='http://global.joyoung.com/wp-content/themes/flatsome/css/jquery-ui-1.11.4.custom/jquery-ui.css?ver=1' type='text/css' media='screen' />
<link rel='stylesheet' id='font-awesome-css'  href='http://global.joyoung.com/wp-content/plugins/download-manager/assets/font-awesome/css/font-awesome.min.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='wpdm-front-css'  href='http://global.joyoung.com/wp-content/plugins/download-manager/assets/css/front.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css'  href='http://global.joyoung.com/wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.2.5.4' type='text/css' media='all' />
<style id='rs-plugin-settings-inline-css' type='text/css'>
#rs-demo-id {}
</style>
<link rel='stylesheet' id='flatsome-css-minified-css'  href='http://global.joyoung.com/wp-content/themes/flatsome/css/flatsome.min.css?ver=2.7.6' type='text/css' media='all' />
<link rel='stylesheet' id='flatsome-style-css'  href='http://global.joyoung.com/wp-content/themes/flatsome/style.css?ver=2.7.6' type='text/css' media='all' />
<link rel='stylesheet' id='flatsome-googlefonts-css'  href='//fonts.googleapis.com/css?family=Dancing+Script%3A300%2C400%2C700%2C900%7CcustomFont%3A300%2C400%2C700%2C900%7CcustomFont%3A300%2C400%2C700%2C900%7CcustomFont%3A300%2C400%2C700%2C900&#038;subset=latin&#038;ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='flatsome-woocommerce-wishlist-css'  href='http://global.joyoung.com/wp-content/themes/flatsome/inc/woocommerce/integrations/wishlist.css?ver=4.4.2' type='text/css' media='all' />
<!-- Inline jetpack_facebook_likebox -->
<style id='jetpack_facebook_likebox-inline-css' type='text/css'>
.widget_facebook_likebox {
	overflow: hidden;
}

</style>
<link rel='stylesheet' id='tablepress-default-css'  href='http://global.joyoung.com/wp-content/tablepress-combined.min.css?ver=64' type='text/css' media='all' />
<link rel='stylesheet' id='jetpack_css-css'  href='http://global.joyoung.com/wp-content/plugins/jetpack/css/jetpack.css?ver=4.1.1' type='text/css' media='all' />
<script type='text/javascript' src='http://global.joyoung.com/wp-includes/js/jquery/jquery.js?ver=1.11.3'></script>
<script type='text/javascript' src='http://global.joyoung.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
<script type='text/javascript' src='http://global.joyoung.com/wp-content/plugins/download-manager/assets/bootstrap/js/bootstrap.min.js?ver=4.4.2'></script>
<script type='text/javascript' src='http://global.joyoung.com/wp-content/plugins/download-manager/assets/js/front.js?ver=4.4.2'></script>
<script type='text/javascript' src='http://global.joyoung.com/wp-content/plugins/download-manager/assets/js/chosen.jquery.min.js?ver=4.4.2'></script>
<script type='text/javascript' src='http://global.joyoung.com/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.2.5.4'></script>
<script type='text/javascript' src='http://global.joyoung.com/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.2.5.4'></script>
<script type='text/javascript' src='http://global.joyoung.com/wp-content/plugins/wp-retina-2x/js/picturefill.min.js?ver=3.0.2'></script>
<script type='text/javascript' src='http://global.joyoung.com/wp-content/themes/flatsome/inc/woocommerce/integrations/wishlist.js?ver=4.4.2'></script>
<script type='text/javascript' src='http://global.joyoung.com/wp-content/themes/flatsome/js/jquery.ui.datepicker-zh-TW.js?ver=4.4.2'></script>
<meta name="generator" content="WordPress Download Manager 2.9.0" />
<link rel='https://api.w.org/' href='http://global.joyoung.com/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://global.joyoung.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://global.joyoung.com/wp-includes/wlwmanifest.xml" /> 

        <script>
            var wpdm_site_url = 'http://global.joyoung.com/';
            var wpdm_home_url = 'http://global.joyoung.com/';
            var ajax_url = 'http://global.joyoung.com/wp-admin/admin-ajax.php';
        </script>


        
<link rel='dns-prefetch' href='//v0.wordpress.com'>
<style type='text/css'>img#wpstats{display:none}</style><style>@font-face{ font-family: customFont; src: url(http://global.joyoung.com/wp-content/uploads/2016/05/Futura-Book-font.ttf); }</style><!--[if lt IE 9]><link rel="stylesheet" type="text/css" href="http://global.joyoung.com/wp-content/themes/flatsome/css/ie8.css"><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><script>var head = document.getElementsByTagName('head')[0],style = document.createElement('style');style.type = 'text/css';style.styleSheet.cssText = ':before,:after{content:none !important';head.appendChild(style);setTimeout(function(){head.removeChild(style);}, 0);</script><![endif]-->		<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
		<style type="text/css">.broken_link, a.broken_link {
	text-decoration: line-through;
}</style><meta name="generator" content="Powered by Slider Revolution 5.2.5.4 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
<link rel="icon" href="http://global.joyoung.com/wp-content/uploads/2015/12/cropped-joyoung_facion-300x300.png" sizes="32x32" />
<link rel="icon" href="http://global.joyoung.com/wp-content/uploads/2015/12/cropped-joyoung_facion-300x300.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="http://global.joyoung.com/wp-content/uploads/2015/12/cropped-joyoung_facion-300x300.png" />
<meta name="msapplication-TileImage" content="http://global.joyoung.com/wp-content/uploads/2015/12/cropped-joyoung_facion-300x300.png" />

<!-- BEGIN GADWP v4.9.3.1 Universal Tracking - https://deconf.com/google-analytics-dashboard-wordpress/ -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-83363003-1', 'auto');
  ga('send', 'pageview');
</script>

<!-- END GADWP Universal Tracking -->

<!-- Custom CSS Codes --><style type="text/css"> .top-bar-nav a.nav-top-link,body,p,#top-bar,.cart-inner .nav-dropdown,.nav-dropdown{font-family:customFont,helvetica,arial,sans-serif!important;}.header-nav a.nav-top-link, a.cart-link, .mobile-sidebar a{font-family:customFont,helvetica,arial,sans-serif!important;}h1,h2,h3,h4,h5,h6{font-family:customFont,helvetica,arial,sans-serif!important;}.alt-font{font-family:Dancing Script,Georgia,serif!important;} body.boxed,body.framed-layout,body{background-color:#def9f7; background-image:url(""); } #masthead{ height:140px;}#logo a img{ max-height:110px} #masthead #logo{width:303px;}#masthead #logo a{max-width:303px} #masthead.stuck.move_down{height:80px;}.wide-nav.move_down{top:80px;}#masthead.stuck.move_down #logo a img{ max-height:50px } ul.header-nav li a {font-size:80%}body{background-color:#def9f7; background-image:url(""); } #masthead{background-color:#fff; ;} .slider-nav-reveal .flickity-prev-next-button, #main-content{background-color:#FFF!important} .wide-nav {background-color:#eee} #top-bar{background-color:#ff8800 }.header-nav li.mini-cart.active .cart-icon strong{background-color:#ff8b07 } .alt-button.primary,.callout.style3 .inner .inner-text,.add-to-cart-grid .cart-icon strong,.tagcloud a,.navigation-paging a, .navigation-image a ,ul.page-numbers a, ul.page-numbers li > span,#masthead .mobile-menu a,.alt-button, #logo a, li.mini-cart .cart-icon strong,.widget_product_tag_cloud a, .widget_tag_cloud a,.post-date,#masthead .mobile-menu a.mobile-menu a,.checkout-group h3,.order-review h3 {color:#ff8b07;}.slider-nav-circle .flickity-prev-next-button:hover svg, .slider-nav-circle .flickity-prev-next-button:hover .arrow, .ux-box.ux-text-badge:hover .ux-box-text, .ux-box.ux-text-overlay .ux-box-image,.ux-header-element a:hover,.featured-table.ux_price_table .title,.scroll-to-bullets a strong,.scroll-to-bullets a.active,.scroll-to-bullets a:hover,.tabbed-content.pos_pills ul.tabs li.active a,.ux_hotspot,ul.page-numbers li > span,.label-new.menu-item a:after,.add-to-cart-grid .cart-icon strong:hover,.text-box-primary, .navigation-paging a:hover, .navigation-image a:hover ,.next-prev-nav .prod-dropdown > a:hover,ul.page-numbers a:hover,.widget_product_tag_cloud a:hover,.widget_tag_cloud a:hover,.custom-cart-count,.iosSlider .sliderNav a:hover span, li.mini-cart.active .cart-icon strong,.product-image .quick-view, .product-image .product-bg, #submit, button, #submit, button, .button, input[type="submit"],li.mini-cart.active .cart-icon strong,.post-item:hover .post-date,.blog_shortcode_item:hover .post-date,.column-slider .sliderNav a:hover,.ux_banner {background-color:#ff8b07}.slider-nav-circle .flickity-prev-next-button:hover svg, .slider-nav-circle .flickity-prev-next-button:hover .arrow, .ux-header-element a:hover,.featured-table.ux_price_table,.text-bordered-primary,.callout.style3 .inner,ul.page-numbers li > span,.add-to-cart-grid .cart-icon strong, .add-to-cart-grid .cart-icon-handle,.add-to-cart-grid.loading .cart-icon strong,.navigation-paging a, .navigation-image a ,ul.page-numbers a ,ul.page-numbers a:hover,.post.sticky,.widget_product_tag_cloud a, .widget_tag_cloud a,.next-prev-nav .prod-dropdown > a:hover,.iosSlider .sliderNav a:hover span,.column-slider .sliderNav a:hover,.woocommerce .order-review, .woocommerce-checkout form.login,.button, button, li.mini-cart .cart-icon strong,li.mini-cart .cart-icon .cart-icon-handle,.post-date{border-color:#ff8b07;}.ux-loading{border-left-color:#ff8b07;}.primary.alt-button:hover,.button.alt-button:hover{background-color:#ff8b07!important}.flickity-prev-next-button:hover svg, .flickity-prev-next-button:hover .arrow, .featured-box:hover svg, .featured-img svg:hover{fill:#ff8b07!important;}.slider-nav-circle .flickity-prev-next-button:hover svg, .slider-nav-circle .flickity-prev-next-button:hover .arrow, .featured-box:hover .featured-img-circle svg{fill:#FFF!important;}.featured-box:hover .featured-img-circle{background-color:#ff8b07!important; border-color:#ff8b07!important;} .star-rating:before, .woocommerce-page .star-rating:before, .star-rating span:before{color:#ff9d2d}.secondary.alt-button,li.menu-sale a{color:#ff9d2d!important}.secondary-bg.button.alt-button.success:hover,.label-sale.menu-item a:after,.mini-cart:hover .custom-cart-count,.callout .inner,.button.secondary,.button.checkout,#submit.secondary, button.secondary, .button.secondary, input[type="submit"].secondary{background-color:#ff9d2d}.button.secondary,.button.secondary{border-color:#ff9d2d;}.secondary.alt-button:hover{color:#FFF!important;background-color:#ff9d2d!important}ul.page-numbers li > span{color:#FFF;} .callout.style3 .inner.success-bg .inner-text,.woocommerce-message{color:#ea4438!important}.success-bg,.woocommerce-message:before,.woocommerce-message:after{color:#FFF!important; background-color:#ea4438}.label-popular.menu-item a:after,.add-to-cart-grid.loading .cart-icon strong,.add-to-cart-grid.added .cart-icon strong{background-color:#ea4438;border-color:#ea4438;}.add-to-cart-grid.loading .cart-icon .cart-icon-handle,.add-to-cart-grid.added .cart-icon .cart-icon-handle{border-color:#ea4438} .star-rating span:before,.star-rating:before, .woocommerce-page .star-rating:before {color:#fcbb3a!important} a,.icons-row a.icon{color:#ff8b07}.cart_list_product_title{color:#ff8b07!important}.icons-row a.icon{border-color:#ff8b07;}.icons-row a.icon:hover{background-color:#ff8b07;border-color:#ff8b07;} .label-new.menu-item > a:after{content:"New";}.label-hot.menu-item > a:after{content:"Hot";}.label-sale.menu-item > a:after{content:"Sale";}.label-popular.menu-item > a:after{content:"Popular";}.featured_item_image{max-height:250px}.callout .inner.callout-new-bg{background-color:#ff8d0c!important;}.callout.style3 .inner.callout-new-bg{background-color:none!important;border-color:#ff8d0c!important} ul.header-nav a.nav-top-link, a.cart-link{font-size:15px!important}ul.header-nav li ul {font-size:20px;}.entry-author-link,.entry-permalink,.entry-date,.entry-meta {display:none;}body.single-post div.ux_banner > div.row.parallax_text > div > div > header > div.share-row{display:none;}table.customIcons td{line-height:1px;}.blog-share .share-row table{width:auto;margin-left:auto;margin-right:auto;} #top-bar li > a { font-size:100%; }body.archive h1.page-title { visibility:hidden;}body.archive h1.page-title span { visibility:visible; float:left;font-size:130%;}.textwidget {line-height:150%;font-size:90%;}.post_comments span{visibility:visible; }.nav-dropdown li {font-size:95%} .modal-content {display:none;}@media only screen and (max-width:48em) {div.blog-share div.share-row{position:absolute;left:28%;}}</style></head>

<body class="error404 antialiased group-blog sticky_header breadcrumb-normal boxed">


	<div id="wrapper" class="box-shadow">
		<div class="header-wrapper before-sticky">
				<div id="top-bar">
			<div class="row">
				<div class="large-12 columns">
					<!-- left text -->
					<div class="left-text left">
						<div class="html">LINE名稱：九陽陽光客服中心 / LINE ID 帳號：joyoungtw</div><!-- .html -->
					</div>
					<!-- top bar right -->
					<div class="right-text right">

							<ul id="menu-top-bar-menu" class="top-bar-nav">
								<li id="menu-item-4293" class="icon-star menu-item menu-item-type-post_type menu-item-object-page menu-item-4293"><a href="http://global.joyoung.com/%e7%94%a2%e5%93%81%e4%bf%9d%e5%9b%ba%e8%aa%aa%e6%98%8e/" class="nav-top-link">產品保固</a></li>
<li id="menu-item-3999" class="icon-facebook menu-item menu-item-type-custom menu-item-object-custom menu-item-3999"><a href="https://www.facebook.com/joyoung.global" class="nav-top-link">九陽健康食尚家粉絲團</a></li>
<li id="menu-item-4187" class="icon-heart menu-item menu-item-type-post_type menu-item-object-page menu-item-4187"><a href="http://global.joyoung.com/my-account/wishlist/" class="nav-top-link">我的收藏</a></li>

			                        									<li class="account-dropdown menu-parent-item">
																				<a href="http://global.joyoung.com/my-account/" class="nav-top-link nav-top-not-logged-in">登入</a>
																
									</li>
									
			                        
																</ul>
					</div><!-- top bar right -->

				</div><!-- .large-12 columns -->
			</div><!-- .row -->
		</div><!-- .#top-bar -->
				<header id="masthead" class="site-header" role="banner">
			<div class="row"> 
				<div class="large-12 columns header-container">
					<div class="mobile-menu show-for-small">
						<a href="#jPanelMenu" class="off-canvas-overlay" data-pos="left" data-color="light"><span class="icon-menu"></span></a>
					</div><!-- end mobile menu -->

					 
					<div id="logo" class="logo-left">
						<a href="http://global.joyoung.com/" title="Joyoung 九陽健康食尚家 - 九陽倡導健康飲食生活方式!" rel="home">
							<img src="http://global.joyoung.com/wp-content/uploads/2015/12/joyoung-0800-stickylogo.png" class="header_logo has_sticky_logo" alt="Joyoung 九陽健康食尚家"/><img src="http://global.joyoung.com/wp-content/uploads/2015/12/joyoung-0800-stickylogo.png" class="header_logo_sticky" alt="Joyoung 九陽健康食尚家"/>						</a>
					</div><!-- .logo -->
					
					<div class="left-links">
													<ul id="site-navigation" class="header-nav">
																
																<li class="search-dropdown">
									<a href="#" class="nav-top-link icon-search" onClick="return false;"></a>
									<div class="nav-dropdown">
										

          
<div class="row yith-search-premium collapse search-wrapper yith-ajaxsearchform-container yith-ajaxsearchform-container 1179572114_container">
<form role="search" method="get" class="yith-search-premium" id="yith-ajaxsearchform" action="http://global.joyoung.com/">
      <div class="large-10 small-10 columns">
        <input type="hidden" name="post_type" class="yit_wcas_post_type" id="yit_wcas_post_type" value="product" />
        <input type="search" 
        value="" 
        name="s"
        id="1179572114_yith-s"
        class="yith-s"
        data-append-top
        placeholder="商品收尋"
        data-loader-icon=""
        data-min-chars="3" />
      </div><!-- input -->
      <div class="large-2 small-2 columns">
        <button type="submit" id="yith-searchsubmit" class="button secondary postfix"><i class="icon-search"></i></button>
      </div><!-- button -->
</form>
</div><!-- row -->

<script type="text/javascript">
jQuery(function($){
    if (jQuery().yithautocomplete) {
        $('#1179572114_yith-s').yithautocomplete({
            minChars: 3,
            appendTo: '.1179572114_container',
            serviceUrl: woocommerce_params.ajax_url + '?action=yith_ajax_search_products',
            onSearchStart: function(){
                $('.1179572114_container').append('<div class="ux-loading"></div>');
            },
            onSearchComplete: function(){
                $('.1179572114_container .ux-loading').remove();

            },
            onSelect: function (suggestion) {
                if( suggestion.id != -1 ) {
                    window.location.href = suggestion.url;
                }
            }
        });

    } else {
        $('#1179572114_yith-s').autocomplete({
            minChars: 3,
            appendTo: '.1179572114_container',
            serviceUrl: woocommerce_params.ajax_url + '?action=yith_ajax_search_products',
            onSearchStart: function(){
                $('.1179572114_container').append('<div class="ux-loading"></div>');
            },
            onSearchComplete: function(){
                $('.1179572114_container .ux-loading').remove();

            },
            onSelect: function (suggestion) {
                if( suggestion.id != -1 ) {
                    window.location.href = suggestion.url;
                }
            }
        });

    }
});
</script> 	
									</div><!-- .nav-dropdown -->
								</li><!-- .search-dropdown -->
								
									<li id="menu-item-4018" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4018"><a href="http://global.joyoung.com/" class="nav-top-link">首頁</a></li>
<li id="menu-item-4005" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-parent-item menu-item-4005"><a href="http://global.joyoung.com/%e5%95%86%e5%93%81%e4%bb%8b%e7%b4%b9/" class="nav-top-link">商品介紹</a>
<div class=nav-dropdown><ul>
	<li id="menu-item-6353" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-6353"><a href="http://global.joyoung.com/商品分類/%e8%b1%86%e6%bc%bf%e6%a9%9f/">豆漿機</a></li>
	<li id="menu-item-6354" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-6354"><a href="http://global.joyoung.com/商品分類/%e5%85%a8%e8%83%bd%e9%8d%8b/">全能鍋</a></li>
	<li id="menu-item-6355" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-6355"><a href="http://global.joyoung.com/商品分類/%e5%8e%9f%e6%b1%81%e6%a9%9f/">原汁機</a></li>
	<li id="menu-item-6356" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-6356"><a href="http://global.joyoung.com/商品分類/%e6%96%99%e7%90%86%e6%a9%9f/">料理機</a></li>
	<li id="menu-item-6357" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-6357"><a href="http://global.joyoung.com/商品分類/%e5%bf%ab%e7%85%ae%e5%a3%ba/">快煮壺</a></li>
	<li id="menu-item-6358" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-6358"><a href="http://global.joyoung.com/商品分類/%e5%84%aa%e7%b1%b3%e6%a9%9f/">優米機</a></li>
	<li id="menu-item-5856" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-5856"><a href="http://global.joyoung.com/商品分類/%e9%9b%bb%e8%92%b8%e9%8d%8b/">電蒸鍋</a></li>
	<li id="menu-item-6359" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-6359"><a href="http://global.joyoung.com/商品分類/%e8%a3%bd%e9%ba%b5%e6%a9%9f/">製麵機</a></li>
</ul></div>
</li>
<li id="menu-item-4010" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-parent-item menu-item-4010"><a href="http://global.joyoung.com/%e6%9c%8d%e5%8b%99%e5%b0%88%e5%8d%80/" class="nav-top-link">服務專區</a>
<div class=nav-dropdown><ul>
	<li id="menu-item-4006" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4006"><a href="http://global.joyoung.com/qa/">產品Q&#038;A</a></li>
	<li id="menu-item-3995" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3995"><a href="http://global.joyoung.com/?page_id=2866">維修流程</a></li>
	<li id="menu-item-4019" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4019"><a href="http://global.joyoung.com/%e6%9c%8d%e5%8b%99%e5%b0%88%e5%8d%80/%e7%b0%a1%e6%98%93%e6%95%85%e9%9a%9c%e5%88%86%e6%9e%90%e5%8f%8a%e6%8e%92%e9%99%a4%e7%90%86%e9%a3%9f%e8%ad%9c/">簡易故障分析及排除</a></li>
	<li id="menu-item-4011" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4011"><a href="http://global.joyoung.com/%e6%9c%8d%e5%8b%99%e5%b0%88%e5%8d%80/%e7%b6%ad%e4%bf%ae%e6%9c%8d%e5%8b%99%e6%93%9a%e9%bb%9e/">維修服務據點</a></li>
	<li id="menu-item-4015" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4015"><a href="http://global.joyoung.com/%e9%8a%b7%e5%94%ae%e9%80%9a%e8%b7%af/%e5%85%a8%e7%9c%81%e8%b3%bc%e8%b2%b7%e6%93%9a%e9%bb%9e/">全省購買據點</a></li>
	<li id="menu-item-4016" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4016"><a href="http://global.joyoung.com/%e9%8a%b7%e5%94%ae%e9%80%9a%e8%b7%af/%e7%89%b9%e7%b4%84%e7%b6%b2%e7%ab%99%e8%b3%bc%e8%b2%b7/">特約網站購買</a></li>
	<li id="menu-item-4495" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4495"><a href="http://global.joyoung.com/%E7%94%A2%E5%93%81%E4%BF%9D%E5%9B%BA%E8%AA%AA%E6%98%8E/">產品保固</a></li>
	<li id="menu-item-6570" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6570"><a href="http://global.joyoung.com/%e6%9c%8d%e5%8b%99%e5%b0%88%e5%8d%80/download%e4%b8%8b%e8%bc%89%e5%b0%88%e5%8d%80/">下載專區</a></li>
</ul></div>
</li>
<li id="menu-item-3994" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-parent-item menu-item-3994"><a href="http://global.joyoung.com/%E7%BE%8E%E5%91%B3%E5%A4%A7%E9%9B%86%E5%90%88/%E5%89%B5%E6%84%8F%E9%A3%9F%E8%AD%9C/" class="nav-top-link">美味健康大集合</a>
<div class=nav-dropdown><ul>
	<li id="menu-item-4012" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4012"><a href="http://global.joyoung.com/%e7%be%8e%e5%91%b3%e5%a4%a7%e9%9b%86%e5%90%88/%e5%89%b5%e6%84%8f%e9%a3%9f%e8%ad%9c/">創意食譜</a></li>
	<li id="menu-item-4007" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4007"><a href="http://global.joyoung.com/%e5%81%a5%e5%ba%b7%e5%b0%8f%e7%9f%a5%e8%ad%98/">健康小知識</a></li>
</ul></div>
</li>
<li id="menu-item-4880" class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-has-children menu-parent-item menu-item-4880"><a href="http://global.joyoung.com/%e6%9c%80%e6%96%b0%e6%b6%88%e6%81%af/" class="nav-top-link">最新消息</a>
<div class=nav-dropdown><ul>
	<li id="menu-item-4881" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-4881"><a title="活動訊息" href="http://global.joyoung.com/category/%e6%9c%80%e6%96%b0%e6%b6%88%e6%81%af/%e6%b4%bb%e5%8b%95%e8%a8%8a%e6%81%af/">活動訊息</a></li>
	<li id="menu-item-4008" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4008"><a href="http://global.joyoung.com/%e6%9c%80%e6%96%b0%e6%b6%88%e6%81%af/%e5%bd%b1%e9%9f%b3%e5%b0%88%e5%8d%80/">影音專區</a></li>
</ul></div>
</li>
<li id="menu-item-4013" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4013"><a href="http://global.joyoung.com/%e8%81%af%e7%b5%a1%e6%88%91%e5%80%91/" class="nav-top-link">聯絡我們</a></li>
<li id="menu-item-3996" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-parent-item menu-item-3996"><a href="http://global.joyoung.com/%E9%97%9C%E6%96%BC%E4%B9%9D%E9%99%BD/" class="nav-top-link">關於九陽</a>
<div class=nav-dropdown><ul>
	<li id="menu-item-4017" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4017"><a href="http://global.joyoung.com/%e9%97%9c%e6%96%bc%e4%b9%9d%e9%99%bd/">公司概述</a></li>
	<li id="menu-item-4021" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4021"><a href="http://global.joyoung.com/%e9%97%9c%e6%96%bc%e4%b9%9d%e9%99%bd/%e6%ad%b7%e5%8f%b2%e6%b2%bf%e9%9d%a9/">歷史沿革</a></li>
	<li id="menu-item-4020" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4020"><a href="http://global.joyoung.com/%e9%97%9c%e6%96%bc%e4%b9%9d%e9%99%bd/%e5%ae%89%e5%bf%83%e4%bf%9d%e8%ad%89/">安心保證</a></li>
</ul></div>
</li>

								
		                        								
							</ul>
											</div><!-- .left-links -->

					
					<div class="right-links">
						<ul  class="header-nav">
							
						
							
											<li class="html-block">
						<div class="html-block-inner">
													</div>
					</li>
									</ul><!-- .header-nav -->
			</div><!-- .right-links -->
		</div><!-- .large-12 -->
	</div><!-- .row -->


</header><!-- .header -->

</div><!-- .header-wrapper -->

<div id="main-content" class="site-main hfeed light">
<div class="row"><div class="large-12 columns"><div class="top-divider"></div></div></div>
<div class="block-html-after-header" style="position:relative;top:-1px;"><script>
jQuery(document).ready(function(){
jQuery('table.customIcons tbody tr td div.jiathis_style_24x24 br').remove();
});
</script></div>
<!-- woocommerce message -->

<div  class="page-wrapper">
<div class="row">

	
<div id="content" class="large-12 left columns" role="main">
		<article id="post-0" class="post error404 not-found">
				<header class="entry-header">
					<h1 class="entry-title">Oops! That page can&rsquo;t be found.</h1>
				</header><!-- .entry-header -->
				<div class="entry-content">


		<p>It looks like nothing was found at this location. Maybe try one of the links below or a search?</p>

					

<div class="row collapse search-wrapper">
<form method="get" id="searchform" class="searchform" action="http://global.joyoung.com/" role="search">
	  <div class="large-10 small-10 columns">
	   		<input type="search" class="field" name="s" value="" id="s" placeholder="搜尋&hellip;" />
	  </div><!-- input -->
	  <div class="large-2 small-2 columns">
	    <button class="button secondary postfix"><i class="icon-search"></i></button>
	  </div><!-- button -->
</form>
</div><!-- row -->


								</div><!-- .entry-content -->
			</article><!-- #post-0 .post .error404 .not-found -->


</div><!-- end #content large-9 left -->

</div><!-- end row -->
</div><!-- end page-right-sidebar container -->




</div><!-- #main-content -->

<footer class="footer-wrapper" role="contentinfo">	

<!-- FOOTER 1 -->


<!-- FOOTER 2 -->
<div class="footer footer-2 light" style="background-color:#ffffff">
	<div class="row">

   		<div id="facebook-likebox-2" class="large-3 columns widget left widget_facebook_likebox"><h3 class="widget-title"><a href="https://www.facebook.com/joyoung.global">九陽健康食尚家 / FACEBOOK</a></h3><div class="tx-div small"></div>		<div id="fb-root"></div>
		<div class="fb-page" data-href="https://www.facebook.com/joyoung.global" data-width="340"  data-height="320" data-hide-cover="false" data-show-facepile="true" data-show-posts="true">
		<div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/joyoung.global"><a href="https://www.facebook.com/joyoung.global">九陽健康食尚家 / FACEBOOK</a></blockquote></div>
		</div>
		</div>		<div id="flatsome_recent_posts-4" class="large-3 columns widget left flatsome_recent_posts">		<h3 class="widget-title">最新消息 / NEWS UPDATE</h3><div class="tx-div small"></div>		<ul>
					<li>
				<div class="post-date">
	                    <span class="post-date-day">19</span>
	                    <span class="post-date-month">八月</span>
                </div>
         
                <a href="http://global.joyoung.com/%e8%b1%86%e6%bc%bf%e6%a9%9f%e4%bd%bf%e7%94%a8%e6%8f%90%e7%a4%ba/" title="豆漿機使用提示!">豆漿機使用提示!</a>
				<div class="post_comments"><span><span class="screen-reader-text">豆漿機使用提示!</span>  已關閉迴響。</span></div>
            </li>
					<li>
				<div class="post-date">
	                    <span class="post-date-day">03</span>
	                    <span class="post-date-month">八月</span>
                </div>
         
                <a href="http://global.joyoung.com/%e4%b9%9d%e9%99%bd%e9%a3%9f%e5%b0%9a%e7%88%b6%e8%a6%aa%e7%af%80%e5%b9%b8%e7%a6%8f%e5%a5%bd%e7%b0%a1%e5%96%ae/" title="九陽食尚父親節,幸福好簡單">九陽食尚父親節,幸福好簡單</a>
				<div class="post_comments"><span><span class="screen-reader-text">九陽食尚父親節,幸福好簡單</span>  已關閉迴響。</span></div>
            </li>
					<li>
				<div class="post-date">
	                    <span class="post-date-day">05</span>
	                    <span class="post-date-month">七月</span>
                </div>
         
                <a href="http://global.joyoung.com/5826-2/" title="九陽食尚愛 幸福好簡單">九陽食尚愛 幸福好簡單</a>
				<div class="post_comments"><span><span class="screen-reader-text">九陽食尚愛 幸福好簡單</span>  已關閉迴響。</span></div>
            </li>
					<li>
				<div class="post-date">
	                    <span class="post-date-day">24</span>
	                    <span class="post-date-month">六月</span>
                </div>
         
                <a href="http://global.joyoung.com/%e7%9b%b8%e9%97%9c%e7%b6%ad%e4%bf%ae%e4%ba%8b%e5%ae%9c%e7%89%b9%e6%ad%a4%e5%85%ac%e5%91%8a/" title="相關維修事宜,特此公告">相關維修事宜,特此公告</a>
				<div class="post_comments"><span><span class="screen-reader-text">相關維修事宜,特此公告</span>  已關閉迴響。</span></div>
            </li>
				</ul>
		</div><div id="ninja_forms_widget-3" class="large-3 columns widget left widget_ninja_forms_widget"><h3 class="widget-title">最新活動訊息 / Promotions</h3><div class="tx-div small"></div>	<div id="ninja_forms_form_8_cont" class="ninja-forms-cont">
		<style type="text/css" media="screen">
	div.ninja-forms-error-msg{border-width: 2px;border-color: #dd0808;color: #dd0808;}div.ninja-forms-error{border-width: 2px;border-style: solid;border-color: #dd0808;color: #dd0808;padding: 10px;}div.ninja-forms-error label{color: #dd0808;}div.ninja-forms-error .ninja-forms-field{color: #dd0808;}div.ninja-forms-error div.ninja-forms-field-error{color: #dd0808;}	</style>
		<div id="ninja_forms_form_8_wrap" class="ninja-forms-form-wrap">
	<div id="ninja_forms_form_8_response_msg" style="" class="ninja-forms-response-msg "></div>	<form id="ninja_forms_form_8" enctype="multipart/form-data" method="post" name="" action="http://global.joyoung.com/wp-admin/admin-ajax.php?action=ninja_forms_ajax_submit" class="ninja-forms-form">

	<input type="hidden" id="_wpnonce" name="_wpnonce" value="731a7522d5" /><input type="hidden" name="_wp_http_referer" value="/page.php" />	<input type="hidden" name="_ninja_forms_display_submit" value="1">
	<input type="hidden" name="_form_id"  id="_form_id" value="8">
		<div class="hp-wrap">
		<label>If you are a human and are seeing this field, please leave it blank.			<input type="text" value="" name="_PKV0f">
			<input type="hidden" value="_PKV0f" name="_hp_name">
		</label>
	</div>
		<div id="ninja_forms_form_8_all_fields_wrap" class="ninja-forms-all-fields-wrap">
			<div class="ninja-forms-required-items">標有 <span class="ninja-forms-req-symbol">*</span> 是必要填寫完成的。</div>
			<div class="ninja-forms-field  nf-desc" id="ninja_forms_field_6_div_wrap" style="" rel="6"><p>輸入您的電子郵件，將不定時收到我們最新的活動訊息、創意食譜等等。</p>
</div>
							<div class="field-wrap text-wrap label-inside"  id="ninja_forms_field_7_div_wrap" data-visible="1">
							<input type="hidden" id="ninja_forms_field_7_type" value="text">
	<input id="ninja_forms_field_7" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_7" type="text" placeholder="" class="ninja-forms-field  ninja-forms-req email " value="請輸入您的E-mail *" rel="7"   />
			<input type="hidden" id="ninja_forms_field_7_label_hidden" value="請輸入您的E-mail *">
			<div id="ninja_forms_field_7_error" style="display:none;" class="ninja-forms-field-error">
		</div>
							</div>
												<div class="field-wrap submit-wrap label-above button-wrap secondary-wrap"  id="ninja_forms_field_8_div_wrap" data-visible="1">
							<input type="hidden" id="ninja_forms_field_8_type" value="submit">
	<div id="nf_submit_8">
		<input type="submit" name="_ninja_forms_field_8" class="ninja-forms-field  button secondary" id="ninja_forms_field_8" value="送出" rel="8" >
	</div>
	<div id="nf_processing_8" style="display:none;">
		<input type="submit" name="_ninja_forms_field_8" class="ninja-forms-field  button secondary" id="ninja_forms_field_8" value="處理中" rel="8" disabled>
	</div>
		<div id="ninja_forms_field_8_error" style="display:none;" class="ninja-forms-field-error">
		</div>
							</div>
							</div>
		</form>
		</div>
		</div>
	</div><div id="text-2" class="large-3 columns widget left widget_text"><h3 class="widget-title">九陽陽光客服中心 / SERVICE</h3><div class="tx-div small"></div>			<div class="textwidget"><p>九陽客服電話：0800-010-288 <br />服務時間：每週一至週五<br />9:00~12.30/13:30~18:00<br />(例假日除外)。歡迎您的來電。</p>
<p>九陽Line ID帳號：joyoungtw<br />
提醒您，在Line上僅限於或詢問九陽相關商品。您所詢問的內容，我們將於上班日儘速回覆，謝謝您！</p>
<p><a href="http://line.me/R/msg/text/?九陽陽光客服中心%0D%0AID帳號：joyoungtw
%0D%0A您所詢問有關於『商品型號、商品功能、維修進度或商品瑕疵』等事項，我們將於上班日儘速回覆，謝謝您！"target="_blank"><img src="http://global.joyoung.com/wp-content/uploads/2016/07/joyoungtw_lineid.png" width="180" height="54" alt="lineid_joyoungtw" /></a></p>
</div>
		</div>        
	</div><!-- end row -->
</div><!-- end footer 2 -->

 

<div id="newsletter-signup" class="mfp-hide mfp-content-inner lightbox-white" style="max-width:600px;padding:0px">
       <div id="banner_1300674278" class="ux_banner light    "  style="height:400px; " data-height="400px" role="banner">
                                  <div class="banner-bg "  style="background-image:url('http://global.joyoung.com/wp-content/uploads/2015/12/newsletterTiffany.jpg'); "><img src="http://global.joyoung.com/wp-content/uploads/2015/12/newsletterTiffany.jpg"  alt="" style="visibility:hidden;" /></div>
                             <div class="row" >
          <div class="inner left top text-center "  style="width:40%;">
            <div class="inner-wrap animated fadeInLeft" style=" ">
              <h3>願意收到最新活動訊息！</h3>
<div class="tx-div medium"></div>
<p>
		<div id="ninja_forms_form_8_cont" class="ninja-forms-cont">
		<style type="text/css" media="screen">
	div.ninja-forms-error-msg{border-width: 2px;border-color: #dd0808;color: #dd0808;}div.ninja-forms-error{border-width: 2px;border-style: solid;border-color: #dd0808;color: #dd0808;padding: 10px;}div.ninja-forms-error label{color: #dd0808;}div.ninja-forms-error .ninja-forms-field{color: #dd0808;}div.ninja-forms-error div.ninja-forms-field-error{color: #dd0808;}	</style>
		<div id="ninja_forms_form_8_wrap" class="ninja-forms-form-wrap">
	<div id="ninja_forms_form_8_response_msg" style="" class="ninja-forms-response-msg "></div>	<form id="ninja_forms_form_8" enctype="multipart/form-data" method="post" name="" action="http://global.joyoung.com/wp-admin/admin-ajax.php?action=ninja_forms_ajax_submit" class="ninja-forms-form">

	<input type="hidden" id="_wpnonce" name="_wpnonce" value="731a7522d5" /><input type="hidden" name="_wp_http_referer" value="/page.php" />	<input type="hidden" name="_ninja_forms_display_submit" value="1">
	<input type="hidden" name="_form_id"  id="_form_id" value="8">
		<div class="hp-wrap">
		<label>If you are a human and are seeing this field, please leave it blank.			<input type="text" value="" name="_rm2XD">
			<input type="hidden" value="_rm2XD" name="_hp_name">
		</label>
	</div>
		<div id="ninja_forms_form_8_all_fields_wrap" class="ninja-forms-all-fields-wrap">
			<div class="ninja-forms-required-items">標有 <span class="ninja-forms-req-symbol">*</span> 是必要填寫完成的。</div>
			<div class="ninja-forms-field  nf-desc" id="ninja_forms_field_6_div_wrap" style="" rel="6"><p>輸入您的電子郵件，將不定時收到我們最新的活動訊息、創意食譜等等。</p>
</div>
							<div class="field-wrap text-wrap label-inside"  id="ninja_forms_field_7_div_wrap" data-visible="1">
							<input type="hidden" id="ninja_forms_field_7_type" value="text">
	<input id="ninja_forms_field_7" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_7" type="text" placeholder="" class="ninja-forms-field  ninja-forms-req email " value="請輸入您的E-mail *" rel="7"   />
			<input type="hidden" id="ninja_forms_field_7_label_hidden" value="請輸入您的E-mail *">
			<div id="ninja_forms_field_7_error" style="display:none;" class="ninja-forms-field-error">
		</div>
							</div>
												<div class="field-wrap submit-wrap label-above button-wrap secondary-wrap"  id="ninja_forms_field_8_div_wrap" data-visible="1">
							<input type="hidden" id="ninja_forms_field_8_type" value="submit">
	<div id="nf_submit_8">
		<input type="submit" name="_ninja_forms_field_8" class="ninja-forms-field  button secondary" id="ninja_forms_field_8" value="送出" rel="8" >
	</div>
	<div id="nf_processing_8" style="display:none;">
		<input type="submit" name="_ninja_forms_field_8" class="ninja-forms-field  button secondary" id="ninja_forms_field_8" value="處理中" rel="8" disabled>
	</div>
		<div id="ninja_forms_field_8_error" style="display:none;" class="ninja-forms-field-error">
		</div>
							</div>
							</div>
		</form>
		</div>
		</div>
	
            </div>
          </div>  
        </div>
                     </div><!-- end .ux_banner -->

</div><!-- Lightbox-newsletter-signup -->

<script>
jQuery(document).ready(function($) {

      
      $('a[href="#newsletter-signup"]').click(function(e){
         // Close openend lightboxes
         var delay = 0;
         
         if($.magnificPopup.open){
            $.magnificPopup.close();
            delay = 300;
         }

         // Start lightbox
         setTimeout(function(){
            $.magnificPopup.open({
                  midClick: true,
                  removalDelay: 300,
                  items: {
                    src: '#newsletter-signup', 
                    type: 'inline'
                  }
            });
          }, delay);

        e.preventDefault();
      });
});
</script>



<img src="http://www.joyoung-global.com/wp-content/uploads/2016/05/joyoung-curve.png"; width="100%" />
<div class="absolute-footer dark" style="background-color:#f26522">
<div class="row">
	<div class="large-12 columns">
		<div class="left">
			 		<div class="copyright-footer"><p style='color:white;'><b>Copyright 2016©九陽Joyoung-豆漿機的開創者與領導者</b></p></div>
		</div><!-- .left -->
		<div class="right">
				<a href="http://global.joyoung.com/%E9%9A%B1%E7%A7%81%E6%AC%8A%E5%92%8C%E5%80%8B%E8%B3%87%E8%81%B2%E6%98%8E%E8%88%87%E6%9C%83%E5%93%A1%E6%AC%8A%E5%88%A9%E5%AE%A3%E5%91%8A/">隱私權和個資聲明</a>		</div>
	</div><!-- .large-12 -->
</div><!-- .row-->
</div><!-- .absolute-footer -->
</footer><!-- .footer-wrapper -->
</div><!-- #wrapper -->

<!-- back to top -->
<a href="#top" id="top-link" class="animated fadeInUp"><span class="icon-angle-up"></span></a>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PZT3N2"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PZT3N2');</script>
<!-- End Google Tag Manager -->
        <div class="w3eden">
            <div id="wpdm-popup-link" class="modal fade">
                <div class="modal-dialog" style="width: 750px">
                    <div class="modal-content">
                        <div class="modal-header">
                              <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body" id='wpdm-modal-body'>
                            <p><a href="http://www.wpdownloadmanager.com/">WordPress Download Manager - Best Download Management Plugin</a></p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->


        </div>
        <script language="JavaScript">
            <!--
            jQuery(function () {
                //jQuery('#wpdm-popup-link').modal('hide');
                jQuery('.wpdm-popup-link').click(function (e) {
                    e.preventDefault();
                    jQuery('#wpdm-popup-link .modal-title').html(jQuery(this).data('title'));
                    jQuery('#wpdm-modal-body').html('<i class="icon"><img align="left" style="margin-top: -1px" src="http://global.joyoung.com/wp-content/plugins/download-manager/images/loading-new.gif" /></i>&nbsp;Please Wait...');
                    jQuery('#wpdm-popup-link').modal('show');
                    jQuery('#wpdm-modal-body').load(this.href,{mode:'popup'});
                    return false;
                });
            });
            //-->
        </script>
        <style type="text/css">
            #wpdm-modal-body img {
                max-width: 100% !important;
            }
        </style>
    	<div style="display:none">
	</div>

<!-- Mobile Popup -->
<div id="jPanelMenu" class="mfp-hide">
    <div class="mobile-sidebar">
                <ul class="html-blocks">
            <li class="html-block">
                             </li>
        </ul>
        
        <ul class="mobile-main-menu">
                <li class="search">
            

          
<div class="row yith-search-premium collapse search-wrapper yith-ajaxsearchform-container yith-ajaxsearchform-container 1511964230_container">
<form role="search" method="get" class="yith-search-premium" id="yith-ajaxsearchform" action="http://global.joyoung.com/">
      <div class="large-10 small-10 columns">
        <input type="hidden" name="post_type" class="yit_wcas_post_type" id="yit_wcas_post_type" value="product" />
        <input type="search" 
        value="" 
        name="s"
        id="1511964230_yith-s"
        class="yith-s"
        data-append-top
        placeholder="商品收尋"
        data-loader-icon=""
        data-min-chars="3" />
      </div><!-- input -->
      <div class="large-2 small-2 columns">
        <button type="submit" id="yith-searchsubmit" class="button secondary postfix"><i class="icon-search"></i></button>
      </div><!-- button -->
</form>
</div><!-- row -->

<script type="text/javascript">
jQuery(function($){
    if (jQuery().yithautocomplete) {
        $('#1511964230_yith-s').yithautocomplete({
            minChars: 3,
            appendTo: '.1511964230_container',
            serviceUrl: woocommerce_params.ajax_url + '?action=yith_ajax_search_products',
            onSearchStart: function(){
                $('.1511964230_container').append('<div class="ux-loading"></div>');
            },
            onSearchComplete: function(){
                $('.1511964230_container .ux-loading').remove();

            },
            onSelect: function (suggestion) {
                if( suggestion.id != -1 ) {
                    window.location.href = suggestion.url;
                }
            }
        });

    } else {
        $('#1511964230_yith-s').autocomplete({
            minChars: 3,
            appendTo: '.1511964230_container',
            serviceUrl: woocommerce_params.ajax_url + '?action=yith_ajax_search_products',
            onSearchStart: function(){
                $('.1511964230_container').append('<div class="ux-loading"></div>');
            },
            onSearchComplete: function(){
                $('.1511964230_container .ux-loading').remove();

            },
            onSelect: function (suggestion) {
                if( suggestion.id != -1 ) {
                    window.location.href = suggestion.url;
                }
            }
        });

    }
});
</script>     
        </li><!-- .search-dropdown -->
        
        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4018"><a href="http://global.joyoung.com/">首頁</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-parent-item menu-item-4005"><a href="http://global.joyoung.com/%e5%95%86%e5%93%81%e4%bb%8b%e7%b4%b9/">商品介紹</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-6353"><a href="http://global.joyoung.com/商品分類/%e8%b1%86%e6%bc%bf%e6%a9%9f/">豆漿機</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-6354"><a href="http://global.joyoung.com/商品分類/%e5%85%a8%e8%83%bd%e9%8d%8b/">全能鍋</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-6355"><a href="http://global.joyoung.com/商品分類/%e5%8e%9f%e6%b1%81%e6%a9%9f/">原汁機</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-6356"><a href="http://global.joyoung.com/商品分類/%e6%96%99%e7%90%86%e6%a9%9f/">料理機</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-6357"><a href="http://global.joyoung.com/商品分類/%e5%bf%ab%e7%85%ae%e5%a3%ba/">快煮壺</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-6358"><a href="http://global.joyoung.com/商品分類/%e5%84%aa%e7%b1%b3%e6%a9%9f/">優米機</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-5856"><a href="http://global.joyoung.com/商品分類/%e9%9b%bb%e8%92%b8%e9%8d%8b/">電蒸鍋</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-6359"><a href="http://global.joyoung.com/商品分類/%e8%a3%bd%e9%ba%b5%e6%a9%9f/">製麵機</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-parent-item menu-item-4010"><a href="http://global.joyoung.com/%e6%9c%8d%e5%8b%99%e5%b0%88%e5%8d%80/">服務專區</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4006"><a href="http://global.joyoung.com/qa/">產品Q&#038;A</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3995"><a href="http://global.joyoung.com/?page_id=2866">維修流程</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4019"><a href="http://global.joyoung.com/%e6%9c%8d%e5%8b%99%e5%b0%88%e5%8d%80/%e7%b0%a1%e6%98%93%e6%95%85%e9%9a%9c%e5%88%86%e6%9e%90%e5%8f%8a%e6%8e%92%e9%99%a4%e7%90%86%e9%a3%9f%e8%ad%9c/">簡易故障分析及排除</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4011"><a href="http://global.joyoung.com/%e6%9c%8d%e5%8b%99%e5%b0%88%e5%8d%80/%e7%b6%ad%e4%bf%ae%e6%9c%8d%e5%8b%99%e6%93%9a%e9%bb%9e/">維修服務據點</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4015"><a href="http://global.joyoung.com/%e9%8a%b7%e5%94%ae%e9%80%9a%e8%b7%af/%e5%85%a8%e7%9c%81%e8%b3%bc%e8%b2%b7%e6%93%9a%e9%bb%9e/">全省購買據點</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4016"><a href="http://global.joyoung.com/%e9%8a%b7%e5%94%ae%e9%80%9a%e8%b7%af/%e7%89%b9%e7%b4%84%e7%b6%b2%e7%ab%99%e8%b3%bc%e8%b2%b7/">特約網站購買</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4495"><a href="http://global.joyoung.com/%E7%94%A2%E5%93%81%E4%BF%9D%E5%9B%BA%E8%AA%AA%E6%98%8E/">產品保固</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6570"><a href="http://global.joyoung.com/%e6%9c%8d%e5%8b%99%e5%b0%88%e5%8d%80/download%e4%b8%8b%e8%bc%89%e5%b0%88%e5%8d%80/">下載專區</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-parent-item menu-item-3994"><a href="http://global.joyoung.com/%E7%BE%8E%E5%91%B3%E5%A4%A7%E9%9B%86%E5%90%88/%E5%89%B5%E6%84%8F%E9%A3%9F%E8%AD%9C/">美味健康大集合</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4012"><a href="http://global.joyoung.com/%e7%be%8e%e5%91%b3%e5%a4%a7%e9%9b%86%e5%90%88/%e5%89%b5%e6%84%8f%e9%a3%9f%e8%ad%9c/">創意食譜</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4007"><a href="http://global.joyoung.com/%e5%81%a5%e5%ba%b7%e5%b0%8f%e7%9f%a5%e8%ad%98/">健康小知識</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-has-children menu-parent-item menu-item-4880"><a href="http://global.joyoung.com/%e6%9c%80%e6%96%b0%e6%b6%88%e6%81%af/">最新消息</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-4881"><a title="活動訊息" href="http://global.joyoung.com/category/%e6%9c%80%e6%96%b0%e6%b6%88%e6%81%af/%e6%b4%bb%e5%8b%95%e8%a8%8a%e6%81%af/">活動訊息</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4008"><a href="http://global.joyoung.com/%e6%9c%80%e6%96%b0%e6%b6%88%e6%81%af/%e5%bd%b1%e9%9f%b3%e5%b0%88%e5%8d%80/">影音專區</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4013"><a href="http://global.joyoung.com/%e8%81%af%e7%b5%a1%e6%88%91%e5%80%91/">聯絡我們</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-parent-item menu-item-3996"><a href="http://global.joyoung.com/%E9%97%9C%E6%96%BC%E4%B9%9D%E9%99%BD/">關於九陽</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4017"><a href="http://global.joyoung.com/%e9%97%9c%e6%96%bc%e4%b9%9d%e9%99%bd/">公司概述</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4021"><a href="http://global.joyoung.com/%e9%97%9c%e6%96%bc%e4%b9%9d%e9%99%bd/%e6%ad%b7%e5%8f%b2%e6%b2%bf%e9%9d%a9/">歷史沿革</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4020"><a href="http://global.joyoung.com/%e9%97%9c%e6%96%bc%e4%b9%9d%e9%99%bd/%e5%ae%89%e5%bf%83%e4%bf%9d%e8%ad%89/">安心保證</a></li>
</ul>
</li>

        
        <li class="menu-item menu-account-item menu-item-has-children">
                        <a href="http://global.joyoung.com/my-account/">登入</a>
                                  
        </li>
                </ul>

                <ul class="top-bar-mob">
                         <li class="icon-star menu-item menu-item-type-post_type menu-item-object-page menu-item-4293"><a href="http://global.joyoung.com/%e7%94%a2%e5%93%81%e4%bf%9d%e5%9b%ba%e8%aa%aa%e6%98%8e/">產品保固</a></li>
<li class="icon-facebook menu-item menu-item-type-custom menu-item-object-custom menu-item-3999"><a href="https://www.facebook.com/joyoung.global">九陽健康食尚家粉絲團</a></li>
<li class="icon-heart menu-item menu-item-type-post_type menu-item-object-page menu-item-4187"><a href="http://global.joyoung.com/my-account/wishlist/">我的收藏</a></li>
            
             
            
        </ul>
        
       
            </div><!-- inner -->
</div><!-- #mobile-menu -->

<link rel='stylesheet' id='ninja-forms-style-display-css'  href='http://global.joyoung.com/wp-content/plugins/ninja-forms-style/css/ninja-forms-style-display.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='ninja-forms-display-css'  href='http://global.joyoung.com/wp-content/plugins/ninja-forms/deprecated/css/ninja-forms-display.css?nf_ver=2.9.38&#038;ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='jquery-qtip-css'  href='http://global.joyoung.com/wp-content/plugins/ninja-forms/deprecated/css/qtip.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='jquery-rating-css'  href='http://global.joyoung.com/wp-content/plugins/ninja-forms/deprecated/css/jquery.rating.css?ver=4.4.2' type='text/css' media='all' />
<script type='text/javascript' src='http://global.joyoung.com/wp-includes/js/jquery/jquery.form.min.js?ver=3.37.0'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/page.php?wc-ajax=%%endpoint%%","i18n_view_cart":"\u67e5\u770b\u8cfc\u7269\u8eca","cart_url":"http:\/\/global.joyoung.com\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
/* ]]> */
</script>
<script type='text/javascript' src='//global.joyoung.com/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=2.5.5'></script>
<script type='text/javascript' src='//global.joyoung.com/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/page.php?wc-ajax=%%endpoint%%"};
/* ]]> */
</script>
<script type='text/javascript' src='//global.joyoung.com/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=2.5.5'></script>
<script type='text/javascript' src='//global.joyoung.com/wp-content/plugins/woocommerce/assets/js/jquery-cookie/jquery.cookie.min.js?ver=1.4.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/page.php?wc-ajax=%%endpoint%%","fragment_name":"wc_fragments"};
/* ]]> */
</script>
<script type='text/javascript' src='//global.joyoung.com/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=2.5.5'></script>
<script type='text/javascript' src='http://global.joyoung.com/wp-content/plugins/yith-woocommerce-ajax-search/assets/js/yith-autocomplete.min.js?ver=1.2.7'></script>
<script type='text/javascript' src='http://s0.wp.com/wp-content/js/devicepx-jetpack.js?ver=201637'></script>
<script type='text/javascript' src='http://s.gravatar.com/js/gprofiles.js?ver=2016Sepaa'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var WPGroHo = {"my_hash":""};
/* ]]> */
</script>
<script type='text/javascript' src='http://global.joyoung.com/wp-content/plugins/jetpack/modules/wpgroho.js?ver=4.4.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var ajaxURL = {"ajaxurl":"http:\/\/global.joyoung.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='http://global.joyoung.com/wp-content/themes/flatsome/js/flatsome.min.js?ver=2.7.6'></script>
<script type='text/javascript' src='http://global.joyoung.com/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://global.joyoung.com/wp-includes/js/jquery/ui/datepicker.min.js?ver=1.11.4'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var jpfbembed = {"appid":"249643311490","locale":"zh_TW"};
/* ]]> */
</script>
<script type='text/javascript' src='http://global.joyoung.com/wp-content/plugins/jetpack/_inc/facebook-embed.js'></script>
<script type='text/javascript' src='http://global.joyoung.com/wp-includes/js/wp-embed.min.js?ver=4.4.2'></script>
<script type='text/javascript' src='http://global.joyoung.com/wp-content/plugins/yith-woocommerce-ajax-search/assets/js/devbridge-jquery-autocomplete.min.js?ver=1.2.7'></script>
<script type='text/javascript' src='http://global.joyoung.com/wp-includes/js/underscore.min.js?ver=1.6.0'></script>
<script type='text/javascript' src='http://global.joyoung.com/wp-includes/js/backbone.min.js?ver=1.1.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var ninja_forms_settings = {"ajax_msg_format":"inline","password_mismatch":"\u6240\u63d0\u4f9b\u7684\u5bc6\u78bc\u4e0d\u4e00\u6a23\u3002","plugin_url":"http:\/\/global.joyoung.com\/wp-content\/plugins\/ninja-forms\/deprecated\/","datepicker_args":{"dateFormat":"dd\/mm\/yy"},"currency_symbol":"$","date_format":"dd\/mm\/yy"};
var thousandsSeparator = ",";
var decimalPoint = ".";
var ninja_forms_form_8_settings = {"ajax":"1","hide_complete":"1","clear_complete":"1"};
var ninja_forms_form_8_calc_settings = {"calc_value":"","calc_fields":[]};
var ninja_forms_password_strength = {"empty":"Strength indicator","short":"Very weak","bad":"Weak","good":"Medium","strong":"Strong","mismatch":"Mismatch"};
var thousandsSeparator = ",";
var decimalPoint = ".";
var ninja_forms_form_8_settings = {"ajax":"1","hide_complete":"1","clear_complete":"1"};
var ninja_forms_form_8_calc_settings = {"calc_value":"","calc_fields":[]};
var ninja_forms_password_strength = {"empty":"Strength indicator","short":"Very weak","bad":"Weak","good":"Medium","strong":"Strong","mismatch":"Mismatch"};
/* ]]> */
</script>
<script type='text/javascript' src='http://global.joyoung.com/wp-content/plugins/ninja-forms/deprecated/js/min/ninja-forms-display.min.js?nf_ver=2.9.38&#038;ver=4.4.2'></script>
<script type='text/javascript' src='http://stats.wp.com/e-201637.js' async defer></script>
<script type='text/javascript'>
	_stq = window._stq || [];
	_stq.push([ 'view', {v:'ext',j:'1:4.1.1',blog:'103996708',post:'0',tz:'8',srv:'global.joyoung.com'} ]);
	_stq.push([ 'clickTrackerInit', '103996708', '0' ]);
</script>

</body>
</html>

	
