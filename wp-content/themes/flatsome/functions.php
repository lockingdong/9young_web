<?php
/**
 * Flatsome functions and definitions
 *
 * @package flatsome
 */

require get_template_directory() . '/inc/init.php';

/**
 * Note: Do not add any custom code here. Please use a child theme so that your customizations aren't lost during updates.
 * http://codex.wordpress.org/Child_Themes
 */

/**

* Add new register fields for WooCommerce registration.

*

* @return string Register fields HTML.

*/

// enqueue datepicker script
function my_datepicker() {
if ( !is_admin() ) {
	wp_enqueue_script('jquery-ui-datepicker');
	wp_enqueue_script( 'custom-datepicker', get_stylesheet_directory_uri() . '/js/jquery.ui.datepicker-zh-TW.js', array('jquery'));
	}
}
add_action('wp_enqueue_scripts', 'my_datepicker');

//add datepicker stylesheet
wp_register_style('jquery-custom-style', get_stylesheet_directory_uri().'/css/jquery-ui-1.11.4.custom/jquery-ui.css', array(), '1', 'screen'); 
wp_enqueue_style('jquery-custom-style');