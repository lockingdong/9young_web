<?php
/**
 * WooCommerce Product Retailers
 *
 * This source file is subject to the GNU General Public License v3.0
 * that is bundled with this package in the file license.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@skyverge.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade WooCommerce Product Retailers to newer
 * versions in the future. If you wish to customize WooCommerce Product Retailers for your
 * needs please refer to http://docs.woothemes.com/document/woocommerce-product-retailers/ for more information.
 *
 * @package     WC-Product-Retailers/Classes
 * @author      SkyVerge
 * @copyright   Copyright (c) 2013-2015, SkyVerge, Inc.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Product Retailer
 *
 * @since 1.0
 */
class WC_Retailer {


	/** @var string the retailer name */
	private $name;

	/** @var string the retailer price (defined on a per-product basis) */
	private $price;

	/** @var string the retailer url */
	private $url;

	/** @var object the post object */
	private $post;


	/**
	 * Construct and initialize a product retailer
	 *
	 * @since 1.0
	 * @param int|object retailer ID or post object
	 * @throws Exception if the retailer identified by $id doesn't exist
	 */
	public function __construct( $id ) {

		// load the post object if we don't already have it
		if ( is_object( $id ) ) {
			$post = $id;
		} else {
			$post = get_post( $id );
			if ( ! $post ) throw new Exception( "Retailer does not exist" );
		}

		$this->post = $post;

		$this->name = $post->post_title;

		// get the default url, if there is one
		$this->url = get_post_meta( $post->ID, '_product_retailer_default_url', true );
	}


	/**
	 * Returns true if this retailer is available for display on the frontend
	 *
	 * @since 1.0
	 * @param boolean $is_admin whether this check is from within the admin where
	 *        we don't care about the url
	 * @return boolean true if this retailer is available
	 */
	public function is_available( $is_admin = false ) {

		$url  = $this->get_url();
		$name = $this->get_name();

		return ( $is_admin || ! empty( $url ) ) && ! empty( $name ) && 'publish' == $this->post->post_status;
	}


	/**
	 * Returns the retailer id
	 *
	 * @since 1.0
	 * @return int retailer post id
	 */
	public function get_id() {
		return $this->post->ID;
	}


	/**
	 * Returns the retailer name
	 *
	 * @since 1.0
	 * @return string the retailer name
	 */
	public function get_name() {
		return $this->name;
	}

	/**
	 * Returns the price set for retailer (defined at the per-product level)
	 *
	 * @since 1.1
	 * @return string the retailer price
	 */
	 public function get_price() {
		return $this->price;
	}

	/**
	 * Sets the retailer price
	 *
	 * @since 1.1
	 * @param string $price the price to set
	 */
	public function set_price( $price ) {
		$this->price = $price;
	}

	/**
	 * Returns the retailer url
	 *
	 * @since 1.0
	 * @return string the retailer url
	 */
	public function get_url() {

		// add http:// if missing
		if ( $this->url && null === parse_url( $this->url, PHP_URL_SCHEME ) ) {
			$this->url = 'http://' . $this->url;
		}

		return $this->url;
	}


	/**
	 * Sets the retailer url
	 *
	 * @since 1.0
	 * @param string $url the url to set
	 */
	public function set_url( $url ) {
		$this->url = $url;
	}


	/**
	 * Persist this retailer to the DB
	 *
	 * @since 1.0
	 */
	public function persist() {
		update_post_meta( $this->post->ID, '_product_retailer_default_url',  $this->get_url() );
	}


} // end \WC_Retailer class
