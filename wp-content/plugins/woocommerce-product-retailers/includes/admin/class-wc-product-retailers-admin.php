<?php
/**
 * WooCommerce Product Retailers
 *
 * This source file is subject to the GNU General Public License v3.0
 * that is bundled with this package in the file license.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@skyverge.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade WooCommerce Product Retailers to newer
 * versions in the future. If you wish to customize WooCommerce Product Retailers for your
 * needs please refer to http://docs.woothemes.com/document/woocommerce-product-retailers/ for more information.
 *
 * @package     WC-Product-Retailers/Admin
 * @author      SkyVerge
 * @copyright   Copyright (c) 2013-2015, SkyVerge, Inc.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Retailers Admin Class - handles admin UX
 *
 * @since 1.0
 */
class WC_Product_Retailers_Admin {


	/**
	 * Setup admin class
	 *
	 * @since 1.0
	 */
	public function __construct() {

		// load styles/scripts
		add_action( 'admin_enqueue_scripts', array( $this, 'load_styles_scripts' ) );

		// load WC scripts on the edit retailers page
		add_filter( 'woocommerce_screen_ids', array( $this, 'load_wc_admin_scripts' ) );

		// add global settings (2.0)
		add_filter( 'woocommerce_catalog_settings', array( $this, 'add_global_settings' ) );

		// add global settings (2.1+)
		add_filter( 'woocommerce_product_settings', array( $this, 'add_global_settings' ) );

		// add product tab
		add_action( 'woocommerce_product_write_panel_tabs', array( $this, 'add_product_tab' ), 11 );

		// add product tab data
		add_action( 'woocommerce_product_write_panels', array( $this, 'add_product_tab_options' ), 11 );

		// save product tab data
		add_action( 'woocommerce_process_product_meta_simple',   array( $this, 'save_product_tab_options' ) );
		add_action( 'woocommerce_process_product_meta_variable', array( $this, 'save_product_tab_options' ) );
		add_action( 'woocommerce_process_product_meta_booking',  array( $this, 'save_product_tab_options' ) );

		// add AJAX retailer search
		add_action( 'wp_ajax_wc_product_retailers_search_retailers', array( $this, 'ajax_search_retailers' ) );
	}


	/**
	 * Load admin js/css
	 *
	 * @since 1.0
	 * @param string $hook_suffix
	 */
	public function load_styles_scripts( $hook_suffix ) {
		global $post_type;

		if ( 'wc_product_retailer' == $post_type && 'edit.php' == $hook_suffix ) {
			ob_start();
			?>
			// get rid of the date filter and also the filter button itself, unless there are other filters added
			$( 'select[name="m"]' ).remove();
			if ( ! $('#post-query-submit').siblings('select').size() ) $('#post-query-submit').remove();
			<?php
			$js = ob_get_clean();
			wc_enqueue_js( $js );
		}

		// load admin css/js only on edit product/new product pages
		if ( 'product' == $post_type && ( 'post.php' == $hook_suffix || 'post-new.php' == $hook_suffix ) ) {

			// admin CSS
			wp_enqueue_style( 'wc-product-retailers-admin', wc_product_retailers()->get_plugin_url() . '/assets/css/admin/wc-product-retailers-admin.min.css', array( 'woocommerce_admin_styles' ), WC_Product_Retailers::VERSION );

			// admin JS
			wp_enqueue_script( 'wc-product-retailers-admin', wc_product_retailers()->get_plugin_url() . '/assets/js/admin/wc-product-retailers-admin.min.js', WC_Product_Retailers::VERSION );

			wp_enqueue_script( 'jquery-ui-sortable' );

			// add script data
			$wc_product_retailers_admin_params = array(
				'search_retailers_nonce' => wp_create_nonce( 'search_retailers' ),
			);
			wp_localize_script( 'wc-product-retailers-admin', 'wc_product_retailers_admin_params', $wc_product_retailers_admin_params );
		}

		// load WC CSS on add/edit retailer page
		if ( 'wc_product_retailer' == $post_type ) {
			wp_enqueue_style( 'woocommerce_admin_styles', WC()->plugin_url() . '/assets/css/admin.css' );
		}
	}


	/**
	 * Add settings/export screen ID to the list of pages for WC to load its CSS/JS on
	 *
	 * @since 1.0
	 * @param array $screen_ids
	 * @return array
	 */
	public function load_wc_admin_scripts( $screen_ids ) {

		$screen_ids[] = 'wc_product_retailer';

		return $screen_ids;
	}


	/**
	 * Returns the global settings array for the plugin
	 *
	 * @since 1.0
	 * @return array the global settings
	 */
	public static function get_global_settings() {

		return apply_filters( 'wc_product_retailers_settings', array(
			// section start
			array(
				'name' => __( 'Product Retailers', WC_Product_Retailers::TEXT_DOMAIN ),
				'type' => 'title',
				'desc' => '',
				'id' => 'wc_product_retailer_options',
			),

			// product button text
			array(
				'title'    => __( 'Product Button Text', WC_Product_Retailers::TEXT_DOMAIN ),
				'desc_tip' => __( 'This text will be shown on the dropdown linking to the external product, unless overridden at the product level.', WC_Product_Retailers::TEXT_DOMAIN ),
				'id'       => 'wc_product_retailers_product_button_text',
				'css'      => 'width:200px;',
				'default'  => __( 'Purchase from Retailer', WC_Product_Retailers::TEXT_DOMAIN ),
				'type'     => 'text',
			),

			// catalog button text
			array(
				'title'    => __( 'Catalog Button Text', WC_Product_Retailers::TEXT_DOMAIN ),
				'desc_tip' => __( 'This text will be shown on the catalog page "Add to Cart" button for simple products that are sold through retailers only, unless overridden at the product level.', WC_Product_Retailers::TEXT_DOMAIN ),
				'id'       => 'wc_product_retailers_catalog_button_text',
				'css'      => 'width:200px;',
				'default'  => __( 'View Retailers', WC_Product_Retailers::TEXT_DOMAIN ),
				'type'     => 'text',
			),

			// open in new tab
			array(
				'title'    => __( 'Open retailer links in a new tab', WC_Product_Retailers::TEXT_DOMAIN ),
				'desc'     => __( 'Enable this option to open links to other retailers in a new tab instead of the current one.', WC_Product_Retailers::TEXT_DOMAIN ),
				'id'       => 'wc_product_retailers_enable_new_tab',
				'default'  => '',
				'type'     => 'checkbox',
			),

			// section end
			array( 'type' => 'sectionend', 'id' => 'wc_product_retailer_options' ),
		) );
	}


	/**
	 * Inject global settings into the Settings > Catalog/Products page(s), immediately after the 'Product Data' section
	 *
	 * @since 1.0
	 * @param array $settings associative array of WooCommerce settings
	 * @return array associative array of WooCommerce settings
	 */
	public function add_global_settings( $settings ) {

		$setting_id = SV_WC_Plugin_Compatibility::is_wc_version_gte_2_3() ? 'catalog_options' : 'product_data_options';

		$updated_settings = array();

		foreach ( $settings as $setting ) {

			$updated_settings[] = $setting;

			if ( isset( $setting['id'] ) && $setting_id === $setting['id']
				 && isset( $setting['type'] ) && 'sectionend' === $setting['type'] ) {
				$updated_settings = array_merge( $updated_settings, self::get_global_settings() );
			}
		}

		return $updated_settings;
	}


	/**
	 * Add 'Retailers' tab to product data writepanel
	 *
	 * @since 1.0
	 */
	public function add_product_tab() {

		?><li class="wc-product-retailers-tab wc-product-retailers-options hide_if_external hide_if_grouped"><a href="#wc-product-retailers-data"><?php _e( 'Retailers', WC_Product_Retailers::TEXT_DOMAIN ); ?></a></li><?php
	}


	/**
	 * Add product retailers options to product writepanel
	 *
	 * @since 1.0
	 */
	public function add_product_tab_options() {

		?>
			<div id="wc-product-retailers-data" class="panel woocommerce_options_panel">
				<div class="options_group">
					<?php

					do_action( 'wc_product_retailers_product_options_start' );

					// retailer only purchase
					woocommerce_wp_checkbox(
						array(
							'id'          => '_wc_product_retailers_retailer_only_purchase',
							'label'       => __( 'Retailer Only Purchase', WC_Product_Retailers::TEXT_DOMAIN ),
							'description' => __( 'Enable this to only allow purchase from the listed retailers. The add to cart button will be removed.', WC_Product_Retailers::TEXT_DOMAIN ),
						)
					);

					// show buttons
					woocommerce_wp_checkbox(
						array(
							'id'          => '_wc_product_retailers_use_buttons',
							'label'       => __( 'Use Buttons', WC_Product_Retailers::TEXT_DOMAIN ),
							'description' => __( 'Enable this to use buttons rather than a dropdown for multiple retailers.', WC_Product_Retailers::TEXT_DOMAIN ),
						)
					);

					// product button text
					woocommerce_wp_text_input(
						array(
							'id'          => '_wc_product_retailers_product_button_text',
							'label'       => __( 'Product Button Text', WC_Product_Retailers::TEXT_DOMAIN ),
							'description' => __( 'This text will be shown on the dropdown linking to the external product, or before the buttons if "Use Buttons" is enabled.', WC_Product_Retailers::TEXT_DOMAIN ),
							'desc_tip'    => true,
							'placeholder' => wc_product_retailers()->get_product_button_text(),
						)
					);

					// product button text
					woocommerce_wp_text_input(
						array(
							'id'          => '_wc_product_retailers_catalog_button_text',
							'label'       => __( 'Catalog Button Text', WC_Product_Retailers::TEXT_DOMAIN ),
							'description' => __( 'This text will be shown on the catalog page "Add to Cart" button for simple products that are sold through retailers only.', WC_Product_Retailers::TEXT_DOMAIN ),
							'desc_tip'    => true,
							'placeholder' => wc_product_retailers()->get_catalog_button_text(),
						)
					);

					// show retailers element on product page
					woocommerce_wp_checkbox(
						array(
							'id'          => '_wc_product_retailers_hide',
							'label'       => __( 'Hide Product Retailers', WC_Product_Retailers::TEXT_DOMAIN ),
							'description' => __( 'Enable this to hide the default product retailers buttons/dropdown on the product page.  Useful if you want to display them elsewhere using the shortcode or widget.', WC_Product_Retailers::TEXT_DOMAIN ),
							'default'     => 'no',
						)
					);

					// show retailers element on product page
					woocommerce_wp_checkbox(
						array(
							'id'            => '_wc_product_retailers_hide_if_in_stock',
							'label'         => __( 'Hide Product Retailers if product is in stock', WC_Product_Retailers::TEXT_DOMAIN ),
							'description'   => __( 'Enable this to hide the default product retailers buttons/dropdown from the product page and shortcodes while the product is in stock. Useful if you want to only sell the item through your store unless it is out of stock.', WC_Product_Retailers::TEXT_DOMAIN ),
							'default'       => 'no',
							'wrapper_class' => 'hide_if_variable',
						)
					);

					do_action( 'wc_product_retailers_product_options_end' );
					?>
				</div>
				<div class="options_group">
					<?php $this->add_retailers_table(); ?>
				</div>
			</div>
		<?php
	}


	/**
	 * Add product retailers add/remove table
	 *
	 * @since 1.0
	 */
	private function add_retailers_table() {
		global $post;
		?>
			<table class="widefat wc-product-retailers">
				<thead>
				<tr>
					<th class="check-column"><input type="checkbox"></th>
					<th class="wc-product-retailer-name"><?php _e( 'Retailer', WC_Product_Retailers::TEXT_DOMAIN ); ?></th>
					<th class="wc-product-retailer-price"><?php _e( 'Product Price', WC_Product_Retailers::TEXT_DOMAIN ); ?></th>
					<th class="wc-product-retailer-product-url"><?php _e( 'Product URL', WC_Product_Retailers::TEXT_DOMAIN ); ?></th>
				</tr>
				</thead>
				<tbody>
				<?php
				$retailers = get_post_meta( $post->ID, '_wc_product_retailers', true );
				if ( ! empty( $retailers) ) :
					$index = 0;
					foreach ( $retailers as $retailer ) :

						try {
							// build the retailer object and override the URL as needed
							$_retailer = new WC_Retailer( $retailer['id'] );

							// product URL for retailer
							if ( isset( $retailer['product_url'] ) ) {
								$_retailer->set_url( $retailer['product_url'] );
							}

							// product price for retailer
							if ( isset( $retailer['product_price'] ) ) {
								$_retailer->set_price( $retailer['product_price'] );
							}

							// if the retailer is not available (trashed) exclude it
							if ( ! $_retailer->is_available( true ) ) {
								continue;
							}

							?>
							<tr class="wc-product-retailer">
								<td class="check-column">
									<input type="checkbox" name="select" />
									<input type="hidden" name="_wc_product_retailer_id[<?php echo $index; ?>]" value="<?php echo esc_attr( $_retailer->get_id() ); ?>" />
								</td>
								<td class="wc-product-retailer_name"><?php echo esc_html( $_retailer->get_name() ); ?></td>
								<td class="wc-product-retailer-product-price">
									<input type="text" data-post-id="<?php echo esc_attr( $_retailer->get_id() ); ?>" id="wc-product-retailer-price-<?php echo esc_attr( $_retailer->get_id() ); ?>" name="_wc_product_retailer_product_price[<?php echo $index; ?>]" value="<?php echo esc_attr( $_retailer->get_price() ); ?>" />
								</td>
								<td class="wc-product-retailer-product-url">
									<input type="text" data-post-id="<?php echo esc_attr( $_retailer->get_id() ); ?>" id="wc-product-retailer-product-url-<?php echo esc_attr( $_retailer->get_id() ); ?>" name="_wc_product_retailer_product_url[<?php echo $index; ?>]" value="<?php echo esc_attr( $_retailer->get_url() ); ?>" />
								</td>
							</tr>
							<?php
							$index++;
						} catch ( Exception $e ) { /* retailer does not exist */ }
					endforeach;
				endif;
				?>
				</tbody>
				<tfoot>
				<tr>
					<th colspan="4">
						<img class="help_tip" data-tip='<?php _e( 'Search for a retailer to add to this product. You may add multiple retailers by searching for them first.', WC_Product_Retailers::TEXT_DOMAIN ) ?>' src="<?php echo WC()->plugin_url(); ?>/assets/images/help.png" height="16" width="16" />

						<?php if ( SV_WC_Plugin_Compatibility::is_wc_version_gte_2_3() ): ?>
							<input type="hidden" id="wc-product-retailers-retailer-search" class="wc-retailers-search" name="wc_product_retailers_retailer_search" style="width:300px;"
								data-multiple="true"
								data-placeholder="<?php printf( __( 'Search for a retailer to add%s', WC_Product_Retailers::TEXT_DOMAIN ), '&hellip;' ); ?>"
								data-allow_clear="true"
								data-action="wc_product_retailers_search_retailers"
								data-nonce="<?php echo wp_create_nonce( 'search_retailers' ); ?>"
								/>
						<?php else: ?>
							<select id="wc-product-retailers-retailer-search" name="wc_product_retailers_retailer_search" class="ajax-chosen-select-retailers" multiple="multiple" data-placeholder="<?php printf( __( 'Search for a retailer to add%s', WC_Product_Retailers::TEXT_DOMAIN ), '&hellip;' ); ?>"></select>
						<?php endif; ?>

						<button type="button" class="button button-primary wc-product-retailers-add-retailer"><?php _e( 'Add Retailer', WC_Product_Retailers::TEXT_DOMAIN ); ?></button>
						<button type="button" class="button button-secondary wc-product-retailers-delete-retailer"><?php _e( 'Delete Selected', WC_Product_Retailers::TEXT_DOMAIN ); ?></button>
					</th>
				</tr>
				</tfoot>
			</table>
		<?php
	}


	/**
	 * Save product retailers options at the product level
	 *
	 * @since 1.0
	 * @param int $post_id the ID of the product being saved
	 */
	public function save_product_tab_options( $post_id ) {

		// retailer only purchase?
		update_post_meta(
			$post_id,
			'_wc_product_retailers_retailer_only_purchase',
			isset( $_POST['_wc_product_retailers_retailer_only_purchase'] ) && 'yes' === $_POST['_wc_product_retailers_retailer_only_purchase'] ? 'yes' : 'no'
		);

		// use buttons rather than a dropdown?
		update_post_meta(
			$post_id,
			'_wc_product_retailers_use_buttons',
			isset( $_POST['_wc_product_retailers_use_buttons'] ) && 'yes' === $_POST['_wc_product_retailers_use_buttons'] ? 'yes' : 'no'
		);

		// product button text
		if ( isset( $_POST['_wc_product_retailers_product_button_text'] ) ) {
			update_post_meta( $post_id, '_wc_product_retailers_product_button_text', $_POST['_wc_product_retailers_product_button_text'] );
		}

		// catalog button text
		if ( isset( $_POST['_wc_product_retailers_catalog_button_text'] ) ) {
			update_post_meta( $post_id, '_wc_product_retailers_catalog_button_text', $_POST['_wc_product_retailers_catalog_button_text'] );
		}

		// whether to hide the product retailers
		update_post_meta(
			$post_id,
			'_wc_product_retailers_hide',
			isset( $_POST['_wc_product_retailers_hide'] ) && 'yes' === $_POST['_wc_product_retailers_hide'] ? 'yes' : 'no'
		);

		// whether to hide the product retailers for in stock products
		update_post_meta(
			$post_id,
			'_wc_product_retailers_hide_if_in_stock',
			isset( $_POST['_wc_product_retailers_hide_if_in_stock'] ) && 'yes' === $_POST['_wc_product_retailers_hide_if_in_stock'] ? 'yes' : 'no'
		);

		$retailers = array();

		// persist any retailers assigned to this product
		if ( ! empty( $_POST['_wc_product_retailer_product_url'] ) && is_array( $_POST['_wc_product_retailer_product_url'] ) ) {

			foreach ( $_POST['_wc_product_retailer_product_url'] as $index => $retailer_product_url ) {

				$retailer_id = $_POST['_wc_product_retailer_id'][ $index ];

				$retailer_price = $_POST['_wc_product_retailer_product_price'][ $index ];

				// only save the product URL if it's unique to the product
				$retailers[] = array(
					'id'            => $retailer_id,
					'product_price' => $retailer_price,
					'product_url'   => $retailer_product_url !== get_post_meta( $retailer_id, '_product_retailer_default_url', true ) ? esc_url_raw( $retailer_product_url ) : '',
				);
			}
		}

		update_post_meta( $post_id, '_wc_product_retailers', $retailers );

	}


	/**
	 * Processes the AJAX retailer search on the edit product page
	 *
	 * @since 1.0
	 */
	public function ajax_search_retailers() {

		// security check
		check_ajax_referer( 'search_retailers', 'security' );

		// set response as JSON
		header( 'Content-Type: application/json; charset=utf-8' );

		// get search term
		$term = (string) urldecode( stripslashes( strip_tags( $_GET['term'] ) ) );

		if ( empty( $term ) ) {
			die();
		}

		$args = array(
			'post_type'    => 'wc_product_retailer',
			'post_status'  => 'publish',
			'nopaging'     => true,
		);

		if ( is_numeric( $term ) ) {

			//search by retailer ID
			$args['p'] = $term;

		} else {

			// search by retailer name
			$args['s'] = $term;

		}

		$posts = get_posts( $args );

		$retailers = array();

		// build the set of found retailers
		if ( ! empty( $posts ) ) {

			foreach ( $posts as $post ) {

				$retailers[] = array(
					'id'          => $post->ID,
					'name'        => $post->post_title,
					'product_url' => get_post_meta( $post->ID, '_product_retailer_default_url', true )
				);
			}
		}

		// json encode and return
		echo json_encode( $retailers );

		die;
	}


} // end \WC_Product_Retailers_Admin class
