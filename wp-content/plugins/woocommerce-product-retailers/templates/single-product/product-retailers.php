<?php
/**
 * WooCommerce Product Retailers
 *
 * This source file is subject to the GNU General Public License v3.0
 * that is bundled with this package in the file license.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@skyverge.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade WooCommerce Product Retailers to newer
 * versions in the future. If you wish to customize WooCommerce Product Retailers for your
 * needs please refer to http://docs.woothemes.com/document/woocommerce-product-retailers/ for more information.
 *
 * @package     WC-Product-Retailers/Templates
 * @author      SkyVerge
 * @copyright   Copyright (c) 2013-2015, SkyVerge, Inc.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

/**
 * Product page product retailers dropdown/button
 *
 * @version 1.3
 * @since 1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product;

// hide the retailers selection for variable products
$style = '';
if ( $product->is_type( 'variable' ) ) {
	$style = 'display:none;';
}

?>
<div class="wc-product-retailers-wrap">
<?php

if ( count( $retailers ) > 1 && ! WC_Product_Retailers_Product::use_buttons( $product ) ) :
	?>
	<select name="wc-product-retailers" class="wc-product-retailers" style="<?php echo $style; ?>">
		<option value=""><?php echo esc_html( WC_Product_Retailers_Product::get_product_button_text( $product ) ); ?></option>
		<?php foreach ( $retailers as $retailer ) : ?>
			<option value="<?php echo esc_attr( $retailer->get_url() ); ?>"><?php echo esc_html( $retailer->get_name() ); if ( $retailer->get_price() ) printf( ' - %s', wp_kses_post( wc_price( ( $retailer->get_price() ) ) ) ); ?></option>
		<?php endforeach; ?>
	</select>
	<?php
elseif ( count( $retailers ) > 1 && WC_Product_Retailers_Product::use_buttons( $product ) ) :
	?>
	<?php if ( trim( WC_Product_Retailers_Product::get_product_button_text( $product ) ) ) : ?>
		<p><?php echo esc_html( WC_Product_Retailers_Product::get_product_button_text( $product ) ); ?></p>
	<?php endif; ?>
	<ul class="wc-product-retailers" style="<?php echo $style; ?>">
		<?php foreach ( $retailers as $retailer ) : ?>
			<li><a href="<?php echo esc_attr( $retailer->get_url() ); ?>" <?php if ( $open_in_new_tab ) echo 'target="_blank"'; ?> rel="nofollow" class="wc-product-retailers button alt"><?php echo esc_html( apply_filters( 'wc_product_retailers_button_label', $retailer->get_name() . ( $retailer->get_price() ? sprintf( ' - %s', wp_kses_post( WC_Product_Retailers_Product::wc_price( ( $retailer->get_price() ) ) ) ) : '' ), $retailer, $product ) ); ?></a></li>
		<?php endforeach; ?>
	</ul>
	<?php
else :
	$retailer = current( $retailers );
	?><a href="<?php echo esc_attr( $retailer->get_url() ); ?>" <?php if ( $open_in_new_tab ) echo 'target="_blank"'; ?> rel="nofollow" class="wc-product-retailers button alt" style="<?php echo $style; ?>"><?php echo esc_html( WC_Product_Retailers_Product::get_product_button_text( $product ) ); if ( $retailer->get_price() ) printf( ' - %s', wp_kses_post( wc_price( ( $retailer->get_price() ) ) ) ); ?></a><?php
endif;

?>
</div>
