<?php
/**
 * Plugin Name: WooCommerce Product Retailers
 * Plugin URI: http://www.woothemes.com/products/product-retailers/
 * Description: Allow customers to purchase products from external retailers
 * Author: WooThemes / SkyVerge
 * Author URI: http://www.woothemes.com
 * Version: 1.6.0
 * Text Domain: woocommerce-product-retailers
 * Domain Path: /i18n/languages/
 *
 * Copyright: (c) 2013-2015 SkyVerge, Inc. (info@skyverge.com)
 *
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 *
 * @package   WC-Product-Retailers
 * @author    SkyVerge
 * @copyright Copyright (c) 2013-2015, SkyVerge, Inc.
 * @license   http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Required functions
if ( ! function_exists( 'woothemes_queue_update' ) ) {
	require_once( plugin_dir_path( __FILE__ ) . 'woo-includes/woo-functions.php' );
}

// Plugin updates
woothemes_queue_update( plugin_basename( __FILE__ ), '9766af75222eed8f4fcdf56263685d41', '187888' );

// WC active check
if ( ! is_woocommerce_active() ) {
	return;
}

// Required library class
if ( ! class_exists( 'SV_WC_Framework_Bootstrap' ) ) {
	require_once( plugin_dir_path( __FILE__ ) . 'lib/skyverge/woocommerce/class-sv-wc-framework-bootstrap.php' );
}

SV_WC_Framework_Bootstrap::instance()->register_plugin( '4.0.0', __( 'WooCommerce Product Retailers', 'woocommerce-product-retailers' ), __FILE__, 'init_woocommerce_product_retailers', array( 'minimum_wc_version' => '2.2', 'backwards_compatible' => '4.0.0' ) );

function init_woocommerce_product_retailers() {

/**
 * ## WooCommerce Product Retailers Plugin Class
 *
 * ### Plugin Overview
 *
 * This plugin allows admins to create a list of retailers which can then be
 * assigned to products along with a URL, to be displayed on the frontend
 * product page as a button or dropdown list as an affiliate/external purchase
 * option.  Products can be configured to be purchasable both on site and through
 * the retailers, as well as only through the retailers, resulting in a more full
 * featured "affiliate/external" product functionality that can be used with
 * simple as well as variable product types
 *
 * ### Terminology
 *
 * Despite the plugin name of **product retailers**, **retailers** is used internally
 * to refer to the retailers.
 *
 * ### Admin Considerations
 *
 * This plugin adds a **Retailers** menu item to the "WooCommerce" top level menu
 * where the retailers post type is managed.
 *
 * Global settings for this plugin are added to the Catalog tab under **Product
 * Retailers**
 *
 * Within the product admin a new admin panel named **Retailers** is added to the
 * Product Data panel with overrides for the global settings, and a retailers
 * list for managing retailers for the product.
 *
 * ### Frontend Considerations
 *
 * On the catalog page the **add to cart** button for simple products which are
 * sold only through retailers is altered to link directly to the product page
 * like a variable product, rather than performing an AJAX add to cart.
 *
 * On the product page if there is a single retailer it is displayed as a button
 * with configurable text.  If there is more than one retailer, they are displayed
 * as a dropdown list, which when selected redirects the client on to the
 * configured URL for purchase.
 *
 * Product retailers for a product can also be displayed anywhere on the frontend
 * with a shortcode named [woocommerce_product_retailers] and a widget.
 *
 * Variations for variable products which are sold only through retailers are
 * displayed regardless of whether a price is configured (usually they are not
 * shown if there is no price set).
 *
 * ### Database
 *
 * #### Options table
 *
 * `wc_product_retailers_version` - the current plugin version, set on install/upgrade
 *
 * `wc_product_retailers_product_button_text` - Text shown on the dropdown/
 *   button linking to the external URL, unless overridden at the product level
 *
 * `wc_product_retailers_catalog_button_text` - Text shown on the catalog page
 *   "Add to Cart" button for simple products which are sold only through retailers,
 *   unless overridden at the product levle.
 *
 * #### Custom Post Type
 *
 * `wc_product_retailer` - A Custom Post Type which represents a retailer
 *
 * #### Retailer CPT Postmeta
 *
 * `_product_retailer_default_url` - (string) optional retailer URL, used
 *   unless overridden at the product level
 *
 * #### Product Postmeta
 *
 * `_wc_product_retailers_retailer_only_purchase` - Indicates whether the product
 *   is available for purchase only from retailers
 *
 * `_wc_product_retailers_product_button_text` - Optionally overrides the
 *   global `wc_product_retailers_product_button_text` setting
 *
 * `_wc_product_retailers_catalog_button_text` - Optionally overrides the
 *   global `wc_product_retailers_catalog_button_text` setting
 *
 * `_wc_product_retailers` - array of assigned retailers, with the following
 * data structure:
 * ```php
 * Array(
 *   id          => (int) retailer id,
 *   product_url => (string) optional product url,
 * )
 * ```
 */
class WC_Product_Retailers extends SV_WC_Plugin {


	/** plugin version number */
	const VERSION = '1.6.0';

	/** @var WC_Product_Retailers single instance of this plugin */
	protected static $instance;

	/** string the plugin id */
	const PLUGIN_ID = 'product_retailers';

	/** plugin text domain */
	const TEXT_DOMAIN = 'woocommerce-product-retailers';

	/** @var WC_Product_Retailers_List the admin retailers list screen */
	private $admin_retailers_list;

	/** @var WC_Product_Retailers_Edit the admin retailers edit screen */
	private $admin_retailers_edit;

	/** @var boolean set to try after the retailer dropdown is rendered on the product page */
	private $retailer_dropdown_rendered = false;

	/** @var \WC_Product_Retailers_Admin instance */
	public $admin;


	/**
	 * Initializes the plugin
	 *
	 * @since 1.0
	 * @see SV_WC_Plugin::__construct()
	*/
	public function __construct() {

		parent::__construct(
			self::PLUGIN_ID,
			self::VERSION,
			self::TEXT_DOMAIN
		);

		// include required files
		$this->includes();

		add_action( 'init', array( $this, 'init' ) );
		add_action( 'init', array( $this, 'include_template_functions' ), 25 );

		// render frontend embedded styles
		add_action( 'wp_print_styles',                array( $this, 'render_embedded_styles' ), 1 );

		// control the loop add to cart buttons for the product retailer products
		add_filter( 'woocommerce_is_purchasable',     array( $this, 'product_is_purchasable' ), 10, 2 );

		add_action( 'woocommerce_init',               array( $this, 'woocommerce_init' ) );
		add_filter( 'woocommerce_product_is_visible', array( $this, 'product_variation_is_visible' ), 1, 2 );

		// register widgets
		add_action( 'widgets_init', array( $this, 'register_widgets' ) );

		// add the product retailers dropdown on the single product page (next to the 'add to cart' button if available)
		add_action( 'woocommerce_after_add_to_cart_button', array( $this, 'add_retailer_dropdown' ) );
		add_action( 'woocommerce_single_product_summary',   array( $this, 'add_retailer_dropdown' ), 35 );

	}


	/**
	 * Initialize translation and taxonomy
	 *
	 * @since 1.0
	 */
	public function init() {

		if ( ! is_admin() || ! defined( 'DOING_AJAX' ) ) {

			// add accordion shortcode
			add_shortcode( 'woocommerce_product_retailers', array( $this, 'product_retailers_shortcode' ) );
		}

		WC_Product_Retailers_Taxonomy::initialize();
	}


	/**
	 * Register product retailers widgets
	 *
	 * @since 1.4
	 */
	public function register_widgets() {

		// load widget
		require_once( $this->get_plugin_path() . '/includes/widgets/class-wc-product-retailers-widget.php' );

		// register widget
		register_widget( 'WC_Product_Retailers_Widget' );
	}


	/**
	 * Product Retailers shortcode.  Renders the product retailers UI element
	 *
	 * @since 1.4
	 * @param array $atts associative array of shortcode parameters
	 * @return string shortcode content
	 */
	public function product_retailers_shortcode( $atts ) {

		require_once( $this->get_plugin_path() . '/includes/shortcodes/class-wc-product-retailers-shortcode.php' );

		return WC_Shortcodes::shortcode_wrapper( array( 'WC_Product_Retailers_Shortcode', 'output' ), $atts );
	}


	/**
	 * Setup after woocommerce is initialized
	 *
	 * @since 1.2
	 */
	public function woocommerce_init() {

		add_filter( 'woocommerce_product_add_to_cart_text', array( $this, 'add_to_cart_text' ), 10, 2 );
	}


	/**
	 * Include required files
	 *
	 * @since 1.0
	 * @see SV_WC_Plugin::includes()
	 */
	private function includes() {

		require_once( $this->get_plugin_path() . '/includes/class-wc-product-retailers-product.php' );
		require_once( $this->get_plugin_path() . '/includes/class-wc-product-retailers-taxonomy.php' );
		require_once( $this->get_plugin_path() . '/includes/class-wc-retailer.php' );

		if ( is_admin() ) {
			$this->admin_includes();
		}

	}


	/**
	 * Include required admin files
	 *
	 * @since 1.0
	 */
	private function admin_includes() {

		require_once( $this->get_plugin_path() . '/includes/admin/class-wc-product-retailers-admin.php' );
		$this->admin = new WC_Product_Retailers_Admin();

		require_once( $this->get_plugin_path() . '/includes/admin/class-wc-product-retailers-list.php' );
		$this->admin_retailers_list = new WC_Product_Retailers_List();

		require_once( $this->get_plugin_path() . '/includes/admin/class-wc-product-retailers-edit.php' );
		$this->admin_retailers_edit = new WC_Product_Retailers_Edit();
	}


	/**
	 * Function used to Init WooCommerce Product Retailers Template Functions
	 * This makes them pluggable by plugins and themes.
	 */
	public function include_template_functions() {
		require_once( $this->get_plugin_path() . '/includes/wc-product-retailers-template-functions.php' );
	}


	/**
	 * Handle localization, WPML compatible
	 *
	 * @since 1.0
	 * @see SV_WC_Plugin::load_translation()
	 */
	public function load_translation() {

		load_plugin_textdomain( 'woocommerce-product-retailers', false, dirname( plugin_basename( $this->get_file() ) ) . '/i18n/languages' );
	}


	/** Admin methods ******************************************************/


	/**
	 * Gets the plugin configuration URL
	 *
	 * @since 1.0-1
	 * @see SV_WC_Plugin::get_settings_url()
	 * @param string $_ unused
	 * @return string plugin settings URL
	 */
	public function get_settings_url( $_ = '' ) {

		return SV_WC_Plugin_Compatibility::is_wc_version_gte_2_3() ? admin_url( 'admin.php?page=wc-settings&tab=products&section=display' ) : admin_url( 'admin.php?page=wc-settings&tab=products' );
	}


	/** Frontend methods ******************************************************/


	/**
	 * Renders the product retailers frontend button/select box styles
	 *
	 * @since 1.0
	 */
	public function render_embedded_styles() {
		global $post;

		if ( is_product() ) {
			$product = wc_get_product( $post->ID );

			if ( WC_Product_Retailers_Product::has_retailers( $product ) ) {
				echo '<style type="text/css">.wc-product-retailers-wrap { padding:1em 0;clear:both; } .wc-product-retailers-wrap ul { list-style: none; } .wc-product-retailers-wrap ul.wc-product-retailers li { margin-bottom:5px; margin-right:5px; overflow: auto; zoom: 1; }</style>';
			}
		}
	}


	/**
	 * Make product variations visible even if they don't have a price, as long
	 * as they are sold only through retailers.
	 *
	 * This is one of the few times where we are altering this filter in a
	 * positive manner, and so we try to hook into it first.
	 *
	 * @since 1.0
	 * @param boolean $visible whether the product is visible
	 * @param int $product_id the product id
	 * @return boolean true if the product is visible, false otherwise.
	 */
	public function product_variation_is_visible( $visible, $product_id ) {

		$product = wc_get_product( $product_id );

		if ( $product->is_type( 'variable' ) &&  WC_Product_Retailers_Product::is_retailer_only_purchase( $product ) ) {
			$visible = true;
		}

		return $visible;
	}


	/**
	 * Marks "retailer only" products as not purchasable
	 *
	 * @since 1.0
	 * @param boolean $purchasable whether the product is purchasable
	 * @param WC_Product $product the product
	 * @return boolean true if $product is purchasable, false otherwise
	 */
	public function product_is_purchasable( $purchasable, $product ) {

		if ( WC_Product_Retailers_Product::is_retailer_only_purchase( $product ) ) {
			$purchasable = false;
		}

		return $purchasable;
	}


	/**
	 * Modify the 'add to cart' text for simple product retailer products which
	 * are sold only through retailers to display the catalog button text.
	 * This is because the customer must select a retailer to purchase
	 *
	 * @since 1.0
	 * @param string $label the 'add to cart' label
	 * @param null|WC_Product $product WC product object in 2.1+, null in 2.0
	 * @return string the 'add to cart' label
	 */
	public function add_to_cart_text( $label, $product = null ) {

		if ( ! $product ) {
			// pre WC 2.1 support
			global $product;
		}

		if ( $product->is_type( 'simple' ) && WC_Product_Retailers_Product::is_retailer_only_purchase( $product ) && WC_Product_Retailers_Product::has_retailers( $product ) ) {
			$label = __( WC_Product_Retailers_Product::get_catalog_button_text( $product ), self::TEXT_DOMAIN );
		}

		return $label;
	}


	/**
	 * Display the product retailers drop down box
	 *
	 * @since 1.0
	 */
	public function add_retailer_dropdown() {
		global $product;

		// get any product retailers
		$retailers = WC_Product_Retailers_Product::get_product_retailers( $product );

		// only add dropdown if retailers have been assigned and it hasn't already been displayed
		if ( $this->retailer_dropdown_rendered || empty( $retailers ) || WC_Product_Retailers_Product::product_retailers_hidden( $product ) || WC_Product_Retailers_Product::product_retailers_hidden_if_in_stock( $product ) ) {
			return;
		}

		$this->retailer_dropdown_rendered = true;

		woocommerce_single_product_product_retailers( $product, $retailers );
	}


	/** Helper methods ******************************************************/


	/**
	 * Main Product Retailers Instance, ensures only one instance is/can be loaded
	 *
	 * @since 1.5.0
	 * @see wc_product_retailers()
	 * @return WC_Product_Retailers
	 */
	public static function instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}


	/**
	 * Returns the plugin name, localized
	 *
	 * @since 1.2
	 * @see SV_WC_Plugin::get_plugin_name()
	 * @return string the plugin name
	 */
	public function get_plugin_name() {
		return __( 'WooCommerce Product Retailers', self::TEXT_DOMAIN );
	}


	/**
	 * Returns __FILE__
	 *
	 * @since 1.2
	 * @see SV_WC_Plugin::get_file
	 * @return string the full path and filename of the plugin file
	 */
	protected function get_file() {
		return __FILE__;
	}


	/**
	 * Gets the global default Product Button text default
	 *
	 * @since 1.0
	 * @return string the default product button text
	 */
	public function get_product_button_text() {
		return get_option( 'wc_product_retailers_product_button_text' );
	}


	/**
	 * Gets the global default Catalog Button text default
	 *
	 * @since 1.0
	 * @return string the default product button text
	 */
	public function get_catalog_button_text() {
		return get_option( 'wc_product_retailers_catalog_button_text' );
	}


	/**
	 * Gets the plugin documentation url
	 *
	 * @since 1.6.0
	 * @see SV_WC_Plugin::get_documentation_url()
	 * @return string documentation URL
	 */
	public function get_documentation_url() {
		return 'http://docs.woothemes.com/document/woocommerce-product-retailers/';
	}

	/**
	 * Gets the plugin support URL
	 *
	 * @since 1.6.0
	 * @see SV_WC_Plugin::get_support_url()
	 * @return string
	 */
	public function get_support_url() {
		return 'http://support.woothemes.com/';
	}


	/** Lifecycle methods ******************************************************/

	/**
	 * Run every time.  Used since the activation hook is not executed when updating a plugin
	 *
	 * @since 1.0
	 * @see SV_WC_Plugin::install
	 */
	protected function install() {

		$this->admin_includes();

		// install default settings
		foreach ( WC_Product_Retailers_Admin::get_global_settings() as $setting ) {

			if ( isset( $setting['default'] ) ) {
				update_option( $setting['id'], $setting['default'] );
			}
		}

	}


} // end \WC_Product_Retailers class


/**
 * Returns the One True Instance of <plugin>
 *
 * @since 1.5.0
 * @return WC_Product_Retailers
 */
function wc_product_retailers() {
	return WC_Product_Retailers::instance();
}


/**
 * The WC_Product_Retailers global object
 *
 * @deprecated 1.5.0
 * @name $wc_product_retailers
 * @global WC_Product_Retailers $GLOBALS['wc_product_retailers']
 */
$GLOBALS['wc_product_retailers'] = wc_product_retailers();


} // init_woocommerce_product_retailers()
