=== WP HTML Mail - WooCommerce ===
Contributors: haet
Tags: email, template, html mail, mail, message, ninja-forms, wp-e-commerce, caldera-forms, wp-e-commerce
Requires at least: 3.9
Tested up to: 4.5.2
Stable tag: 2.0.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Beautiful responsive HTML mails, fully customizable without any coding knowledge

== Description ==
Beautiful responsive HTML mails, fully customizable without any coding knowledge 
Create your own professional email template within a few minutes without any coding skills. 
Easily change texts, colors, fonts, pictures, alignment and see all your changes immediately in the live preview.


== Installation ==
Extract the zip file and just drop the contents in the wp-content/plugins/ directory of your WordPress installation and then activate the Plugin from Plugins page.


== Changelog ==

= 2.0.4 =
* Fixed duplicate headlines in global template mode

= 2.0.3 =
* Fixed a bug with related products
* Fixed password reset email
* Fixed New Account email
* Added German and Brasilian translations


= 2.0.2 =
Restore a few hidden WooCommerce Email settings

= 2.0.1 = 
Fixed a bug causing an error message if an order used for preview has been deleted

= 2.0 = 
Added Drag&Drop Mailbuilder to customize email content

= 1.2.1 =
Updated the admin-new-order template

= 1.2 =
Fixed a warning and improved compatibility with other woocommerce plugins

= 1.1 = 
Added support for custom WooCommerce templates 

= 1.0 =
* initial release

== Upgrade Notice ==

== Frequently Asked Questions ==