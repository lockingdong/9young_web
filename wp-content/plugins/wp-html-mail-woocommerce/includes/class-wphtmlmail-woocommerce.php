<?php if ( ! defined( 'ABSPATH' ) ) exit;


final class WPHTMLMail_Woocommerce
{

    private static $instance;

    
    
    public static function instance(){
        if (!isset(self::$instance) && !(self::$instance instanceof WPHTMLMail_Woocommerce)) {
            self::$instance = new WPHTMLMail_Woocommerce();
        }

        return self::$instance;
    }




    public function __construct(){
        add_filter( 'mce_external_plugins', array( $this,'register_editor_plugins') );
        add_filter( 'mce_buttons', array( $this,'register_editor_buttons') );        
        add_filter( 'tiny_mce_before_init', array( $this, 'customize_editor_toolbar'), 100 );

        add_action( 'admin_enqueue_scripts', array($this, 'enqueue_scripts_and_styles') );
        add_action( 'add_meta_boxes', array( $this, 'setup_meta_boxes' ) );

        add_action( 'save_post', array($this,'save_post'), 20 );

        add_filter( 'haet_mail_enqueue_js_data', array( $this, 'enqueue_js_translations' ) );
        add_filter( 'haet_mail_enqueue_js_data', array( $this, 'enqueue_js_placeholder_values' ) );
        add_filter( 'haet_mail_enqueue_js_data', array( $this, 'enqueue_js_placeholder_menu' ) );
        add_action( 'haet_mailbuilder_create_email', array( $this, 'set_email_default_content' ), 10, 2 );

        add_filter( 'haet_mail_print_content_text', array( $this, 'fill_placeholders_text' ), 10, 3 );
        add_filter( 'haet_mail_print_content_twocol', array( $this, 'fill_placeholders_text' ), 10, 3 );

        $this->replace_email_subjects();
        
    }




    private function replace_email_subjects(){
        $wc_mail = WC_Emails::instance();
        foreach( $wc_mail->emails AS $email_name => $email){
            add_filter( 
                'woocommerce_email_subject_'.strtolower( str_replace('WC_Email_', '',$email_name) ), 
                function( $subject, $order ) use ( $email_name, $email ){
                    $email_id = Haet_Mail_Builder()->get_email_post_id( $email_name );
                    $new_subject = get_post_meta( $email_id, 'subject', true );
                    if( $new_subject ){
                        $settings = array( 
                                'wc_order' => $order, 
                                'wc_email' => $email, 
                                'wc_sent_to_admin' => false,
                                'plain_text' => true
                            );
                        $new_subject = $this->fill_placeholders_text($new_subject, '', $settings);
                        return $new_subject;
                    }
                    return $subject;
                }, 10, 3 );
        }
    }





    public function enqueue_scripts_and_styles($page){
        if( false !== strpos($page, 'post.php')){
            wp_enqueue_style('haet_mailbuilder_woocommerce_css',  HAET_MAIL_WOOCOMMERCE_URL.'/css/mailbuilder.css');
            wp_enqueue_script( 'haet_mailbuilder_woocommerce_js',  HAET_MAIL_WOOCOMMERCE_URL.'/js/mailbuilder.js', array( 'jquery' ) );
        }
    }




    public function customize_editor_toolbar( $initArray ) {  
        global $post;
        if ( $post ){
            $post_type = get_post_type( $post->ID );
            if( 'wphtmlmail_mail' == $post_type && $initArray['selector'] == '#mb_wysiwyg_editor' ){
                $initArray['toolbar1'] .= ',mb_woocommercetext_placeholder';
            }
        }

        return $initArray;  
      
    } 





    public function set_email_default_content( $post_id, $email_name ){
        include('email-default-content.php');
        if( array_key_exists( $email_name, $mailbuilder_content ) )
            update_post_meta( $post_id, 'mailbuilder_json', addslashes( json_encode( $mailbuilder_content[$email_name] ) ) );
        if( array_key_exists( $email_name, $mailbuilder_subject ) )
            update_post_meta( $post_id, 'subject', $mailbuilder_subject[$email_name]);
    }




    function register_editor_buttons($buttons){
        array_push( $buttons, 'mb_woocommercetext_placeholder' );
        return $buttons;
    }





    function register_editor_plugins($plugin_array) {
        global $post;
        if( $post ){
            $post_type = get_post_type( $post->ID );
            if( 'wphtmlmail_mail' == $post_type ){
                $plugin_array['mb_woocommercetext_placeholder'] = HAET_MAIL_WOOCOMMERCE_URL.'/js/contenttype-text-editor-placeholder.js';
            }
        }
        return $plugin_array;
    }




    /**
     * Add PHP data to the main MailBuilder Javascript file using wp_localize_script
     * @param  array $enqueue_data
     * @return array $enqueue_data
     */
    public function enqueue_js_translations( $enqueue_data ){
        $enqueue_data['translations']['placeholder_button']     = __('Placeholder','woocommerce');
        $enqueue_data['translations']['confirm_restore_default_content'] = __('Are you sure you want to restore default content? All your changes will be deleted.','haet_mail');

        return $enqueue_data;
    }




    public function enqueue_js_placeholder_menu( $enqueue_data ){
        if( !array_key_exists( 'text', $enqueue_data['placeholder_menu'] ) )
            $enqueue_data['placeholder_menu']['text'] = array();

        $checkout_fields = $this->get_wc_checkout_fields();
        $billing_fields = array();
        foreach ($checkout_fields['billing'] as $key => $field) {
            if( isset( $field['custom'] ) && $field['custom'] == 1 ) // custom fields are automatically prefixed with field group
                $key = 'billing_'.$key;
            $billing_fields[] = array(
                'text'      => ( isset( $field['label'] ) && $field['label']!= "" ? $field['label'] : '['. strtoupper($key) .']' ),
                'tooltip'   => '['. strtoupper($key) .']',
            );
        }

        $shipping_fields = array();
        foreach ($checkout_fields['shipping'] as $key => $field) {
            if( isset( $field['custom'] ) && $field['custom'] == 1 ) // custom fields are automatically prefixed with field group
                $key = 'shipping_'.$key;
            $shipping_fields[] = array(
                'text'      => ( isset( $field['label'] ) && $field['label']!= "" ? $field['label'] : '['. strtoupper($key) .']' ),
                'tooltip'   => '['. strtoupper($key) .']',
            );
        }

        $order_fields = array(
                    array(
                        'text'      => __('Order Number','haet_mail'),
                        'tooltip'   => '[ORDER_NUMBER]',
                    ),
                    array(
                        'text'      => __('Order Date','haet_mail'),
                        'tooltip'   => '[ORDER_DATE]',
                    ),
                    array(
                        'text'      => __('Edit Order URL','haet_mail'),
                        'tooltip'   => '[EDIT_ORDER_URL]',
                    ),
                    array(
                        'text'      => __('Payment URL','haet_mail'),
                        'tooltip'   => '[PAYMENT_URL]',
                    ),
                    array(
                        'text'      => __('Order Meta','haet_mail'),
                        'tooltip'   => '[ORDER_META]',
                    ),
                    array(
                        'text'      => __('Customer note','woocommerce'),
                        'tooltip'   => '[CUSTOMER_NOTE]',
                    ),
                    array(
                        'text'      => __('Payment Instructions','haet_mail'),
                        'tooltip'   => '[PAYMENT_INSTRUCTIONS]',
                    ),
                );
        foreach ($checkout_fields['order'] as $key => $field) {
            if( $key != 'order_comments' ){
                if( isset( $field['custom'] ) && $field['custom'] == 1 )
                    $key = 'order_custom_'.$key;
                $order_fields[] = array(
                    'text'      => ( isset( $field['label'] ) && $field['label']!= "" ? $field['label'] : '['. strtoupper($key) .']' ),
                    'tooltip'   => '['. strtoupper($key) .']',
                );
            }
        }

        $enqueue_data['placeholder_menu']['text'] = array(
            array(
                'text'      => __('Order','woocommerce'),
                'menu'      => $order_fields
            ),
            array(
                'text'      => __('Customer','woocommerce'),
                'menu'      => array(
                    array(
                        'text'      => __('Billing full name','haet_mail'),
                        'tooltip'   => '[BILLING_FULL_NAME]',
                    ),
                    array(
                        'text'      => __('Customer details','woocommerce'),
                        'tooltip'   => '[CUSTOMER_DETAILS]',
                    ),
                    array(
                        'text'      => __('Billing address','woocommerce'),
                        'tooltip'   => '[BILLING_ADDRESS]',
                    ),
                    array(
                        'text'      => __('Billing fields','haet_mail'),
                        'menu'      => $billing_fields
                    ),
                    array(
                        'text'      => __('Shipping address','woocommerce'),
                        'tooltip'   => '[SHIPPING_ADDRESS]',
                    ),
                    array(
                        'text'      => __('Shipping fields','haet_mail'),
                        'menu'      => $shipping_fields
                    ),
                )
            ),
            array(
                'text'      => __('General','woocommerce'),
                'menu'      => array(
                    array(
                        'text'      => __('Website name','haet_mail'),
                        'tooltip'   => '[WEBSITE_NAME]',
                    ),
                )
            ),
            array(
                'text'      => __('Profile','woocommerce'),
                'menu'      => array(
                    array(
                        'text'      => __('Username','woocommerce'),
                        'tooltip'   => '[USERNAME]',
                    ),
                    array(
                        'text'      => __('New Password','haet_mail'),
                        'tooltip'   => '[NEW_PASSWORD]',
                    ),
                    array(
                        'text'      => __('My account URL','haet_mail'),
                        'tooltip'   => '[MY_ACCOUNT_URL]',
                    ),
                    array(
                        'text'      => __('Reset password URL','haet_mail'),
                        'tooltip'   => '[RESET_PASSWORD_URL]',
                    ),
                )
            )
        );


        return $enqueue_data;
    }





    /**
     * Get size information for all currently-registered image sizes.
     *
     * @global $_wp_additional_image_sizes
     * @uses   get_intermediate_image_sizes()
     * @return array $sizes Data for all currently-registered image sizes.
     */
    public function get_image_sizes() {
        global $_wp_additional_image_sizes;

        $sizes = array();

        foreach ( get_intermediate_image_sizes() as $_size ) {
            if ( in_array( $_size, array('thumbnail', 'medium', 'medium_large', 'large') ) ) {
                $sizes[ $_size ]['width']  = get_option( "{$_size}_size_w" );
                $sizes[ $_size ]['height'] = get_option( "{$_size}_size_h" );
                $sizes[ $_size ]['crop']   = (bool) get_option( "{$_size}_crop" );
            } elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {
                $sizes[ $_size ] = array(
                    'width'  => $_wp_additional_image_sizes[ $_size ]['width'],
                    'height' => $_wp_additional_image_sizes[ $_size ]['height'],
                    'crop'   => $_wp_additional_image_sizes[ $_size ]['crop'],
                );
            }
        }

        return $sizes;
    }





    public function enqueue_js_placeholder_values( $enqueue_data ){
        if( !array_key_exists( 'productstable', $enqueue_data['placeholders'] ) )
            $enqueue_data['placeholders']['productstable'] = array();
        $demo_order = WPHTMLMail_Woocommerce()->get_demo_order();
        $enqueue_data['placeholders']['text'] = $demo_order;
        return $enqueue_data;
    }





    public function setup_meta_boxes(){
        add_meta_box ( 'mb_restore_defaults_metabox', __( 'Restore default email content', 'haet_mail' ), array($this,'render_restore_defaults_meta_box'), 'wphtmlmail_mail', 'side', 'default' );
        add_meta_box ( 'mb_back_to_overview_metabox', __( 'Back to Email settings', 'haet_mail' ), array($this,'render_back_to_overview_meta_box'), 'wphtmlmail_mail', 'side', 'default' );
    }




    public function render_restore_defaults_meta_box(){
        ?>
        <input type="hidden" name="mb_restore_defaults" id="mb_restore_defaults" value="0">
        <a href="#" class="mb-restore-defaults button">
            <span class="dashicons dashicons-image-rotate"></span>
            <?php _e('Restore default template','haet_mail'); ?>
        </a>

        <?php
    }




    public function render_back_to_overview_meta_box(){
        ?>
        <a href="<?php echo Haet_Mail()->get_tab_url('woocommerce'); ?>" class="mb-back-to-overview button">
            <span class="dashicons dashicons-arrow-left-alt2"></span>
            <?php _e('Back to overview','haet_mail'); ?>
        </a>

        <?php
    }




    public static function get_demo_order(  ){
        $plugin_options = get_option('haet_mail_plugin_options');

        if( isset( $plugin_options['woocommerce'] ) && isset( $plugin_options['woocommerce']['preview_order'] ) ){
            $order_id = $plugin_options['woocommerce']['preview_order'];
            $wc_order = wc_get_order( $order_id );
        }

        if( FALSE === $wc_order ){
            // get latest order id
            $args = array (
                'post_type'              => 'shop_order',
                'posts_per_page'         => '1',
                'order'                  => 'DESC',
                'orderby'                => 'ID',
                'post_status'            => 'any',
                'fields'                 => 'ids'
            );

            // The Query
            $query = new WP_Query( $args );
            // The Loop
            if ( $query->have_posts() ) {
                $wc_order = wc_get_order( $query->posts[0] );
            }
        }


        $totals = $wc_order->get_order_item_totals();
        $totals_indexed_array = array();
        foreach ( $totals as $total ) {
            $totals_indexed_array[] = $total;
        }
        $wc_emails = WC_Emails::instance()->get_emails();
        global $post;
        $email_name = get_the_title( $post );

        $current_user = wp_get_current_user();
        $order = WPHTMLMail_Woocommerce()->get_placeholders_text( 
                array(
                    'wc_order' => $wc_order, 
                    'wc_email' => $wc_emails[$email_name], 
                    'wc_sent_to_admin' => ( strpos($email_name, 'Customer') === false ),
                    'user_login' => $current_user->user_login,
                    'user_pass' => 'XXXXXXXXXXXX'
                )
            );

        $order['items'] = WPHTMLMail_Woocommerce()->get_placeholders_productstable( array( 'wc_order' => $wc_order ) );
        $order['totals'] = $totals_indexed_array;

        return $order;
    }





    private function get_wc_checkout_fields(){
        // WooCommerce::session and WooCommerce::customer are only loaded in frontend so they may not be defined here
        if( !WC()->customer ){
            if( !WC()->session ){
                include_once( trailingslashit( dirname( WC_PLUGIN_FILE ) ).'includes/abstracts/abstract-wc-session.php' );
                include_once( trailingslashit( dirname( WC_PLUGIN_FILE ) ).'includes/class-wc-session-handler.php' );
                $session_class  = apply_filters( 'woocommerce_session_handler', 'WC_Session_Handler' );
                WC()->session  = new $session_class();
            }
            WC()->customer = new WC_Customer();
        }
        
        return WC_Checkout::instance()->checkout_fields;
    }




    private function get_placeholders_text( $settings ){
        $wc_email = $settings['wc_email']; 

        $plain_text = false;
        if( isset( $settings['plain_text'] ) && $settings['plain_text'] == true )
            $plain_text = true;

        

        $reset_password_url = '';
        if( isset( $settings['reset_key'] ) &&  isset( $settings['user_login'] ) )
            $reset_password_url = esc_url( add_query_arg( array( 'key' => $settings['reset_key'], 'login' => rawurlencode( $settings['user_login'] ) ), wc_get_endpoint_url( 'lost-password', '', wc_get_page_permalink( 'myaccount' ) ) ) );

        $order = array(
                'website_name'      =>  get_option( 'blogname' ),
                'username'          =>  ( isset( $settings['user_login'] ) ? $settings['user_login'] : '' ),
                'new_password'      =>  ( isset( $settings['user_pass'] ) ? $settings['user_pass'] : '' ),
                'my_account_url'    =>  wc_get_page_permalink( 'myaccount' ),
                'reset_password_url'=>  $reset_password_url,
            );

        if( isset( $settings['wc_order'] )){
            $wc_order = $settings['wc_order'];
            
            ob_start();
            do_action( 'woocommerce_email_order_meta', $wc_order, $settings['wc_sent_to_admin'], false, $wc_email );
            $order_meta = ob_get_clean();

            ob_start();
            // remove customer details and add our own 
            remove_action( 'woocommerce_email_customer_details', array( WC_Emails::instance(), 'email_addresses' ), 20, 3 );
            remove_action( 'woocommerce_email_customer_details', array( WC_Emails::instance(), 'customer_details' ), 10, 3 );
            add_action( 'woocommerce_email_customer_details', array( $this, 'print_customer_details' ), 10, 3 );
            do_action( 'woocommerce_email_customer_details', $wc_order, $settings['wc_sent_to_admin'], false, $wc_email );
            $customer_details = ob_get_clean();

            ob_start();
            do_action( 'woocommerce_email_before_order_table', $wc_order, $settings['wc_sent_to_admin'], false, $wc_email );
            $payment_instructions = ob_get_clean();




            $order['id']                =   $wc_order->id;
            $order['order_number']      =   $wc_order->get_order_number();
            $order['order_date']        =   sprintf( '<time datetime="%s">%s</time>', date_i18n( 'c', strtotime( $wc_order->order_date ) ), date_i18n( wc_date_format(), strtotime( $wc_order->order_date ) ) );
            $order['edit_order_url']    =   esc_url( admin_url( 'post.php?post=' . $wc_order->id . '&action=edit' ) );
            $order['order_meta']        =   $order_meta;
            $order['customer_note']     =   $wc_order->customer_note;
            $order['payment_url']       =   esc_url( $wc_order->get_checkout_payment_url() );

            $order['billing_full_name'] =   $wc_order->get_formatted_billing_full_name();
            $order['customer_details']  =   $customer_details;
            $order['billing_address']   =   $wc_order->get_formatted_billing_address();

            $order['shipping_address']  =   $wc_order->get_formatted_shipping_address();
                    
            $order['payment_instructions'] =   $payment_instructions; // the action woocommerce_email_before_order_table is primary used for payment instructions



            // WooCommerce::session and WooCommerce::customer are only loaded in frontend so they may not be defined here
            if( !WC()->customer ){
                if( !WC()->session ){
                    include_once( trailingslashit( dirname( WC_PLUGIN_FILE ) ).'includes/abstracts/abstract-wc-session.php' );
                    include_once( trailingslashit( dirname( WC_PLUGIN_FILE ) ).'includes/class-wc-session-handler.php' );
                    $session_class  = apply_filters( 'woocommerce_session_handler', 'WC_Session_Handler' );
                    WC()->session  = new $session_class();
                }
                WC()->customer = new WC_Customer();
            }
            
            $checkout_fields = $this->get_wc_checkout_fields();

            foreach ($checkout_fields['billing'] as $key => $field) {
                if( isset( $field['custom'] ) && $field['custom'] == 1 ) // custom fields are automatically prefixed with field group
                    $order['billing_'.$key] = get_post_meta($wc_order->id, $key, true);
                else
                    $order[$key] = get_post_meta($wc_order->id, '_'.$key, true);
            }
            foreach ($checkout_fields['shipping'] as $key => $field) {
                if( isset( $field['custom'] ) && $field['custom'] == 1 ) // custom fields are automatically prefixed with field group
                    $order['shipping_'.$key] = get_post_meta($wc_order->id, $key, true);
                else
                    $order[$key] = get_post_meta($wc_order->id, '_'.$key, true);
            }
            foreach ($checkout_fields['order'] as $key => $field) {
                if( isset( $field['custom'] ) && $field['custom'] == 1 ) // custom fields are automatically prefixed with field group
                    $order['order_custom_'.$key] = get_post_meta($wc_order->id, $key, true);
                else
                    $order[$key] = get_post_meta($wc_order->id, '_'.$key, true);
            }

            if( $plain_text )
                $order['order_date'] = date_i18n( wc_date_format(), strtotime( $wc_order->order_date ) );
        }


        

        return $order;
    }



    /**
     * print customer details
     * the original template prints too much formatting so we create out own here
     */
    public function print_customer_details( $order, $sent_to_admin = false, $plain_text = false ) {
        $fields = array();

        if ( $order->customer_note ) {
            $fields['customer_note'] = array(
                'label' => __( 'Note', 'woocommerce' ),
                'value' => wptexturize( $order->customer_note )
            );
        }
        
        if ( $order->billing_email ) {
            $fields['billing_email'] = array(
                'label' => __( 'Email', 'woocommerce' ),
                'value' => wptexturize( $order->billing_email )
            );
        }

        if ( $order->billing_phone ) {
            $fields['billing_phone'] = array(
                'label' => __( 'Tel', 'woocommerce' ),
                'value' => wptexturize( $order->billing_phone )
            );
        }

        $fields = array_filter( apply_filters( 'woocommerce_email_customer_details_fields', $fields, $sent_to_admin, $order ), array( WC_Emails::instance(), 'customer_detail_field_is_valid' ) );

        ?>
        <?php foreach ( $fields as $field ) : ?>
            <strong><?php echo wp_kses_post( $field['label'] ); ?>:</strong> <span class="text"><?php echo wp_kses_post( $field['value'] ); ?></span><br>
        <?php endforeach; ?>
        <?php
    }




    public function get_placeholders_productstable( $settings ){
        $wc_order = $settings['wc_order']; 

        $wc_items = $wc_order->get_items();

        $image_placeholder_sizes = array();
        $image_sizes = WPHTMLMail_Woocommerce()->get_image_sizes();
        foreach ($image_sizes as $name => $dimensions) {
            if( $dimensions['width']>0 
                && $dimensions['width']<500
                && $dimensions['height']>0
                && $dimensions['height']<500
                ){
                $image_size_placeholder_name = 'photo_'.strtolower( preg_replace('/[^\da-z]/i', '', $name) );
                $image_placeholder_sizes[$image_size_placeholder_name] = $name;
            }
        }
        
        $items = array();

        foreach ( $wc_items as $item_id => $item ) {
            $_product     = apply_filters( 'woocommerce_order_item_product', $wc_order->get_product_from_item( $item ), $item );
            $item_meta    = new WC_Order_Item_Meta( $item, $_product );
            $i = 0;
            if ( apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
                $i = count( $items );
                foreach ( $image_placeholder_sizes as $image_size_placeholder_name => $image_size_name) {
                    $items[ $i ][$image_size_placeholder_name] = '<img src="' . ( $_product->get_image_id() ? current( wp_get_attachment_image_src( $_product->get_image_id(), $image_size_name) ) : wc_placeholder_img_src() ) . '">';
                }

                $items[ $i ]['productname'] = apply_filters( 'woocommerce_order_item_name', $item['name'], $item, false );

                $items[ $i ]['product_link'] = get_permalink( $_product->id );

                $items[ $i ]['sku'] = $_product->get_sku();

                ob_start();
                // allow other plugins to add additional product information here
                do_action( 'woocommerce_order_item_meta_start', $item_id, $item, $wc_order );

                // Variation
                if ( ! empty( $item_meta->meta ) ) {
                    echo nl2br( $item_meta->display( true, true, '_', "\n" ) );
                }
                // File URLs
                $wc_order->display_item_downloads( $item );
                // allow other plugins to add additional product information here
                do_action( 'woocommerce_order_item_meta_end', $item_id, $item, $wc_order );
                $items[ $i ]['itemmeta'] = ob_get_clean();
                        
                $items[ $i ]['quantity'] = apply_filters( 'woocommerce_email_order_item_quantity', $item['qty'], $item );
                $items[ $i ]['price'] = $wc_order->get_formatted_line_subtotal( $item ); 

                $wc_product     = apply_filters( 'woocommerce_order_item_product', $wc_order->get_product_from_item( $item ), $item );
                $items[ $i ]['purchase_note'] = wpautop( do_shortcode( wp_kses_post( get_post_meta( $wc_product->id, '_purchase_note', true ) ) ) );
            }
        }
        return $items;
    }
    




    public function save_post( $post_id ){
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
            return;
            
        if ( ! isset( $_POST[ 'mailbuilder_nonce' ] ) ||
            ! wp_verify_nonce( $_POST[ 'mailbuilder_nonce' ], 'save_mailbuilder' ) )
            return;
        
        if ( ! current_user_can( 'edit_posts' ) )
            return;

        if( isset( $_POST['mb_restore_defaults'] ) && $_POST['mb_restore_defaults'] )
            $this->set_email_default_content( $post_id, get_the_title( $post_id ) );
    }





    public function fill_placeholders_text($html, $element_content, $settings){
        if( array_key_exists('wc_email', $settings) ){
            $order = $this->get_placeholders_text( $settings );
            //Payment instructions contain block elements (h2, h3 & p) so we convert the wrapping <p> tag to a <div>
            $html = preg_replace_callback(
               "/<p(.*)>(\[PAYMENT\_INSTRUCTIONS\])(.*)<\/p>/i",
                function ( $placeholder ) use ( $order ) {
                    if( array_key_exists( 'payment_instructions', $order) )
                        $placeholder_value = '<div '.$placeholder[1].'>'.$order['payment_instructions'].$placeholder[3].'</div>';
                    return $placeholder_value;
                },
                $html
              );

            $html = preg_replace_callback(
               "/\[([a-z0-9\_]*)\]/i",
                function ( $placeholder ) use ( $order ) {
                    if( array_key_exists( strtolower($placeholder[1]), $order) )
                        $placeholder_value = $order[strtolower($placeholder[1])];
                    else
                        $placeholder_value = $placeholder[0];
                    return $placeholder_value;
                },
                $html
              );
        }
        return $html;
    }
}



function WPHTMLMail_Woocommerce()
{
    return WPHTMLMail_Woocommerce::instance();
}

WPHTMLMail_Woocommerce();