<?php 
$mailbuilder_subject = array();
$mailbuilder_content = array();


$productstable_admin = array(
        "mb-edit-productstable-header[0]"   =>  "<p><span style=\"font-family: Arial, Helvetica, sans-serif;\"><strong>Produkt</strong></span></p>",
        "mb-edit-productstable-header-styles[0]"    =>  "{\"border-left-style\":\"none\",\"border-top-style\":\"none\",\"border-right-style\":\"none\",\"border-bottom-style\":\"solid\",\"border-left-color\":\"#000000\",\"border-top-color\":\"#000000\",\"border-right-color\":\"#000000\",\"border-bottom-color\":\"#000000\",\"border-left-width\":\"1px\",\"border-top-width\":\"1px\",\"border-right-width\":\"1px\",\"border-bottom-width\":\"2px\"}",
        "mb-edit-productstable-header[1]"   =>  "",
        "mb-edit-productstable-header-styles[1]"    =>  "{\"border-left-style\":\"none\",\"border-top-style\":\"none\",\"border-right-style\":\"none\",\"border-bottom-style\":\"solid\",\"border-left-color\":\"#000000\",\"border-top-color\":\"#000000\",\"border-right-color\":\"#000000\",\"border-bottom-color\":\"#000000\",\"border-left-width\":\"1px\",\"border-top-width\":\"1px\",\"border-right-width\":\"1px\",\"border-bottom-width\":\"2px\"}",
        "mb-edit-productstable-header[2]"   =>  "<p style=\"text-align: center;\"><span style=\"font-family: Arial, Helvetica, sans-serif;\"><strong>Anzahl</strong></span></p>",
        "mb-edit-productstable-header-styles[2]"    =>  "{\"border-left-style\":\"none\",\"border-top-style\":\"none\",\"border-right-style\":\"none\",\"border-bottom-style\":\"solid\",\"border-left-color\":\"#000000\",\"border-top-color\":\"#000000\",\"border-right-color\":\"#000000\",\"border-bottom-color\":\"#000000\",\"border-left-width\":\"1px\",\"border-top-width\":\"1px\",\"border-right-width\":\"1px\",\"border-bottom-width\":\"2px\",\"width\":\"auto\",\"padding-left\":\"0px\",\"padding-top\":\"0px\",\"padding-right\":\"0px\",\"padding-bottom\":\"0px\"}",
        "mb-edit-productstable-header[3]"   =>  "<p style=\"text-align: right;\"><strong><span style=\"font-family: Arial, Helvetica, sans-serif;\">Preis</span></strong></p>",
        "mb-edit-productstable-header-styles[3]"    =>  "{\"border-left-style\":\"none\",\"border-top-style\":\"none\",\"border-right-style\":\"none\",\"border-bottom-style\":\"solid\",\"border-left-color\":\"#000000\",\"border-top-color\":\"#000000\",\"border-right-color\":\"#000000\",\"border-bottom-color\":\"#000000\",\"border-left-width\":\"1px\",\"border-top-width\":\"1px\",\"border-right-width\":\"1px\",\"border-bottom-width\":\"2px\"}",
        "mb-edit-productstable-body[0]" =>  "<p>[PHOTO_THUMBNAIL]</p>",
        "mb-edit-productstable-body-styles[0]"  =>  "{\"border-left-style\":\"none\",\"border-top-style\":\"none\",\"border-right-style\":\"none\",\"border-bottom-style\":\"solid\",\"border-left-color\":\"#000000\",\"border-top-color\":\"#000000\",\"border-right-color\":\"#000000\",\"border-bottom-color\":\"#727272\",\"border-left-width\":\"1px\",\"border-top-width\":\"1px\",\"border-right-width\":\"1px\",\"border-bottom-width\":\"1px\",\"width\":\"50px\",\"padding-left\":\"0px\",\"padding-top\":\"1px\",\"padding-right\":\"0px\",\"padding-bottom\":\"0px\"}",
        "mb-edit-productstable-body[1]" =>  "<p><span style=\"font-family: Arial, Helvetica, sans-serif; font-size: 14px;\"><strong>[PRODUCTNAME]</strong></span></p>\r\n<p><span style=\"font-size: 12px; font-family: Arial, Helvetica, sans-serif; color: #808080;\">[ITEMMETA]</span></p>",
        "mb-edit-productstable-body-styles[1]"  =>  "{\"border-left-style\":\"none\",\"border-top-style\":\"none\",\"border-right-style\":\"none\",\"border-bottom-style\":\"solid\",\"border-left-color\":\"#000000\",\"border-top-color\":\"#000000\",\"border-right-color\":\"#000000\",\"border-bottom-color\":\"#727272\",\"border-left-width\":\"1px\",\"border-top-width\":\"1px\",\"border-right-width\":\"1px\",\"border-bottom-width\":\"1px\",\"width\":\"auto\",\"padding-left\":\"10px\",\"padding-top\":\"0px\",\"padding-right\":\"0px\",\"padding-bottom\":\"0px\"}",
        "mb-edit-productstable-body[2]" =>  "<p style=\"text-align: center;\">[QUANTITY]</p>",
        "mb-edit-productstable-body-styles[2]"  =>  "{\"border-left-style\":\"none\",\"border-top-style\":\"none\",\"border-right-style\":\"none\",\"border-bottom-style\":\"solid\",\"border-left-color\":\"#000000\",\"border-top-color\":\"#000000\",\"border-right-color\":\"#000000\",\"border-bottom-color\":\"#727272\",\"border-left-width\":\"1px\",\"border-top-width\":\"1px\",\"border-right-width\":\"1px\",\"border-bottom-width\":\"1px\",\"width\":\"40px\",\"padding-left\":\"0px\",\"padding-top\":\"0px\",\"padding-right\":\"0px\",\"padding-bottom\":\"0px\"}",
        "mb-edit-productstable-body[3]" =>  "<p style=\"text-align: right;\"><strong>[PRICE]</strong></p>",
        "mb-edit-productstable-body-styles[3]"  =>  "{\"border-left-style\":\"none\",\"border-top-style\":\"none\",\"border-right-style\":\"none\",\"border-bottom-style\":\"solid\",\"border-left-color\":\"#000000\",\"border-top-color\":\"#000000\",\"border-right-color\":\"#000000\",\"border-bottom-color\":\"#727272\",\"border-left-width\":\"1px\",\"border-top-width\":\"1px\",\"border-right-width\":\"1px\",\"border-bottom-width\":\"1px\",\"width\":\"70px\",\"padding-left\":\"0px\",\"padding-top\":\"0px\",\"padding-right\":\"0px\",\"padding-bottom\":\"0px\"}",
        "mb-edit-productstable-purchasenote[0]" =>  "<p><span style=\"font-size: 12px;\"><em>[PURCHASE_NOTE]</em></span></p>",
        "mb-edit-productstable-purchasenote-styles[0]"  =>  "{\"border-left-style\":\"none\",\"border-top-style\":\"none\",\"border-right-style\":\"none\",\"border-bottom-style\":\"solid\",\"border-left-color\":\"#000000\",\"border-top-color\":\"#000000\",\"border-right-color\":\"#000000\",\"border-bottom-color\":\"#848484\",\"border-left-width\":\"1px\",\"border-top-width\":\"1px\",\"border-right-width\":\"1px\",\"border-bottom-width\":\"2px\",\"width\":\"auto\",\"padding-left\":\"10px\",\"padding-top\":\"0px\",\"padding-right\":\"0px\",\"padding-bottom\":\"0px\"}",
        "mb-edit-totalstable-body[0]"   =>  "",
        "mb-edit-totalstable-body-styles[0]"    =>  "{\"border-left-style\":\"none\",\"border-top-style\":\"none\",\"border-right-style\":\"none\",\"border-bottom-style\":\"none\",\"border-left-color\":\"#000000\",\"border-top-color\":\"#000000\",\"border-right-color\":\"#000000\",\"border-bottom-color\":\"#707070\",\"border-left-width\":\"1px\",\"border-top-width\":\"1px\",\"border-right-width\":\"1px\",\"border-bottom-width\":\"1px\",\"width\":\"auto\",\"padding-left\":\"0px\",\"padding-top\":\"0px\",\"padding-right\":\"0px\",\"padding-bottom\":\"0px\"}",
        "mb-edit-totalstable-body[1]"   =>  "<p style=\"text-align: right;\"><span style=\"font-size: 14px; font-family: Arial, Helvetica, sans-serif;\"><span style=\"color: #000000;\">[LABEL]</span></span></p>",
        "mb-edit-totalstable-body-styles[1]"    =>  "{\"border-left-style\":\"none\",\"border-top-style\":\"none\",\"border-right-style\":\"none\",\"border-bottom-style\":\"solid\",\"border-left-color\":\"#000000\",\"border-top-color\":\"#000000\",\"border-right-color\":\"#000000\",\"border-bottom-color\":\"#707070\",\"border-left-width\":\"1px\",\"border-top-width\":\"1px\",\"border-right-width\":\"1px\",\"border-bottom-width\":\"1px\",\"width\":\"140px\",\"padding-left\":\"0px\",\"padding-top\":\"0px\",\"padding-right\":\"0px\",\"padding-bottom\":\"0px\"}",
        "mb-edit-totalstable-body[2]"   =>  "<p style=\"text-align: right;\"><span style=\"font-size: 14px; font-family: Arial, Helvetica, sans-serif;\"><strong>[VALUE]</strong></span></p>",
        "mb-edit-totalstable-body-styles[2]"    =>  "{\"border-left-style\":\"none\",\"border-top-style\":\"none\",\"border-right-style\":\"none\",\"border-bottom-style\":\"solid\",\"border-left-color\":\"#000000\",\"border-top-color\":\"#000000\",\"border-right-color\":\"#000000\",\"border-bottom-color\":\"#707070\",\"border-left-width\":\"1px\",\"border-top-width\":\"1px\",\"border-right-width\":\"1px\",\"border-bottom-width\":\"1px\",\"width\":\"160px\",\"padding-left\":\"0px\",\"padding-top\":\"0px\",\"padding-right\":\"0px\",\"padding-bottom\":\"0px\"}"
    );

$productstable_customer = array(
        "mb-edit-productstable-header[0]"           => "<p><span style=\"font-family: Arial, Helvetica, sans-serif;\"><strong>" . __( 'Product', 'woocommerce' ) . "</strong></span></p>",
        "mb-edit-productstable-header-styles[0]"    => "{\"border-left-style\":\"none\",\"border-top-style\":\"none\",\"border-right-style\":\"none\",\"border-bottom-style\":\"solid\",\"border-left-color\":\"#000000\",\"border-top-color\":\"#000000\",\"border-right-color\":\"#000000\",\"border-bottom-color\":\"#000000\",\"border-left-width\":\"1px\",\"border-top-width\":\"1px\",\"border-right-width\":\"1px\",\"border-bottom-width\":\"2px\"}",
        "mb-edit-productstable-header[1]"           => "",
        "mb-edit-productstable-header-styles[1]"    => "{\"border-left-style\":\"none\",\"border-top-style\":\"none\",\"border-right-style\":\"none\",\"border-bottom-style\":\"solid\",\"border-left-color\":\"#000000\",\"border-top-color\":\"#000000\",\"border-right-color\":\"#000000\",\"border-bottom-color\":\"#000000\",\"border-left-width\":\"1px\",\"border-top-width\":\"1px\",\"border-right-width\":\"1px\",\"border-bottom-width\":\"2px\"}",
        "mb-edit-productstable-header[2]"           => "<p style=\"text-align: center;\"><span style=\"font-family: Arial, Helvetica, sans-serif;\"><strong>" . __( 'Quantity', 'woocommerce' ) . "</strong></span></p>",
        "mb-edit-productstable-header-styles[2]"    => "{\"border-left-style\":\"none\",\"border-top-style\":\"none\",\"border-right-style\":\"none\",\"border-bottom-style\":\"solid\",\"border-left-color\":\"#000000\",\"border-top-color\":\"#000000\",\"border-right-color\":\"#000000\",\"border-bottom-color\":\"#000000\",\"border-left-width\":\"1px\",\"border-top-width\":\"1px\",\"border-right-width\":\"1px\",\"border-bottom-width\":\"2px\",\"width\":\"auto\",\"padding-left\":\"0px\",\"padding-top\":\"0px\",\"padding-right\":\"0px\",\"padding-bottom\":\"0px\"}",
        "mb-edit-productstable-header[3]"           => "<p style=\"text-align: right;\"><strong><span style=\"font-family: Arial, Helvetica, sans-serif;\">" . __( 'Price', 'woocommerce' ) . "</span></strong></p>",
        "mb-edit-productstable-header-styles[3]"    => "{\"border-left-style\":\"none\",\"border-top-style\":\"none\",\"border-right-style\":\"none\",\"border-bottom-style\":\"solid\",\"border-left-color\":\"#000000\",\"border-top-color\":\"#000000\",\"border-right-color\":\"#000000\",\"border-bottom-color\":\"#000000\",\"border-left-width\":\"1px\",\"border-top-width\":\"1px\",\"border-right-width\":\"1px\",\"border-bottom-width\":\"2px\"}",
        "mb-edit-productstable-body[0]"             => "<p>[PHOTO_THUMBNAIL]</p>",
        "mb-edit-productstable-body-styles[0]"      => "{\"border-left-style\":\"none\",\"border-top-style\":\"none\",\"border-right-style\":\"none\",\"border-bottom-style\":\"solid\",\"border-left-color\":\"#000000\",\"border-top-color\":\"#000000\",\"border-right-color\":\"#000000\",\"border-bottom-color\":\"#727272\",\"border-left-width\":\"1px\",\"border-top-width\":\"1px\",\"border-right-width\":\"1px\",\"border-bottom-width\":\"1px\",\"width\":\"50px\",\"padding-left\":\"0px\",\"padding-top\":\"0px\",\"padding-right\":\"0px\",\"padding-bottom\":\"0px\"}",
        "mb-edit-productstable-body[1]"             => "<p><span style=\"font-family: Arial, Helvetica, sans-serif; font-size: 14px;\"><strong>[PRODUCTNAME]</strong></span></p>\r\n<p><span style=\"font-size: 12px; font-family: Arial, Helvetica, sans-serif; color: #808080;\">[ITEMMETA]</span></p>",
        "mb-edit-productstable-body-styles[1]"      => "{\"border-left-style\":\"none\",\"border-top-style\":\"none\",\"border-right-style\":\"none\",\"border-bottom-style\":\"solid\",\"border-left-color\":\"#000000\",\"border-top-color\":\"#000000\",\"border-right-color\":\"#000000\",\"border-bottom-color\":\"#727272\",\"border-left-width\":\"1px\",\"border-top-width\":\"1px\",\"border-right-width\":\"1px\",\"border-bottom-width\":\"1px\",\"width\":\"auto\",\"padding-left\":\"10px\",\"padding-top\":\"0px\",\"padding-right\":\"0px\",\"padding-bottom\":\"0px\"}",
        "mb-edit-productstable-body[2]"             => "<p style=\"text-align: center;\">[QUANTITY]</p>",
        "mb-edit-productstable-body-styles[2]"      => "{\"border-left-style\":\"none\",\"border-top-style\":\"none\",\"border-right-style\":\"none\",\"border-bottom-style\":\"solid\",\"border-left-color\":\"#000000\",\"border-top-color\":\"#000000\",\"border-right-color\":\"#000000\",\"border-bottom-color\":\"#727272\",\"border-left-width\":\"1px\",\"border-top-width\":\"1px\",\"border-right-width\":\"1px\",\"border-bottom-width\":\"1px\",\"width\":\"40px\",\"padding-left\":\"0px\",\"padding-top\":\"0px\",\"padding-right\":\"0px\",\"padding-bottom\":\"0px\"}",
        "mb-edit-productstable-body[3]"             => "<p style=\"text-align: right;\"><strong>[PRICE]</strong></p>",
        "mb-edit-productstable-body-styles[3]"      => "{\"border-left-style\":\"none\",\"border-top-style\":\"none\",\"border-right-style\":\"none\",\"border-bottom-style\":\"solid\",\"border-left-color\":\"#000000\",\"border-top-color\":\"#000000\",\"border-right-color\":\"#000000\",\"border-bottom-color\":\"#727272\",\"border-left-width\":\"1px\",\"border-top-width\":\"1px\",\"border-right-width\":\"1px\",\"border-bottom-width\":\"1px\",\"width\":\"70px\",\"padding-left\":\"0px\",\"padding-top\":\"0px\",\"padding-right\":\"0px\",\"padding-bottom\":\"0px\"}",
        "mb-edit-totalstable-body[0]"               => "",
        "mb-edit-totalstable-body-styles[0]"        => "{\"border-left-style\":\"none\",\"border-top-style\":\"none\",\"border-right-style\":\"none\",\"border-bottom-style\":\"none\",\"border-left-color\":\"#000000\",\"border-top-color\":\"#000000\",\"border-right-color\":\"#000000\",\"border-bottom-color\":\"#707070\",\"border-left-width\":\"1px\",\"border-top-width\":\"1px\",\"border-right-width\":\"1px\",\"border-bottom-width\":\"1px\",\"width\":\"auto\",\"padding-left\":\"0px\",\"padding-top\":\"0px\",\"padding-right\":\"0px\",\"padding-bottom\":\"0px\"}",
        "mb-edit-totalstable-body[1]"               => "<p style=\"text-align: right;\"><span style=\"font-size: 14px; font-family: Arial, Helvetica, sans-serif;\"><span style=\"color: #000000;\">[LABEL]</span></span></p>",
        "mb-edit-totalstable-body-styles[1]"        => "{\"border-left-style\":\"none\",\"border-top-style\":\"none\",\"border-right-style\":\"none\",\"border-bottom-style\":\"solid\",\"border-left-color\":\"#000000\",\"border-top-color\":\"#000000\",\"border-right-color\":\"#000000\",\"border-bottom-color\":\"#707070\",\"border-left-width\":\"1px\",\"border-top-width\":\"1px\",\"border-right-width\":\"1px\",\"border-bottom-width\":\"1px\",\"width\":\"140px\",\"padding-left\":\"0px\",\"padding-top\":\"0px\",\"padding-right\":\"0px\",\"padding-bottom\":\"0px\"}",
        "mb-edit-totalstable-body[2]"               => "<p style=\"text-align: right;\"><span style=\"font-size: 14px; font-family: Arial, Helvetica, sans-serif;\"><strong>[VALUE]</strong></span></p>",
        "mb-edit-totalstable-body-styles[2]"        => "{\"border-left-style\":\"none\",\"border-top-style\":\"none\",\"border-right-style\":\"none\",\"border-bottom-style\":\"solid\",\"border-left-color\":\"#000000\",\"border-top-color\":\"#000000\",\"border-right-color\":\"#000000\",\"border-bottom-color\":\"#707070\",\"border-left-width\":\"1px\",\"border-top-width\":\"1px\",\"border-right-width\":\"1px\",\"border-bottom-width\":\"1px\",\"width\":\"160px\",\"padding-left\":\"0px\",\"padding-top\":\"0px\",\"padding-right\":\"0px\",\"padding-bottom\":\"0px\"}"
    );
/*************************************
*   WC_Email_New_Order
* ***********************************/
$mailbuilder_subject['WC_Email_New_Order'] = __( '[{site_title}] New customer order ({order_number}) - {order_date}', 'woocommerce' );

// change placeholder names
$mailbuilder_subject['WC_Email_New_Order'] = str_replace( '[{site_title}]', '[WEBSITE_NAME] - ', $mailbuilder_subject['WC_Email_New_Order'] );
$mailbuilder_subject['WC_Email_New_Order'] = str_replace( '{order_number}', '[ORDER_NUMBER]', $mailbuilder_subject['WC_Email_New_Order'] );
$mailbuilder_subject['WC_Email_New_Order'] = str_replace( '{order_date}', '[ORDER_DATE]', $mailbuilder_subject['WC_Email_New_Order'] );


$mailbuilder_content['WC_Email_New_Order'] = array(
        "mb-1459256221516" => array(
                "id"        =>   "mb-1459256221516",
                "type"      =>   "text",
                "content"   => array(
                    "content"   =>  "<h1>" . __( 'New customer order', 'woocommerce' ) . "</h1><p>".sprintf( __( 'You have received an order from %s. The order is as follows:', 'woocommerce' ), "[BILLING_FULL_NAME]" )."</p>"
                )
            ),
        "mb-1460667961059" => array(
                "id"        =>   "mb-1460667961059",
                "type"      =>   "productstable",
                "content"   =>   $productstable_admin
            ),
        "mb-1462224620039" => array(
                "id"        =>   "mb-1462224620039",
                "type"      =>   "text",
                "content"   =>   array(
                    "content"   =>  
                            "<h3 style=\"text-align: center;\">".__( 'Customer details', 'woocommerce' )."</h3>"
                            ."<p style=\"text-align: center;\">[CUSTOMER_DETAILS]</p>"
                )
            ),
        "mb-1459258484139" => array(
                "id"        =>   "mb-1459258484139",
                "type"      =>   "twocol",
                "content"   => array(
                    "col1"      => "<h2>" . __('Billing address','woocommerce') . "</h2><p>[BILLING_ADDRESS]</p>",
                    "col2"      => "<h2 style=\"text-align: right;\">" . __('Shipping address','woocommerce') . "</h2><p style=\"text-align: right;\">[SHIPPING_ADDRESS]</p>"
                )
            ),
    );


/*************************************
*   WC_Email_Cancelled_Order
* ***********************************/
$mailbuilder_subject['WC_Email_Cancelled_Order'] = __( '[{site_title}] Cancelled order ({order_number})', 'woocommerce' );

// change placeholder names
$mailbuilder_subject['WC_Email_Cancelled_Order'] = str_replace( '[{site_title}]', '[WEBSITE_NAME] - ', $mailbuilder_subject['WC_Email_Cancelled_Order'] );
$mailbuilder_subject['WC_Email_Cancelled_Order'] = str_replace( '{order_number}', '[ORDER_NUMBER]', $mailbuilder_subject['WC_Email_Cancelled_Order'] );


$mailbuilder_content['WC_Email_Cancelled_Order'] = array(
        "mb-1459256221516" => array(
                "id"        =>   "mb-1459256221516",
                "type"      =>   "text",
                "content"   => array(
                    "content"   =>  "<h1>" 
                                .str_replace(
                                    '9999999',
                                    '[ORDER_NUMBER]', 
                                    __( 'Cancelled order', 'woocommerce' ) 
                                    ."</h1><p>"
                                    .sprintf( __( 'The order #%d from %s has been cancelled. The order was as follows:', 'woocommerce' ), 9999999, "[BILLING_FULL_NAME]" ) 
                                ) 
                                . "</p>" // use 9999999 as numeric placeholder for sprintf and replace afterwards
                                . "<h2><a class=\"link\" href=\"[EDIT_ORDER_URL]\">"
                                . str_replace(
                                    '9999999',
                                    '[ORDER_NUMBER]',
                                    sprintf( __( 'Order #%s', 'woocommerce'), 9999999 )
                                )
                                ."</a> ([ORDER_DATE])</h2>"
                )
            ),
        "mb-1460667961059" => array(
                "id"        =>   "mb-1460667961059",
                "type"      =>   "productstable",
                "content"   =>   $productstable_admin
            ),
        "mb-1462224620039" => array(
                "id"        =>   "mb-1462224620039",
                "type"      =>   "text",
                "content"   =>   array(
                    "content"   =>  
                            "<h3 style=\"text-align: center;\">".__( 'Customer details', 'woocommerce' )."</h3>"
                            ."<p style=\"text-align: center;\">[CUSTOMER_DETAILS]</p>"
                )
            ),
        "mb-1459258484139" => array(
                "id"        =>   "mb-1459258484139",
                "type"      =>   "twocol",
                "content"   => array(
                    "col1"      => "<h2>" . __('Billing address','woocommerce') . "</h2><p>[BILLING_ADDRESS]</p>",
                    "col2"      => "<h2 style=\"text-align: right;\">" . __('Shipping address','woocommerce') . "</h2><p style=\"text-align: right;\">[SHIPPING_ADDRESS]</p>"
                )
            ),
    );




/*************************************
*   WC_Email_Failed_Order
* ***********************************/
$mailbuilder_subject['WC_Email_Failed_Order'] = __( '[{site_title}] Failed order ({order_number})', 'woocommerce' );

// change placeholder names
$mailbuilder_subject['WC_Email_Failed_Order'] = str_replace( '[{site_title}]', '[WEBSITE_NAME] - ', $mailbuilder_subject['WC_Email_Failed_Order'] );
$mailbuilder_subject['WC_Email_Failed_Order'] = str_replace( '{order_number}', '[ORDER_NUMBER]', $mailbuilder_subject['WC_Email_Failed_Order'] );


$mailbuilder_content['WC_Email_Failed_Order'] = array(
        "mb-1459256221516" => array(
                "id"        =>   "mb-1459256221516",
                "type"      =>   "text",
                "content"   => array(
                    "content"   =>  "<h1>" 
                                .str_replace(
                                    '9999999',
                                    '[ORDER_NUMBER]', 
                                    __( 'Failed order', 'woocommerce' ) 
                                    ."</h1><p>"
                                    .sprintf( __( 'Payment for order #%d from %s has failed. The order was as follows:', 'woocommerce' ), 9999999, "[BILLING_FULL_NAME]" ) 
                                ) 
                                . "</p>" // use 9999999 as numeric placeholder for sprintf and replace afterwards
                                . "<h2><a class=\"link\" href=\"[EDIT_ORDER_URL]\">"
                                . str_replace(
                                    '9999999',
                                    '[ORDER_NUMBER]',
                                    sprintf( __( 'Order #%s', 'woocommerce'), 9999999 )
                                )
                                ."</a> ([ORDER_DATE])</h2>"
                )
            ),
        "mb-1460667961059" => array(
                "id"        =>   "mb-1460667961059",
                "type"      =>   "productstable",
                "content"   =>   $productstable_admin
            ),
        "mb-1462224620039" => array(
                "id"        =>   "mb-1462224620039",
                "type"      =>   "text",
                "content"   =>   array(
                    "content"   =>  
                            "<h3 style=\"text-align: center;\">".__( 'Customer details', 'woocommerce' )."</h3>"
                            ."<p style=\"text-align: center;\">[CUSTOMER_DETAILS]</p>"
                )
            ),
        "mb-1459258484139" => array(
                "id"        =>   "mb-1459258484139",
                "type"      =>   "twocol",
                "content"   => array(
                    "col1"      => "<h2>" . __('Billing address','woocommerce') . "</h2><p>[BILLING_ADDRESS]</p>",
                    "col2"      => "<h2 style=\"text-align: right;\">" . __('Shipping address','woocommerce') . "</h2><p style=\"text-align: right;\">[SHIPPING_ADDRESS]</p>"
                )
            ),
    );





/*************************************
*   WC_Email_Customer_Processing_Order
* ***********************************/
$mailbuilder_subject['WC_Email_Customer_Processing_Order'] = __( 'Your {site_title} order receipt from {order_date}', 'woocommerce' );

// change placeholder names
$mailbuilder_subject['WC_Email_Customer_Processing_Order'] = str_replace( '{site_title}', '[WEBSITE_NAME] - ', $mailbuilder_subject['WC_Email_Customer_Processing_Order'] );
$mailbuilder_subject['WC_Email_Customer_Processing_Order'] = str_replace( '{order_date}', '[ORDER_DATE]', $mailbuilder_subject['WC_Email_Customer_Processing_Order'] );


$mailbuilder_content['WC_Email_Customer_Processing_Order'] = array(
        "mb-1459256221516" => array(
                "id"        =>   "mb-1459256221516",
                "type"      =>   "text",
                "content"   => array(
                    "content"   =>  "<h1>" 
                                .__( 'Thank you for your order', 'woocommerce' ) 
                                ."</h1><p>"
                                .__( 'Your order has been received and is now being processed. Your order details are shown below for your reference:', 'woocommerce' )
                                . "</p>" 
                                . "[PAYMENT_INSTRUCTIONS]"
                                . "<h2>"
                                . str_replace(
                                    '9999999',
                                    '[ORDER_NUMBER]',
                                    sprintf( __( 'Order #%s', 'woocommerce'), 9999999 )
                                )
                                ."</h2>"
                )
            ),
        "mb-1460667961059" => array(
                "id"        =>   "mb-1460667961059",
                "type"      =>   "productstable",
                "content"   =>   $productstable_customer
            ),
        "mb-1462224620039" => array(
                "id"        =>   "mb-1462224620039",
                "type"      =>   "text",
                "content"   =>   array(
                    "content"   =>  
                            "<h3 style=\"text-align: center;\">".__( 'Customer details', 'woocommerce' )."</h3>"
                            ."<p style=\"text-align: center;\">[CUSTOMER_DETAILS]</p>"
                )
            ),
        "mb-1459258484139" => array(
                "id"        =>   "mb-1459258484139",
                "type"      =>   "twocol",
                "content"   => array(
                    "col1"      => "<h2>" . __('Billing address','woocommerce') . "</h2><p>[BILLING_ADDRESS]</p>",
                    "col2"      => "<h2 style=\"text-align: right;\">" . __('Shipping address','woocommerce') . "</h2><p style=\"text-align: right;\">[SHIPPING_ADDRESS]</p>"
                )
            ),
    );





/*************************************
*   WC_Email_Customer_Completed_Order
* ***********************************/
$mailbuilder_subject['WC_Email_Customer_Completed_Order'] = __( 'Your {site_title} order from {order_date} is complete', 'woocommerce' );

// change placeholder names
$mailbuilder_subject['WC_Email_Customer_Completed_Order'] = str_replace( '{site_title}', '[WEBSITE_NAME] - ', $mailbuilder_subject['WC_Email_Customer_Completed_Order'] );
$mailbuilder_subject['WC_Email_Customer_Completed_Order'] = str_replace( '{order_date}', '[ORDER_DATE]', $mailbuilder_subject['WC_Email_Customer_Completed_Order'] );


$mailbuilder_content['WC_Email_Customer_Completed_Order'] = array(
        "mb-1459256221516" => array(
                "id"        =>   "mb-1459256221516",
                "type"      =>   "text",
                "content"   => array(
                    "content"   =>  "<h1>" 
                                .__( 'Your order is complete', 'woocommerce' ) 
                                ."</h1><p>"
                                .sprintf( __( 'Hi there. Your recent order on %s has been completed. Your order details are shown below for your reference:', 'woocommerce' ), "[WEBSITE_NAME]" )
                                . "</p>" 
                                . "<h2>"
                                . str_replace(
                                    '9999999',
                                    '[ORDER_NUMBER]',
                                    sprintf( __( 'Order #%s', 'woocommerce'), 9999999 )
                                )
                                ."</h2>"
                )
            ),
        "mb-1460667961059" => array(
                "id"        =>   "mb-1460667961059",
                "type"      =>   "productstable",
                "content"   =>   $productstable_customer
            ),
        "mb-1462224620039" => array(
                "id"        =>   "mb-1462224620039",
                "type"      =>   "text",
                "content"   =>   array(
                    "content"   =>  
                            "<h3 style=\"text-align: center;\">".__( 'Customer details', 'woocommerce' )."</h3>"
                            ."<p style=\"text-align: center;\">[CUSTOMER_DETAILS]</p>"
                )
            ),
        "mb-1459258484139" => array(
                "id"        =>   "mb-1459258484139",
                "type"      =>   "twocol",
                "content"   => array(
                    "col1"      => "<h2>" . __('Billing address','woocommerce') . "</h2><p>[BILLING_ADDRESS]</p>",
                    "col2"      => "<h2 style=\"text-align: right;\">" . __('Shipping address','woocommerce') . "</h2><p style=\"text-align: right;\">[SHIPPING_ADDRESS]</p>"
                )
            ),
    );





/*************************************
*   WC_Email_Customer_Invoice
* ***********************************/
$mailbuilder_subject['WC_Email_Customer_Invoice'] = __( 'Invoice for order {order_number} from {order_date}', 'woocommerce' );

// change placeholder names
$mailbuilder_subject['WC_Email_Customer_Invoice'] = str_replace( '{order_number}', '[ORDER_NUMBER]', $mailbuilder_subject['WC_Email_Customer_Invoice'] );
$mailbuilder_subject['WC_Email_Customer_Invoice'] = str_replace( '{order_date}', '[ORDER_DATE]', $mailbuilder_subject['WC_Email_Customer_Invoice'] );


$mailbuilder_content['WC_Email_Customer_Invoice'] = array(
        "mb-1459256221516" => array(
                "id"        =>   "mb-1459256221516",
                "type"      =>   "text",
                "content"   => array(
                    "content"   =>  "<h1>" 
                                .str_replace(
                                    '{order_number}',
                                    '[ORDER_NUMBER]', 
                                    __( 'Invoice for order {order_number}', 'woocommerce' )
                                ) 
                                ."</h1><p>"
                                .sprintf( __( 'An order has been created for you on %s. To pay for this order please use the following link: %s', 'woocommerce' ), '[WEBSITE_NAME]', '<a href="[PAYMENT_URL]">' . __( 'pay', 'woocommerce' ) . '</a>' )
                                . "</p>"
                                . "[PAYMENT_INSTRUCTIONS]" 
                                . "<h2>"
                                . str_replace(
                                    '9999999',
                                    '[ORDER_NUMBER]',
                                    sprintf( __( 'Order #%s', 'woocommerce'), 9999999 )
                                )
                                ."</h2>"
                )
            ),
        "mb-1460667961059" => array(
                "id"        =>   "mb-1460667961059",
                "type"      =>   "productstable",
                "content"   =>   $productstable_customer
            ),
        "mb-1462224620039" => array(
                "id"        =>   "mb-1462224620039",
                "type"      =>   "text",
                "content"   =>   array(
                    "content"   =>  
                            "<h3 style=\"text-align: center;\">".__( 'Customer details', 'woocommerce' )."</h3>"
                            ."<p style=\"text-align: center;\">[CUSTOMER_DETAILS]</p>"
                )
            ),
        "mb-1459258484139" => array(
                "id"        =>   "mb-1459258484139",
                "type"      =>   "twocol",
                "content"   => array(
                    "col1"      => "<h2>" . __('Billing address','woocommerce') . "</h2><p>[BILLING_ADDRESS]</p>",
                    "col2"      => "<h2 style=\"text-align: right;\">" . __('Shipping address','woocommerce') . "</h2><p style=\"text-align: right;\">[SHIPPING_ADDRESS]</p>"
                )
            ),
    );





/*************************************
*   WC_Email_Customer_Refunded_Order
* ***********************************/
$mailbuilder_subject['WC_Email_Customer_Refunded_Order'] = __( 'Your {site_title} order from {order_date} has been refunded', 'woocommerce' );

// change placeholder names
$mailbuilder_subject['WC_Email_Customer_Refunded_Order'] = str_replace( '{site_title}', '[WEBSITE_NAME]', $mailbuilder_subject['WC_Email_Customer_Refunded_Order'] );
$mailbuilder_subject['WC_Email_Customer_Refunded_Order'] = str_replace( '{order_date}', '[ORDER_DATE]', $mailbuilder_subject['WC_Email_Customer_Refunded_Order'] );


$mailbuilder_content['WC_Email_Customer_Refunded_Order'] = array(
        "mb-1459256221516" => array(
                "id"        =>   "mb-1459256221516",
                "type"      =>   "text",
                "content"   => array(
                    "content"   =>  "<h1>" 
                                .__( 'Your order has been fully refunded', 'woocommerce' ) 
                                ."</h1>"
                                . "<p>".sprintf( __( 'Hi there. Your order on %s has been refunded.', 'woocommerce' ), '[WEBSITE_NAME]' )."</p>"
                                . "<h2>"
                                . str_replace(
                                    '9999999',
                                    '[ORDER_NUMBER]',
                                    sprintf( __( 'Order #%s', 'woocommerce'), 9999999 )
                                )
                                ."</h2>"
                )
            ),
        "mb-1460667961059" => array(
                "id"        =>   "mb-1460667961059",
                "type"      =>   "productstable",
                "content"   =>   $productstable_customer
            ),
        "mb-1462224620039" => array(
                "id"        =>   "mb-1462224620039",
                "type"      =>   "text",
                "content"   =>   array(
                    "content"   =>  
                            "<h3 style=\"text-align: center;\">".__( 'Customer details', 'woocommerce' )."</h3>"
                            ."<p style=\"text-align: center;\">[CUSTOMER_DETAILS]</p>"
                )
            ),
        "mb-1459258484139" => array(
                "id"        =>   "mb-1459258484139",
                "type"      =>   "twocol",
                "content"   => array(
                    "col1"      => "<h2>" . __('Billing address','woocommerce') . "</h2><p>[BILLING_ADDRESS]</p>",
                    "col2"      => "<h2 style=\"text-align: right;\">" . __('Shipping address','woocommerce') . "</h2><p style=\"text-align: right;\">[SHIPPING_ADDRESS]</p>"
                )
            ),
    );





/*************************************
*   WC_Email_Customer_Note
* ***********************************/
$mailbuilder_subject['WC_Email_Customer_Note'] = __( 'Note added to your {site_title} order from {order_date}', 'woocommerce' );

// change placeholder names
$mailbuilder_subject['WC_Email_Customer_Note'] = str_replace( '{site_title}', '[WEBSITE_NAME]', $mailbuilder_subject['WC_Email_Customer_Note'] );
$mailbuilder_subject['WC_Email_Customer_Note'] = str_replace( '{order_date}', '[ORDER_DATE]', $mailbuilder_subject['WC_Email_Customer_Note'] );


$mailbuilder_content['WC_Email_Customer_Note'] = array(
        "mb-1459256221516" => array(
                "id"        =>   "mb-1459256221516",
                "type"      =>   "text",
                "content"   => array(
                    "content"   =>  "<h1>" 
                                .__( 'A note has been added to your order', 'woocommerce' ) 
                                ."</h1>"
                                . "<p>".__( 'Hello, a note has just been added to your order:', 'woocommerce' )."</p>"
                                . "<p>[CUSTOMER_NOTE]</p>"
                                . "<p>".__( 'For your reference, your order details are shown below.', 'woocommerce' )."</p>"
                                . "<h2>"
                                . str_replace(
                                    '9999999',
                                    '[ORDER_NUMBER]',
                                    sprintf( __( 'Order #%s', 'woocommerce'), 9999999 )
                                )
                                ."</h2>"
                )
            ),
        "mb-1460667961059" => array(
                "id"        =>   "mb-1460667961059",
                "type"      =>   "productstable",
                "content"   =>   $productstable_customer
            ),
        "mb-1462224620039" => array(
                "id"        =>   "mb-1462224620039",
                "type"      =>   "text",
                "content"   =>   array(
                    "content"   =>  
                            "<h3 style=\"text-align: center;\">".__( 'Customer details', 'woocommerce' )."</h3>"
                            ."<p style=\"text-align: center;\">[CUSTOMER_DETAILS]</p>"
                )
            ),
        "mb-1459258484139" => array(
                "id"        =>   "mb-1459258484139",
                "type"      =>   "twocol",
                "content"   => array(
                    "col1"      => "<h2>" . __('Billing address','woocommerce') . "</h2><p>[BILLING_ADDRESS]</p>",
                    "col2"      => "<h2 style=\"text-align: right;\">" . __('Shipping address','woocommerce') . "</h2><p style=\"text-align: right;\">[SHIPPING_ADDRESS]</p>"
                )
            ),
    );


/*************************************
*   WC_Email_Customer_Reset_Password
* ***********************************/
$mailbuilder_subject['WC_Email_Customer_Reset_Password'] = __( 'Password Reset for {site_title}', 'woocommerce' );

// change placeholder names
$mailbuilder_subject['WC_Email_Customer_Reset_Password'] = str_replace( '{site_title}', '[WEBSITE_NAME]', $mailbuilder_subject['WC_Email_Customer_Reset_Password'] );


$mailbuilder_content['WC_Email_Customer_Reset_Password'] = array(
        "mb-1459256221516" => array(
                "id"        =>   "mb-1459256221516",
                "type"      =>   "text",
                "content"   => array(
                    "content"   =>  "<h1>" 
                                .__( 'Password Reset Instructions', 'woocommerce' ) 
                                . "</h1>"
                                . "<p>".__( 'Someone requested that the password be reset for the following account:', 'woocommerce' )."</p>"
                                . "<p>".sprintf( __( 'Username: %s', 'woocommerce' ), '[USERNAME]' ).'</p>'
                                . "<p>".__( 'If this was a mistake, just ignore this email and nothing will happen.', 'woocommerce' ).'</p>'
                                . "<p>".__( 'To reset your password, visit the following address:', 'woocommerce' ).'</p>'
                                . "<p><a class=\"link\" href=\"[RESET_PASSWORD_URL]\">"
                                . __( 'Click here to reset your password', 'woocommerce' )
                                . "</a></p><p></p>"
                )
            ),
    );





/*************************************
*   WC_Email_Customer_New_Account
* ***********************************/
$mailbuilder_subject['WC_Email_Customer_New_Account'] = __( 'Your account on {site_title}', 'woocommerce' );

// change placeholder names
$mailbuilder_subject['WC_Email_Customer_New_Account'] = str_replace( '{site_title}', '[WEBSITE_NAME]', $mailbuilder_subject['WC_Email_Customer_New_Account'] );


$mailbuilder_content['WC_Email_Customer_New_Account'] = array(
        "mb-1459256221516" => array(
                "id"        =>   "mb-1459256221516",
                "type"      =>   "text",
                "content"   => array(
                    "content"   =>  "<h1>" 
                                . str_replace( '{site_title}', '[WEBSITE_NAME]', __( 'Welcome to {site_title}', 'woocommerce' ) )
                                . "</h1>"
                                . "<p>".sprintf( __( "Thanks for creating an account on %s. Your username is <strong>%s</strong>.", 'woocommerce' ), '[WEBSITE_NAME]', '[USERNAME]' ).'</p>'
                                . "<p>".sprintf( __( "Your password has been automatically generated: <strong>%s</strong>", 'woocommerce' ), '[NEW_PASSWORD]' ).'</p>'
                                . "<p>".sprintf( __( 'You can access your account area to view your orders and change your password here: %s.', 'woocommerce' ), '[MY_ACCOUNT_URL]' ).'</p>'
                                . "<p></p>"
                )
            ),
    );