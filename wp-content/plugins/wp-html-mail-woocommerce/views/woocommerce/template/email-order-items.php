<?php
/**
 * Email Order Items
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.1.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $haet_mail_options;
global $haet_mail_plugin_options;
global $current_mail_template;


if( $sent_to_admin ){
	if( $haet_mail_plugin_options['woocommerce']['thumbs_admin'] )
		$show_image = true;
	else
		$show_image = false;
}else{
	if( $haet_mail_plugin_options['woocommerce']['thumbs_customer'] )
		$show_image = true;
	else
		$show_image = false;
}

if( isset($haet_mail_plugin_options['woocommerce']['thumb_size']) )
	$image_size = array($haet_mail_plugin_options['woocommerce']['thumb_size'],$haet_mail_plugin_options['woocommerce']['thumb_size']);


foreach ( $items as $item_id => $item ) :
	$_product     = apply_filters( 'woocommerce_order_item_product', $order->get_product_from_item( $item ), $item );
	$item_meta    = new WC_Order_Item_Meta( $item, $_product );

	if ( apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
		?>
		<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_order_item_class', 'order_item', $item, $order ) ); ?>">
			<td class="col-product">
				<?php if ( $show_image ) { ?>

				<table class="product-left-col" >
					<tr>

						<td class="image-wrap" style="width:<?php echo $image_size[0];?>px">
							<?php
								if( $_product->get_image_id() ){
									$image = wp_get_attachment_image_src( $_product->get_image_id(), $image_size );
									$image_src = $image[0];
									$current_image_size = array($image[1],$image[2]);
								}else{
									$image_src = wc_placeholder_img_src();
									$current_image_size = $image_size;
								}
								// Show title/image etc
								echo apply_filters( 'woocommerce_order_item_thumbnail', '<img src="' . $image_src .'" alt="' . __( 'Product Image', 'woocommerce' ) . '" height="' . esc_attr( $current_image_size[1] ) . '" width="' . esc_attr( $current_image_size[0] ) . '" style="vertical-align:middle; margin-right: 10px;" />', $item );
							?>							
						</td>
						<td class="product-name" style="vertical-align:middle;">
				<?php } 
					// Product name
					echo apply_filters( 'woocommerce_order_item_name', $item['name'], $item );

					// SKU
					if ( $show_sku && is_object( $_product ) && $_product->get_sku() ) {
						echo ' (#' . $_product->get_sku() . ')';
					}

					// allow other plugins to add additional product information here
					do_action( 'woocommerce_order_item_meta_start', $item_id, $item, $order );

					// Variation
					if ( ! empty( $item_meta->meta ) ) {
						echo '<br/><span class="variation">' . nl2br( $item_meta->display( true, true, '_', "\n" ) ) . '</span>';
					}

					// File URLs
					if ( $show_download_links && is_object( $_product ) && $_product->exists() && $_product->is_downloadable() ) {

						$download_files = $order->get_item_downloads( $item );
						$i              = 0;

						foreach ( $download_files as $download_id => $file ) {
							$i++;

							if ( count( $download_files ) > 1 ) {
								$prefix = sprintf( __( 'Download %d', 'woocommerce' ), $i );
							} elseif ( $i == 1 ) {
								$prefix = __( 'Download', 'woocommerce' );
							}

							echo '<br/><small>' . $prefix . ': <a href="' . esc_url( $file['download_url'] ) . '" target="_blank">' . esc_html( $file['name'] ) . '</a></small>';
						}
					}

					// allow other plugins to add additional product information here
					do_action( 'woocommerce_order_item_meta_end', $item_id, $item, $order );

				if ( $show_image ) { ?>
							</td>
						</tr>
					</table>
				<?php } ?>
			</td>
			<td class="col-quantity"><?php echo $item['qty'] ;?></td>
			<td class="col-price"><?php echo $order->get_formatted_line_subtotal( $item ); ?></td>
		</tr>
		<?php
	}

	if ( $show_purchase_note && is_object( $_product ) && ( $purchase_note = get_post_meta( $_product->id, '_purchase_note', true ) ) ) : ?>
		<tr>
			<td colspan="3" style="text-align:left; vertical-align:middle;"><?php echo wpautop( do_shortcode( wp_kses_post( $purchase_note ) ) ); ?></td>
		</tr>
	<?php endif; ?>

<?php endforeach; ?>
<?php if( isset( $current_mail_template ) ) $current_mail_template = ''; ?> 


