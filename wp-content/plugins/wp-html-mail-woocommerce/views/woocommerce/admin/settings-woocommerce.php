<div class="postbox">
    <div style="" class="inside">
        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row"><label><?php _e('How do you want to edit your WooCommerce emails?','haet_mail'); ?></label></th>
                    <td class="">
                        <input type="radio" name="haet_mail_plugins[woocommerce][edit_mode]" id="haet_mail_plugins_woocommerce_edit_mode_global" <?php echo (!isset($plugin_options['woocommerce']['edit_mode']) || $plugin_options['woocommerce']['edit_mode']=='global' ?'checked':''); ?> value="global">
                        <label for="haet_mail_plugins_woocommerce_edit_mode_global">
                            <?php _e('Set one global template and use WooCommerce default content.','haet_mail'); ?>
                        </label>
                        <br>
                        <input type="radio" name="haet_mail_plugins[woocommerce][edit_mode]" id="haet_mail_plugins_woocommerce_edit_mode_mailbuilder" <?php echo (isset($plugin_options['woocommerce']['edit_mode']) && $plugin_options['woocommerce']['edit_mode']=='mailbuilder' ?'checked':''); ?> value="mailbuilder">
                        <label for="haet_mail_plugins_woocommerce_edit_mode_mailbuilder">
                            <?php _e('Customize each email individually.','haet_mail'); ?>
                        </label>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="postbox haet-mail-woocommerce-global-template">
    <h3 class="hndle"><span><?php _e('WooCommerce global template','haet_mail'); ?></span></h3>
    <div style="" class="inside">
        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row"><label><?php _e('Show product thumbnails','haet_mail'); ?></label></th>
                    <td>
                        <input type="hidden" name="haet_mail_plugins[woocommerce][thumbs_customer]" value="0">
                        <input type="checkbox" id="haet_mail_plugins_woocommerce_thumbs_customer" name="haet_mail_plugins[woocommerce][thumbs_customer]" value="1" <?php echo (isset($plugin_options['woocommerce']['thumbs_customer']) && $plugin_options['woocommerce']['thumbs_customer']==1 || !isset($plugin_options['woocommerce'])?'checked':''); ?>>
                        <label for="haet_mail_plugins_woocommerce_thumbs_customer"><?php _e('for customers','haet_mail'); ?></label><br>

                        <input type="hidden" name="haet_mail_plugins[woocommerce][thumbs_admin]" value="0">
                        <input type="checkbox" id="haet_mail_plugins_woocommerce_thumbs_admin" name="haet_mail_plugins[woocommerce][thumbs_admin]" value="1" <?php echo (isset($plugin_options['woocommerce']['thumbs_admin']) && $plugin_options['woocommerce']['thumbs_admin']==1 || !isset($plugin_options['woocommerce'])?'checked':''); ?>>
                        <label for="haet_mail_plugins_woocommerce_thumbs_admin"><?php _e('for admins','haet_mail'); ?></label><br>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row"><label><?php _e('Thumbnail size','haet_mail'); ?></label></th>
                    <td>
                        <input type="number" id="haet_mail_plugins_woocommerce_thumb_size" name="haet_mail_plugins[woocommerce][thumb_size]" value="<?php echo (isset($plugin_options['woocommerce']['thumb_size'])?$plugin_options['woocommerce']['thumb_size']:'32'); ?>" style="width:60px;">
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="haet_mail_plugins_woocommerce_headline_font"><?php _e('Table Headline Font','haet_mail'); ?></label>
                    </th>
                    <td>
                        <select  id="haet_mail_plugins_woocommerce_headline_font" name="haet_mail_plugins[woocommerce][headlinefont]">
                            <?php foreach ($fonts as $font => $display_name) :?>
                                <option value="<?php echo $font; ?>" <?php echo ($plugin_options['woocommerce']['headlinefont']==$font?'selected':''); ?>><?php echo $display_name; ?></option>     
                            <?php endforeach; ?>
                        </select>
                        
                        <select  id="haet_mail_plugins_woocommerce_headline_fontsize" name="haet_mail_plugins[woocommerce][headlinefontsize]">
                            <?php for ($fontsize=12; $fontsize<=50; $fontsize++) :?>
                                <option value="<?php echo $fontsize; ?>" <?php echo ($plugin_options['woocommerce']['headlinefontsize']==$fontsize?'selected':''); ?>><?php echo $fontsize; ?>px</option>       
                            <?php endfor; ?>
                        </select>

                        <input type="text" class="color" id="haet_mail_plugins_woocommerce_headline_color" name="haet_mail_plugins[woocommerce][headlinecolor]" value="<?php echo $plugin_options['woocommerce']['headlinecolor']; ?>">

                        <input type="hidden" name="haet_mail_plugins[woocommerce][headlinebold]" value="0">
                        <input type="checkbox" id="haet_mail_plugins_woocommerce_headline_bold" name="haet_mail_plugins[woocommerce][headlinebold]" value="1" <?php echo ($plugin_options['woocommerce']['headlinebold']==1?'checked':''); ?>>
                        <label for="haet_mail_plugins_woocommerce_headline_bold"><?php _e('Bold','haet_mail'); ?></label>
                        
                        &nbsp;&nbsp; 
                        
                        <input type="hidden" name="haet_mail_plugins[woocommerce][headlineitalic]" value="0">
                        <input type="checkbox" id="haet_mail_plugins_woocommerce_headline_italic" name="haet_mail_plugins[woocommerce][headlineitalic]" value="1" <?php echo ($plugin_options['woocommerce']['headlineitalic']==1?'checked':''); ?>>
                        <label for="haet_mail_plugins_woocommerce_headline_italic"><?php _e('Italic','haet_mail'); ?></label>
                    </td>
                </tr>

                <tr>
                    <th scope="row">
                        <label for="haet_mail_plugins_woocommerce_content_font"><?php _e('Table Content Font','haet_mail'); ?></label>
                    </th>
                    <td>
                        <select  id="haet_mail_plugins_woocommerce_content_font" name="haet_mail_plugins[woocommerce][contentfont]">
                            <?php foreach ($fonts as $font => $display_name) :?>
                                <option value="<?php echo $font; ?>" <?php echo ($plugin_options['woocommerce']['contentfont']==$font?'selected':''); ?>><?php echo $display_name; ?></option>     
                            <?php endforeach; ?>
                        </select>
                        
                        <select  id="haet_mail_plugins_woocommerce_content_fontsize" name="haet_mail_plugins[woocommerce][contentfontsize]">
                            <?php for ($fontsize=12; $fontsize<=50; $fontsize++) :?>
                                <option value="<?php echo $fontsize; ?>" <?php echo ($plugin_options['woocommerce']['contentfontsize']==$fontsize?'selected':''); ?>><?php echo $fontsize; ?>px</option>       
                            <?php endfor; ?>
                        </select>

                        <input type="text" class="color" id="haet_mail_plugins_woocommerce_content_color" name="haet_mail_plugins[woocommerce][contentcolor]" value="<?php echo $plugin_options['woocommerce']['contentcolor']; ?>">

                        <input type="hidden" name="haet_mail_plugins[woocommerce][contentbold]" value="0">
                        <input type="checkbox" id="haet_mail_plugins_woocommerce_content_bold" name="haet_mail_plugins[woocommerce][contentbold]" value="1" <?php echo ($plugin_options['woocommerce']['contentbold']==1?'checked':''); ?>>
                        <label for="haet_mail_plugins_woocommerce_content_bold"><?php _e('Bold','haet_mail'); ?></label>
                        
                        &nbsp;&nbsp; 
                        
                        <input type="hidden" name="haet_mail_plugins[woocommerce][contentitalic]" value="0">
                        <input type="checkbox" id="haet_mail_plugins_woocommerce_content_italic" name="haet_mail_plugins[woocommerce][contentitalic]" value="1" <?php echo ($plugin_options['woocommerce']['contentitalic']==1?'checked':''); ?>>
                        <label for="haet_mail_plugins_woocommerce_content_italic"><?php _e('Italic','haet_mail'); ?></label>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="haet_mail_plugins_woocommerce_variation_font"><?php _e('Variation Font','haet_mail'); ?></label>
                    </th>
                    <td>
                        <select  id="haet_mail_plugins_woocommerce_variation_font" name="haet_mail_plugins[woocommerce][variationfont]">
                            <?php foreach ($fonts as $font => $display_name) :?>
                                <option value="<?php echo $font; ?>" <?php echo ($plugin_options['woocommerce']['variationfont']==$font?'selected':''); ?>><?php echo $display_name; ?></option>     
                            <?php endforeach; ?>
                        </select>
                        
                        <select  id="haet_mail_plugins_woocommerce_variation_fontsize" name="haet_mail_plugins[woocommerce][variationfontsize]">
                            <?php for ($fontsize=12; $fontsize<=50; $fontsize++) :?>
                                <option value="<?php echo $fontsize; ?>" <?php echo ($plugin_options['woocommerce']['variationfontsize']==$fontsize?'selected':''); ?>><?php echo $fontsize; ?>px</option>       
                            <?php endfor; ?>
                        </select>

                        <input type="text" class="color" id="haet_mail_plugins_woocommerce_variation_color" name="haet_mail_plugins[woocommerce][variationcolor]" value="<?php echo $plugin_options['woocommerce']['variationcolor']; ?>">

                        <input type="hidden" name="haet_mail_plugins[woocommerce][variationbold]" value="0">
                        <input type="checkbox" id="haet_mail_plugins_woocommerce_variation_bold" name="haet_mail_plugins[woocommerce][variationbold]" value="1" <?php echo ($plugin_options['woocommerce']['variationbold']==1?'checked':''); ?>>
                        <label for="haet_mail_plugins_woocommerce_variation_bold"><?php _e('Bold','haet_mail'); ?></label>
                        
                        &nbsp;&nbsp; 
                        
                        <input type="hidden" name="haet_mail_plugins[woocommerce][variationitalic]" value="0">
                        <input type="checkbox" id="haet_mail_plugins_woocommerce_variation_italic" name="haet_mail_plugins[woocommerce][variationitalic]" value="1" <?php echo ($plugin_options['woocommerce']['variationitalic']==1?'checked':''); ?>>
                        <label for="haet_mail_plugins_woocommerce_variation_italic"><?php _e('Italic','haet_mail'); ?></label>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="haet_mail_plugins_woocommerce_total_font"><?php _e('Total Font','haet_mail'); ?></label>
                    </th>
                    <td>
                        <select  id="haet_mail_plugins_woocommerce_total_font" name="haet_mail_plugins[woocommerce][totalfont]">
                            <?php foreach ($fonts as $font => $display_name) :?>
                                <option value="<?php echo $font; ?>" <?php echo ($plugin_options['woocommerce']['totalfont']==$font?'selected':''); ?>><?php echo $display_name; ?></option>     
                            <?php endforeach; ?>
                        </select>
                        
                        <select  id="haet_mail_plugins_woocommerce_total_fontsize" name="haet_mail_plugins[woocommerce][totalfontsize]">
                            <?php for ($fontsize=12; $fontsize<=50; $fontsize++) :?>
                                <option value="<?php echo $fontsize; ?>" <?php echo ($plugin_options['woocommerce']['totalfontsize']==$fontsize?'selected':''); ?>><?php echo $fontsize; ?>px</option>       
                            <?php endfor; ?>
                        </select>

                        <select  id="haet_mail_plugins_woocommerce_total_align" name="haet_mail_plugins[woocommerce][totalalign]">
                            <option value="left" <?php echo ($plugin_options['woocommerce']['totalalign']=="left"?"selected":""); ?>><?php _e('Left','haet_mail'); ?></option>
                            <option value="center" <?php echo ($plugin_options['woocommerce']['totalalign']=="center"?"selected":""); ?>><?php _e('Center','haet_mail'); ?></option>
                            <option value="right" <?php echo ($plugin_options['woocommerce']['totalalign']=="right"?"selected":""); ?>><?php _e('Right','haet_mail'); ?></option>
                        </select>

                        <input type="text" class="color" id="haet_mail_plugins_woocommerce_total_color" name="haet_mail_plugins[woocommerce][totalcolor]" value="<?php echo $plugin_options['woocommerce']['totalcolor']; ?>">

                        <input type="hidden" name="haet_mail_plugins[woocommerce][totalbold]" value="0">
                        <input type="checkbox" id="haet_mail_plugins_woocommerce_total_bold" name="haet_mail_plugins[woocommerce][totalbold]" value="1" <?php echo ($plugin_options['woocommerce']['totalbold']==1?'checked':''); ?>>
                        <label for="haet_mail_plugins_woocommerce_total_bold"><?php _e('Bold','haet_mail'); ?></label>
                        
                        &nbsp;&nbsp; 
                        
                        <input type="hidden" name="haet_mail_plugins[woocommerce][totalitalic]" value="0">
                        <input type="checkbox" id="haet_mail_plugins_woocommerce_total_italic" name="haet_mail_plugins[woocommerce][totalitalic]" value="1" <?php echo ($plugin_options['woocommerce']['totalitalic']==1?'checked':''); ?>>
                        <label for="haet_mail_plugins_woocommerce_total_italic"><?php _e('Italic','haet_mail'); ?></label>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label><?php _e('Alignment','haet_mail'); ?></label>
                    </th>
                    <td>
                        <label><?php _e('Quantity','haet_mail'); ?></label>
                        <select name="haet_mail_plugins[woocommerce][quantity_align]">
                            <option value="left" <?php echo ($plugin_options['woocommerce']['quantity_align']=="left"?"selected":""); ?>><?php _e('Left','haet_mail'); ?></option>
                            <option value="center" <?php echo ($plugin_options['woocommerce']['quantity_align']=="center"?"selected":""); ?>><?php _e('Center','haet_mail'); ?></option>
                            <option value="right" <?php echo ($plugin_options['woocommerce']['quantity_align']=="right"?"selected":""); ?>><?php _e('Right','haet_mail'); ?></option>
                        </select>
                        &nbsp;&nbsp; 
                        <label><?php _e('Price','haet_mail'); ?></label>
                        <select name="haet_mail_plugins[woocommerce][price_align]">
                            <option value="left" <?php echo ($plugin_options['woocommerce']['price_align']=="left"?"selected":""); ?>><?php _e('Left','haet_mail'); ?></option>
                            <option value="center" <?php echo ($plugin_options['woocommerce']['price_align']=="center"?"selected":""); ?>><?php _e('Center','haet_mail'); ?></option>
                            <option value="right" <?php echo ($plugin_options['woocommerce']['price_align']=="right"?"selected":""); ?>><?php _e('Right','haet_mail'); ?></option>
                        </select>
                        &nbsp;&nbsp; 
                        <label><?php _e('Address','haet_mail'); ?></label>
                        <select name="haet_mail_plugins[woocommerce][address_align]">
                            <option value="left" <?php echo ($plugin_options['woocommerce']['address_align']=="left"?"selected":""); ?>><?php _e('Left','haet_mail'); ?></option>
                            <option value="center" <?php echo ($plugin_options['woocommerce']['address_align']=="center"?"selected":""); ?>><?php _e('Center','haet_mail'); ?></option>
                            <option value="right" <?php echo ($plugin_options['woocommerce']['address_align']=="right"?"selected":""); ?>><?php _e('Right','haet_mail'); ?></option>
                        </select>
                    </td>
                </tr>

                <tr valign="top">
                    <th scope="row"><label><?php _e('Header border','haet_mail'); ?></label></th>
                    <td>
                        <input type="text" class="color" id="haet_mail_plugins_woocommerce_header_bordercolor" name="haet_mail_plugins[woocommerce][header_bordercolor]" value="<?php echo ( isset($plugin_options['woocommerce']['header_bordercolor']) ? $plugin_options['woocommerce']['header_bordercolor'] : '#000'); ?>">
                        
                        <input type="hidden" name="haet_mail_plugins[woocommerce][header_border_outer_v]" value="0">
                        <input type="checkbox" id="haet_mail_plugins_woocommerce_header_border_outer_v" name="haet_mail_plugins[woocommerce][header_border_outer_v]" value="1" <?php echo (isset($plugin_options['woocommerce']['header_border_outer_v']) && $plugin_options['woocommerce']['header_border_outer_v']==1 ?'checked':''); ?>>
                        <label for="haet_mail_plugins_woocommerce_header_border_outer_v" class="border-choice-label">
                            <table class="border-choice border-choice-outer-v"><tr><td></td><td></td></tr><tr><td></td><td></td></tr></table>
                        </label>

                        <input type="hidden" name="haet_mail_plugins[woocommerce][header_border_inner_v]" value="0">
                        <input type="checkbox" id="haet_mail_plugins_woocommerce_header_border_inner_v" name="haet_mail_plugins[woocommerce][header_border_inner_v]" value="1" <?php echo (isset($plugin_options['woocommerce']['header_border_inner_v']) && $plugin_options['woocommerce']['header_border_inner_v']==1 ?'checked':''); ?>>
                        <label for="haet_mail_plugins_woocommerce_header_border_inner_v" class="border-choice-label">
                            <table class="border-choice border-choice-inner-v"><tr><td></td><td></td></tr><tr><td></td><td></td></tr></table>
                        </label>

                        <input type="hidden" name="haet_mail_plugins[woocommerce][header_border_top]" value="0">
                        <input type="checkbox" id="haet_mail_plugins_woocommerce_header_border_top" name="haet_mail_plugins[woocommerce][header_border_top]" value="1" <?php echo (isset($plugin_options['woocommerce']['header_border_top']) && $plugin_options['woocommerce']['header_border_top']==1 ?'checked':''); ?>>
                        <label for="haet_mail_plugins_woocommerce_header_border_top" class="border-choice-label">
                            <table class="border-choice border-choice-top"><tr><td></td><td></td></tr><tr><td></td><td></td></tr></table>
                        </label>

                        <input type="hidden" name="haet_mail_plugins[woocommerce][header_border_bottom]" value="0">
                        <input type="checkbox" id="haet_mail_plugins_woocommerce_header_border_bottom" name="haet_mail_plugins[woocommerce][header_border_bottom]" value="1" <?php echo (isset($plugin_options['woocommerce']['header_border_bottom']) && $plugin_options['woocommerce']['header_border_bottom']==1 || !isset($plugin_options['woocommerce'])?'checked':''); ?>>
                        <label for="haet_mail_plugins_woocommerce_header_border_bottom" class="border-choice-label">
                            <table class="border-choice border-choice-bottom"><tr><td></td><td></td></tr><tr><td></td><td></td></tr></table>
                        </label>
                    </td>
                </tr>

                <tr valign="top">
                    <th scope="row"><label><?php _e('Products border','haet_mail'); ?></label></th>
                    <td>
                        <input type="text" class="color" id="haet_mail_plugins_woocommerce_products_bordercolor" name="haet_mail_plugins[woocommerce][products_bordercolor]" value="<?php echo ( isset($plugin_options['woocommerce']['products_bordercolor']) ? $plugin_options['woocommerce']['products_bordercolor'] : '#000'); ?>">

                        <input type="hidden" name="haet_mail_plugins[woocommerce][products_border_outer_v]" value="0">
                        <input type="checkbox" id="haet_mail_plugins_woocommerce_products_border_outer_v" name="haet_mail_plugins[woocommerce][products_border_outer_v]" value="1" <?php echo (isset($plugin_options['woocommerce']['products_border_outer_v']) && $plugin_options['woocommerce']['products_border_outer_v']==1 ?'checked':''); ?>>
                        <label for="haet_mail_plugins_woocommerce_products_border_outer_v" class="border-choice-label">
                            <table class="border-choice border-choice-outer-v"><tr><td></td><td></td></tr><tr><td></td><td></td></tr></table>
                        </label>

                        <input type="hidden" name="haet_mail_plugins[woocommerce][products_border_inner_v]" value="0">
                        <input type="checkbox" id="haet_mail_plugins_woocommerce_products_border_inner_v" name="haet_mail_plugins[woocommerce][products_border_inner_v]" value="1" <?php echo (isset($plugin_options['woocommerce']['products_border_inner_v']) && $plugin_options['woocommerce']['products_border_inner_v']==1 ?'checked':''); ?>>
                        <label for="haet_mail_plugins_woocommerce_products_border_inner_v" class="border-choice-label">
                            <table class="border-choice border-choice-inner-v"><tr><td></td><td></td></tr><tr><td></td><td></td></tr></table>
                        </label>
    
                        <input type="hidden" name="haet_mail_plugins[woocommerce][products_border_top]" value="0">
                        <input type="checkbox" id="haet_mail_plugins_woocommerce_products_border_top" name="haet_mail_plugins[woocommerce][products_border_top]" value="1" <?php echo (isset($plugin_options['woocommerce']['products_border_top']) && $plugin_options['woocommerce']['products_border_top']==1 ?'checked':''); ?>>
                        <label for="haet_mail_plugins_woocommerce_products_border_top" class="border-choice-label">
                            <table class="border-choice border-choice-top"><tr><td></td><td></td></tr><tr><td></td><td></td></tr></table>
                        </label>
                        
                        <input type="hidden" name="haet_mail_plugins[woocommerce][products_border_inner_h]" value="0">
                        <input type="checkbox" id="haet_mail_plugins_woocommerce_products_border_inner_h" name="haet_mail_plugins[woocommerce][products_border_inner_h]" value="1" <?php echo (isset($plugin_options['woocommerce']['products_border_inner_h']) && $plugin_options['woocommerce']['products_border_inner_h']==1 ?'checked':''); ?>>
                        <label for="haet_mail_plugins_woocommerce_products_border_inner_h" class="border-choice-label">
                            <table class="border-choice border-choice-inner-h"><tr><td></td><td></td></tr><tr><td></td><td></td></tr></table>
                        </label>

                        <input type="hidden" name="haet_mail_plugins[woocommerce][products_border_bottom]" value="0">
                        <input type="checkbox" id="haet_mail_plugins_woocommerce_products_border_bottom" name="haet_mail_plugins[woocommerce][products_border_bottom]" value="1" <?php echo (isset($plugin_options['woocommerce']['products_border_bottom']) && $plugin_options['woocommerce']['products_border_bottom']==1 || !isset($plugin_options['woocommerce'])?'checked':''); ?>>
                        <label for="haet_mail_plugins_woocommerce_products_border_bottom" class="border-choice-label">
                            <table class="border-choice border-choice-bottom"><tr><td></td><td></td></tr><tr><td></td><td></td></tr></table>
                        </label>
                    </td>
                </tr>

                <tr valign="top">
                    <th scope="row"><label><?php _e('Total border','haet_mail'); ?></label></th>
                    <td>
                        <input type="text" class="color" id="haet_mail_plugins_woocommerce_total_bordercolor" name="haet_mail_plugins[woocommerce][total_bordercolor]" value="<?php echo ( isset($plugin_options['woocommerce']['total_bordercolor']) ? $plugin_options['woocommerce']['total_bordercolor'] : '#000'); ?>">

                        <input type="hidden" name="haet_mail_plugins[woocommerce][total_border_outer_v]" value="0">
                        <input type="checkbox" id="haet_mail_plugins_woocommerce_total_border_outer_v" name="haet_mail_plugins[woocommerce][total_border_outer_v]" value="1" <?php echo (isset($plugin_options['woocommerce']['total_border_outer_v']) && $plugin_options['woocommerce']['total_border_outer_v']==1 ?'checked':''); ?>>
                        <label for="haet_mail_plugins_woocommerce_total_border_outer_v" class="border-choice-label">
                            <table class="border-choice border-choice-outer-v"><tr><td></td><td></td></tr><tr><td></td><td></td></tr></table>
                        </label>

                        <input type="hidden" name="haet_mail_plugins[woocommerce][total_border_inner_v]" value="0">
                        <input type="checkbox" id="haet_mail_plugins_woocommerce_total_border_inner_v" name="haet_mail_plugins[woocommerce][total_border_inner_v]" value="1" <?php echo (isset($plugin_options['woocommerce']['total_border_inner_v']) && $plugin_options['woocommerce']['total_border_inner_v']==1 ?'checked':''); ?>>
                        <label for="haet_mail_plugins_woocommerce_total_border_inner_v" class="border-choice-label">
                            <table class="border-choice border-choice-inner-v"><tr><td></td><td></td></tr><tr><td></td><td></td></tr></table>
                        </label>

                        <input type="hidden" name="haet_mail_plugins[woocommerce][total_border_top]" value="0">
                        <input type="checkbox" id="haet_mail_plugins_woocommerce_total_border_top" name="haet_mail_plugins[woocommerce][total_border_top]" value="1" <?php echo (isset($plugin_options['woocommerce']['total_border_top']) && $plugin_options['woocommerce']['total_border_top']==1 ?'checked':''); ?>>
                        <label for="haet_mail_plugins_woocommerce_total_border_top" class="border-choice-label">
                            <table class="border-choice border-choice-top"><tr><td></td><td></td></tr><tr><td></td><td></td></tr></table>
                        </label>

                        <input type="hidden" name="haet_mail_plugins[woocommerce][total_border_inner_h]" value="0">
                        <input type="checkbox" id="haet_mail_plugins_woocommerce_total_border_inner_h" name="haet_mail_plugins[woocommerce][total_border_inner_h]" value="1" <?php echo (isset($plugin_options['woocommerce']['total_border_inner_h']) && $plugin_options['woocommerce']['total_border_inner_h']==1 ?'checked':''); ?>>
                        <label for="haet_mail_plugins_woocommerce_total_border_inner_h" class="border-choice-label">
                            <table class="border-choice border-choice-inner-h"><tr><td></td><td></td></tr><tr><td></td><td></td></tr></table>
                        </label>

                        <input type="hidden" name="haet_mail_plugins[woocommerce][total_border_bottom]" value="0">
                        <input type="checkbox" id="haet_mail_plugins_woocommerce_total_border_bottom" name="haet_mail_plugins[woocommerce][total_border_bottom]" value="1" <?php echo (isset($plugin_options['woocommerce']['total_border_bottom']) && $plugin_options['woocommerce']['total_border_bottom']==1 || !isset($plugin_options['woocommerce'])?'checked':''); ?>>
                        <label for="haet_mail_plugins_woocommerce_total_border_bottom" class="border-choice-label">
                            <table class="border-choice border-choice-bottom"><tr><td></td><td></td></tr><tr><td></td><td></td></tr></table>
                        </label>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="postbox haet-mail-woocommerce-mailbuilder">
    <div style="" class="inside">
        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row"><label><?php _e('Build your email','haet_mail'); ?></label></th>
                    <td class="customize-email-content-selection">
                        <?php if( !is_array( $order_ids ) || count( $order_ids ) == 0 ): ?>
                            <?php _e('Please create at least one order. We need this as demo data.','haet_mail'); ?>
                        <?php else: ?>
                            <?php foreach ($available_woocommerce_mails as $woocommerce_mail) :?>
                                <p>
                                    <?php 
                                    echo str_replace('_',' ', str_replace('WC_Email_', '', $woocommerce_mail) );
                                    
                                    edit_post_link('<span class="dashicons dashicons-edit"></span>','','', Haet_Mail_Builder()->get_email_post_id( $woocommerce_mail )); 
                                    ?>
                                </p>       
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="postbox">
    <div style="" class="inside">
        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row"><label><?php _e('Preview woocommerce mail','haet_mail'); ?></label></th>
                    <td>
                        <select id="haet_mail_plugins_woocommerce_preview_order" name="haet_mail_plugins[woocommerce][preview_order]" >
                            <?php foreach ($order_ids as $order_id) :?>
                                <option value="<?php echo $order_id; ?>" <?php echo ($plugin_options['woocommerce']['preview_order']==$order_id?'selected':''); ?>><?php echo __('Order #','haet_mail').$order_id; ?></option>       
                            <?php endforeach; ?>
                        </select>
                        <select id="haet_mail_plugins_woocommerce_preview_mail" name="haet_mail_plugins[woocommerce][preview_mail]" >
                            <?php foreach ($available_woocommerce_mails as $woocommerce_mail) :?>
                                <option value="<?php echo $woocommerce_mail; ?>" <?php echo ($plugin_options['woocommerce']['preview_mail']==$woocommerce_mail?'selected':''); ?>><?php echo str_replace('_',' ', str_replace('WC_Email_', '', $woocommerce_mail) ); ?></option>       
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row"><label><?php _e('Send test mails','haet_mail'); ?></label></th>
                    <td>
                        <?php _e('Go to the order edit screen to resend some order mails and see the results in your mail client.','haet_mail'); ?>
                    </td>
                </tr>
                
            </tbody>
        </table>
    </div>
</div>
<?php 
// output default settings
// echo '<pre>';
// foreach ($plugin_options['woocommerce'] as $key => $value)
//     echo "'$key' => '$value',\n";
// echo '</pre>';
?>